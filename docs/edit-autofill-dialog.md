# Edit Autosolve Dialog
**Status**: draft | implemented

The most important thing about the autosolve dialog is that it's
basically just a giant state machine. The UI updates itself fully when
the state changes.

* **READY**: We haven't started the search
* **RUNNING**: In the middle of searching
* **COMPLETE**: A search completed.

We do _NOT_ get signals from the solver when boards are found, and for
state changes other than RUNNING → COMPLETE. Instead, we poll
externally. This is for performance reasons; notifying when boards are
found can slow down the entire operation.
