# Internals overview

These documents describes the way the two applications are written. It
doesn't go into a lot of detail in each section, but gives a rough
overview and hints to understand the codebase better.

This page covers the **shared code** between the two
applications. There are four major sections:

* libipuz
* Puzzle state
* The puzzle stack
* The word list

# libipuz
libipuz is one of the main dependencies of the game. This library
loads and save puzzles and provides the fundamental structure that
games are based on. It mostly adheres to the [ipuz](http://ipuz.org)
spec as befitting its name, but has its own extensions as well to
provide functionality from other formats. libipuz can load and save
files to only this format: in order to read from other formats, we use
convertors first.

When using this librariy, there is an `IPuzPuzzle` GObject that serves
as a base class for all puzzle types inherit from. `IPuzCrossword` is
an IPuzPuzzle that represents crossword-style games, while
`IPuzAcrostic` *[planned]* is another base type. Other puzzle types
(such as cryptic crosswords) inherit from IPuzCrossword.

libipuz has the following characteristics that we count on:
* ***It provides an API to .ipuz files.*** The ipuz file format is
  essentially a structured json format. The expectation is that users
  of those files would just load the structure into memory and
  directly read/write from it. libipuz consolidates and validates the
  file, and provides a more convenient API for reading and writing to
  puzzles.
* ***IPuzPuzzles are relatively fast to load, save, and duplicate.***
  We create and destroy puzzles fairly regularly. We make deep copies
  of the puzzles in order for the undo stack to work.
* ***We use IPuzPuzzles immutably.*** While it's certainly possible to
  change an IPuzPuzzle, we never do so in the game, and do so only in
  a structured manner in the Editor. IPuzPuzzles don't emit signals
  when their state changes.
* ***Current user guesses are stored in an IPuzGuesses struct.*** This
  is a separate data type from the puzzle. It can be modified and
  attached to a puzzle. It is not a GObject, but is refcounted.
* ***libipuz is data-only.*** libipuz doesn't link to gtk and has no
  widget support.

In addition, puzzles using libipuz can be extensively styled. Wex try
to support as many of styling options found in the spec as possible.

**Related code:**
* **IPuzPuzzle:** ipuz-puzzle([.h](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-puzzle.h),[.c](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-puzzle.c))
* **IPuzCrossword:** ipuz-crossword([.h](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-crossword.h),[.c](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-crossword.c))
* **IPuzGuesses:** ipuz-guesses([.h](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-guesses.h),[.c](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/ipuz-guesses.c))

# Puzzle state

# The puzzle stack

# The word list

