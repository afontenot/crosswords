/* puzzle-set-config.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>

#include <contrib/gnome-languages.h>
#include "crosswords-misc.h"
#include "picker-grid.h"
#include "picker-list.h"
#include "puzzle-set-config.h"


enum
{
  PROP_0,
  PROP_RESOURCE,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };


#define PROPAGATE_LOCAL_ERROR()                 \
  if (local_error)                              \
    {                                           \
      g_propagate_error (error, local_error);   \
      return FALSE;                             \
    }                                           \


typedef struct
{
  gchar *puzzle_name;
  /* Thumbnail */
} ConfigPuzzle;

struct _PuzzleSetConfig
{
  GObject parent_object;
  GResource *resource;
  GKeyFile *key_file;

  /* [Puzzle Set] */
  gchar *id;
  gchar *short_name;
  gchar *long_name;
  gchar *locale;
  gchar *language;
  GType picker_gtype;
  gboolean disabled;
  ConfigSetTags tags;

  /* [Picker] */
  gchar *header;
  gchar *subheader;
  gchar *header_face;
  gchar *subheader_face;
  gchar *puzzle_image; /* Unused */

  /* [Picker Grid] */
  ConfigGridStyle grid_style;
  guint width;
  guint height;

  /* [Picker List] */
  gboolean use_button;
  gboolean show_progress;
  gchar *url;

  /* [Picker Button] */
  gchar *button_label;
  ConfigButtonType button_type;

  /* [Downloader] */
  ConfigDownloaderType downloader_type;
  gchar *downloader_command;
  gchar *downloader_primary_text;
  gchar *downloader_secondary_text;
  gchar *downloader_link_uri;
  gchar *downloader_link_text;
  gboolean downloader_requires_network;
  gboolean downloader_convert_puz_to_ipuz;

  GDate *lower_date;
  GDate *upper_date;
  GDate *default_date;

  int lower_number;
  int upper_number;
  int default_number;

  /* [Puzzles] */
  GArray *puzzles;
};


static void     puzzle_set_config_init         (PuzzleSetConfig       *self);
static void     puzzle_set_config_class_init   (PuzzleSetConfigClass  *klass);
static void     puzzle_set_config_set_property (GObject               *object,
                                                guint                  prop_id,
                                                const GValue          *value,
                                                GParamSpec            *pspec);
static void     puzzle_set_config_get_property (GObject               *object,
                                                guint                  prop_id,
                                                GValue                *value,
                                                GParamSpec            *pspec);
static void     puzzle_set_config_dispose      (GObject               *object);
static gboolean puzzle_set_config_load         (PuzzleSetConfig       *self,
                                                GError               **error);


G_DEFINE_TYPE (PuzzleSetConfig, puzzle_set_config, G_TYPE_OBJECT);


static void
clear_config_puzzle (gpointer data)
{
  ConfigPuzzle *config_puzzle = data;

  g_clear_pointer (&config_puzzle->puzzle_name, g_free);
}
static void
puzzle_set_config_init (PuzzleSetConfig *self)
{
  self->lower_number = -1;
  self->upper_number = -1;
  self->default_number = -1;

  self->puzzles = g_array_new (FALSE, TRUE, sizeof (ConfigPuzzle));
  g_array_set_clear_func (self->puzzles, clear_config_puzzle);
}

static void
puzzle_set_config_class_init (PuzzleSetConfigClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = puzzle_set_config_get_property;
  object_class->set_property = puzzle_set_config_set_property;
  object_class->dispose = puzzle_set_config_dispose;

  obj_props[PROP_RESOURCE] =
    g_param_spec_boxed ("resource",
                        "Resource",
                        "Resource that the picker set is loaded from",
                        G_TYPE_RESOURCE,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
puzzle_set_config_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  PuzzleSetConfig *self;

  self = PUZZLE_SET_CONFIG (object);

  switch (prop_id)
    {
    case PROP_RESOURCE:
      g_clear_pointer (&self->resource, g_resource_unref);
      self->resource = (GResource *) g_value_dup_boxed (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_config_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  PuzzleSetConfig *self;

  self = PUZZLE_SET_CONFIG (object);

  switch (prop_id)
    {
    case PROP_RESOURCE:
      g_value_set_boxed (value, self->resource);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_config_dispose (GObject *object)
{
  PuzzleSetConfig *self;

  self = PUZZLE_SET_CONFIG (object);

  g_clear_pointer (&self->resource, g_resource_unref);
  g_clear_pointer (&self->key_file, g_key_file_unref);
  g_clear_pointer (&self->puzzles, g_array_unref);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->short_name, g_free);
  g_clear_pointer (&self->long_name, g_free);
  g_clear_pointer (&self->locale, g_free);
  g_clear_pointer (&self->language, g_free);

  G_OBJECT_CLASS (puzzle_set_config_parent_class)->dispose (object);
}

/* Note: This can't be used before self->key_file and self->id is
 * set. */
static gboolean
get_boolean_value (PuzzleSetConfig  *self,
                   gboolean          required,
                   gboolean          default_val,
                   const gchar      *group_name,
                   const gchar      *key,
                   GError          **error)
{
  gboolean retval;
  GError *local_error = NULL;

  g_assert (self);
  g_assert (self->key_file);
  g_assert (self->id);

  retval = g_key_file_get_boolean (self->key_file, group_name, key, &local_error);

  if (local_error)
    {
      if (local_error->code == G_KEY_FILE_ERROR_GROUP_NOT_FOUND)
        g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                     "Puzzle Set %s: missing group %s",
                     self->id, group_name);
      else if (required && local_error->code == G_KEY_FILE_ERROR_KEY_NOT_FOUND)
        g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                     "Puzzle Set %s: Section [%s] missing key %s",
                     self->id, group_name, key);
      else if (local_error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND)
        g_propagate_error (error, local_error);

      return default_val;
    }

  return retval;
}

static gchar *
get_string_value (PuzzleSetConfig  *self,
                  gboolean          translate_str,
                  gboolean          required,
                  const gchar      *group_name,
                  const gchar      *key,
                  GError          **error)
{
  GError *local_error = NULL;
  gchar *str;

  g_assert (self);
  g_assert (self->key_file);
  g_assert (self->id);

  if (translate_str)
    str = g_key_file_get_locale_string (self->key_file, group_name, key, NULL, &local_error);
  else
    str = g_key_file_get_string (self->key_file, group_name, key, &local_error);

  if (local_error && local_error->code == G_KEY_FILE_ERROR_GROUP_NOT_FOUND)
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set %s: missing group %s",
                   self->id, group_name);
      return NULL;
    }

  if (required && local_error && local_error->code == G_KEY_FILE_ERROR_KEY_NOT_FOUND)
    g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                 "Puzzle Set %s: Section [%s] missing key %s",
                 self->id, group_name, key);

  if (local_error && local_error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND)
    g_propagate_error (error, local_error);

  return str;
}


static GDate *
get_date_value (PuzzleSetConfig  *self,
                gboolean          required,
                const gchar      *group_name,
                const gchar      *key,
                GError          **error)
{
  GError *local_error = NULL;
  g_autofree gchar *date_string = NULL;

  date_string = g_key_file_get_string (self->key_file, group_name, key, &local_error);

  if (required &&
      ((local_error && local_error->code == G_KEY_FILE_ERROR_KEY_NOT_FOUND) ||
       date_string == NULL))
    g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                 "Puzzle Set %s: Section [%s] missing key %s",
                 self->id, group_name, key);
  if (local_error && local_error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND)
    g_propagate_error (error, local_error);
  if (*error)
    return NULL;

  if (date_string)
    {
      GDate *date;
      date = g_date_new ();
      g_date_set_parse (date, date_string);

      if (! g_date_valid (date))
        {
          g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                       "Puzzle Set %s: Section [%s] key %s: Invalid date string %s",
                       self->id, group_name, key,date_string);
          return NULL;
        }
      return date;
    }
  /* we only reach this if the date string is empty and we're not
   * required. */
  return NULL;
}

static gchar *
find_puzzle_config (PuzzleSetConfig  *self,
                    GError          **error)
{
  g_autoptr (GError) local_error = NULL;
  gchar **children;
  g_autofree gchar *prefix = NULL;

  children = g_resource_enumerate_children (self->resource,
                                            PUZZLE_SET_PREFIX,
                                            0, &local_error);
  if (local_error)
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Unable to read resource for Puzzle Set: %s", local_error->message);
      return FALSE;
    }

  /* We only support the puzzle_config being in the first directory
   * for now. At some point I could imagine having multiple games in
   * a resource, or more complicated layouts. */
  if (children && children[0] != NULL)
    prefix = g_build_filename (PUZZLE_SET_PREFIX, children[0], NULL);
  g_strfreev (children);

  return g_build_filename (prefix, "puzzle.config", NULL);
}

static gboolean
load_puzzle_config (PuzzleSetConfig  *self,
                    GError          **error)
{
  g_autoptr (GError) local_error = NULL;
  g_autofree gchar *config_file = NULL;
  g_autoptr (GBytes) bytes = NULL;
  GKeyFile *key_file = NULL;

  config_file = find_puzzle_config (self, error);
  if (config_file == NULL)
    return FALSE;

  bytes = g_resource_lookup_data (self->resource, config_file, 0, &local_error);
  if (local_error)
    {
      g_warning ("Unable to find %s in resource%s", config_file, local_error->message);
      g_propagate_error (error, local_error);
      return FALSE;
    }

  key_file = g_key_file_new ();
  if (! g_key_file_load_from_bytes (key_file, bytes, G_KEY_FILE_NONE, &local_error))
    {
      g_key_file_unref (key_file);
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Corrupt puzzle.config file: %s", local_error->message);
      return FALSE;
    }

  self->key_file = key_file;
  return TRUE;
}

static gboolean
load_puzzle_set (PuzzleSetConfig  *self,
                 GError          **error)
{
  g_autoptr (GError) local_error = NULL;
  gchar **tags = NULL;
  gsize tags_length = 0;

  self->id = g_key_file_get_string (self->key_file, "Puzzle Set", "ID", NULL);
  if (self->id == NULL)
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set: puzzle.conf file missing ID value");
      return FALSE;
    };


  self->short_name = get_string_value (self, TRUE, TRUE, "Puzzle Set", "ShortName", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->long_name = get_string_value (self, TRUE, TRUE, "Puzzle Set", "LongName", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->locale = get_string_value (self, FALSE, TRUE, "Puzzle Set", "Locale", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (self->locale)
    gnome_parse_locale (self->locale,
                        &self->language,
                        NULL, NULL, NULL);

  /* Is this an enabled Puzzle Set? We will hide it if Disabled is
   * TRUE, or if the try exec test fails. */
  self->disabled = g_key_file_get_boolean (self->key_file, "Puzzle Set", "Disabled", NULL);
  if (!self->disabled)
    {
      g_autofree gchar *try_exec = NULL;

      try_exec = g_key_file_get_string (self->key_file, "Puzzle Set", "TryExec", NULL);
      if (try_exec != NULL)
        {
          g_autofree gchar *program = NULL;

          program = g_find_program_in_path (try_exec);
          if (program == NULL)
            self->disabled = TRUE;
        }
    }

  tags = g_key_file_get_string_list (self->key_file,
                                     "Puzzle Set",
                                     "Tags",
                                     &tags_length,
                                     &local_error);

  /* tags are optional, so we don't propagate errors */
  self->tags =0;
  for (guint i = 0; i < tags_length; i++)
    {
      if (g_ascii_strncasecmp (tags[i], "mini",
                               strlen ("mini")) == 0)
        self->tags |= CONFIG_SET_TAGS_MINI;
      else if (g_ascii_strncasecmp (tags[i], "regular",
                                    strlen ("regular")) == 0)
        self->tags |= CONFIG_SET_TAGS_REGULAR;
      else if (g_ascii_strncasecmp (tags[i], "jumbo",
                                    strlen ("jumbo")) == 0)
        self->tags |= CONFIG_SET_TAGS_JUMBO;
      else if (g_ascii_strncasecmp (tags[i], "acrostic",
                                    strlen ("acrostic")) == 0)
        self->tags |= CONFIG_SET_TAGS_ACROSTIC;
      else if (g_ascii_strncasecmp (tags[i], "arrowword",
                                    strlen ("arrowword")) == 0)
        self->tags |= CONFIG_SET_TAGS_ARROWWORD;
      else if (g_ascii_strncasecmp (tags[i], "barred",
                                    strlen ("barred")) == 0)
        self->tags |= CONFIG_SET_TAGS_ACROSTIC;
      else if (g_ascii_strncasecmp (tags[i], "crossword",
                                    strlen ("crossword")) == 0)
        self->tags |= CONFIG_SET_TAGS_CROSSWORD;
      else if (g_ascii_strncasecmp (tags[i], "cryptic",
                                    strlen ("cryptic")) == 0)
        self->tags |= CONFIG_SET_TAGS_CRYPTIC;
      else if (g_ascii_strncasecmp (tags[i], "filippine",
                                    strlen ("filippine")) == 0)
        self->tags |= CONFIG_SET_TAGS_FILIPPINE;
    }
  g_strfreev (tags);
  return TRUE;
}

static gboolean
load_puzzle_set_picker_grid (PuzzleSetConfig  *self,
                             GError          **error)
{
  GError *local_error = NULL;
  g_autofree char *grid_style = NULL;

  grid_style = get_string_value (self, FALSE, TRUE, "Picker Grid", "GridStyle", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (g_strcmp0 (grid_style, "open") == 0)
    self->grid_style = CONFIG_GRID_OPEN;
  else if (g_strcmp0 (grid_style, "lock-and-image") == 0)
    self->grid_style = CONFIG_GRID_LOCK_AND_IMAGE;
  else if (g_strcmp0 (grid_style, "riddle") == 0)
    self->grid_style = CONFIG_GRID_RIDDLE;
  else
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set %s: Unknown grid type %s", self->id, grid_style);
      return FALSE;
    }

  self->width = g_key_file_get_uint64 (self->key_file, "Picker Grid", "Width", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->height = g_key_file_get_uint64 (self->key_file, "Picker Grid", "Height", &local_error);
  PROPAGATE_LOCAL_ERROR();

  return TRUE;
}

static gboolean
load_puzzle_set_downloader (PuzzleSetConfig  *self,
                            GError          **error)
{
  GError *local_error = NULL;
  g_autofree gchar *downloader_type = NULL;

  downloader_type = get_string_value (self, FALSE, TRUE, "Downloader", "Type", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (g_strcmp0 (downloader_type, "auto") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_AUTO;
  else if (g_strcmp0 (downloader_type, "date") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_DATE;
  /* FIXME: I shipped this as "url" though it should be uri. */
  else if (g_strcmp0 (downloader_type, "url") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_URI;
  else if (g_strcmp0 (downloader_type, "uri") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_URI;
  else if (g_strcmp0 (downloader_type, "number") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_NUMBER;
  else if (g_strcmp0 (downloader_type, "entry") == 0)
    self->downloader_type = CONFIG_DOWNLOADER_ENTRY;
  else
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set %s: Unknown downloader type %s", self->id, downloader_type);
      return FALSE;
    }

  self->downloader_command = get_string_value (self, FALSE, TRUE, "Downloader", "Command", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_primary_text = get_string_value (self, TRUE, FALSE, "Downloader", "PrimaryText", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_secondary_text = get_string_value (self, TRUE, FALSE, "Downloader", "SecondaryText", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_link_uri = get_string_value (self, TRUE, FALSE, "Downloader", "LinkUri", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_link_text = get_string_value (self, TRUE, FALSE, "Downloader", "LinkText", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_convert_puz_to_ipuz = get_boolean_value (self, FALSE, FALSE, "Downloader", "ConvertPuzToIpuz", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->downloader_requires_network = get_boolean_value (self, FALSE, FALSE, "Downloader", "RequiresNetwork", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (self->downloader_type == CONFIG_DOWNLOADER_NUMBER)
    {
      self->lower_number = g_key_file_get_int64 (self->key_file, "Downloader", "LowerValue", &local_error);
      if (local_error)
        {
          self->lower_number = -1;
          g_clear_pointer (&local_error, g_error_free);
        }
      self->upper_number = g_key_file_get_int64 (self->key_file, "Downloader", "UpperValue", &local_error);
      if (local_error)
        {
          self->upper_number = -1;
          g_clear_pointer (&local_error, g_error_free);
        }
      self->default_number = g_key_file_get_int64 (self->key_file, "Downloader", "DefaultValue", &local_error);
      if (local_error)
        {
          self->default_number = -1;
          g_clear_pointer (&local_error, g_error_free);
        }
    }

  if (self->downloader_type == CONFIG_DOWNLOADER_DATE)
    {
      self->lower_date = get_date_value (self, FALSE, "Downloader", "LowerValue", &local_error);
      PROPAGATE_LOCAL_ERROR();

      self->upper_date = get_date_value (self, FALSE, "Downloader", "UpperValue", &local_error);
      PROPAGATE_LOCAL_ERROR();

      self->default_date = get_date_value (self, FALSE, "Downloader", "DefaultValue", &local_error);
      PROPAGATE_LOCAL_ERROR();

    }

  return TRUE;
}

static gboolean
load_puzzle_set_picker_button (PuzzleSetConfig  *self,
                               GError          **error)
{
  GError *local_error = NULL;
  g_autofree gchar *button_type = NULL;

  self->button_label = get_string_value (self, TRUE, TRUE, "Picker Button", "Label", &local_error);
  PROPAGATE_LOCAL_ERROR();

  button_type = get_string_value (self, TRUE, TRUE, "Picker Button", "Type", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (g_strcmp0 (button_type, "file") == 0)
    self->button_type = CONFIG_BUTTON_FILE;
  else if (g_strcmp0 (button_type, "downloader") == 0)
    self->button_type = CONFIG_BUTTON_DOWNLOADER;
  else
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set %s: Unknown button type %s", self->id, button_type);
      return FALSE;
    }

  if (self->button_type == CONFIG_BUTTON_DOWNLOADER)
    return load_puzzle_set_downloader (self, error);
  else
    return TRUE;
}

static gboolean
load_puzzle_set_picker_list (PuzzleSetConfig  *self,
                             GError          **error)
{
  GError *local_error = NULL;

  self->use_button = get_boolean_value (self, FALSE, FALSE, "Picker List", "UseButton", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->show_progress = get_boolean_value (self, FALSE, FALSE, "Picker List", "ShowProgress", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->url = get_string_value (self, FALSE, FALSE, "Picker List", "URL", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (self->use_button)
    return load_puzzle_set_picker_button (self, error);
  else
    return TRUE;
}

static gboolean
load_puzzle_set_picker (PuzzleSetConfig  *self,
                        GError          **error)
{
  GError *local_error = NULL;
  g_autofree gchar *picker = NULL;
  const gchar *group = NULL;

  picker = get_string_value (self, FALSE, TRUE, "Puzzle Set", "Picker", &local_error);
  PROPAGATE_LOCAL_ERROR();

  if (g_strcmp0 (picker, "grid") == 0)
    {
      self->picker_gtype = PICKER_TYPE_GRID;
      group = "Picker Grid";
    }
  else if (g_strcmp0 (picker, "list") == 0)
    {
      self->picker_gtype = PICKER_TYPE_LIST;
      group = "Picker List";
    }
  else if (g_strcmp0 (picker, "none") == 0)
    {
      self->picker_gtype = G_TYPE_NONE;
      return TRUE;
    }
  else
    {
      g_set_error (error, CONFIG_ERROR, CONFIG_ERROR_INVALID_CONF,
                   "Puzzle Set %s: Unknown picker type %s", self->id, picker);
      return FALSE;
    }

  self->header = get_string_value (self, TRUE, TRUE, group, "Header", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->header_face = get_string_value (self, FALSE, FALSE, group, "HeaderFace", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->subheader = get_string_value (self, TRUE, FALSE, group, "SubHeader", &local_error);
  PROPAGATE_LOCAL_ERROR();

  self->subheader_face = get_string_value (self, FALSE, FALSE, group, "SubHeaderFace", &local_error);
  PROPAGATE_LOCAL_ERROR();

  /* load the picker details */
  if (self->picker_gtype == PICKER_TYPE_GRID)
    return load_puzzle_set_picker_grid (self, error);
  else if (self->picker_gtype == PICKER_TYPE_LIST)
    return load_puzzle_set_picker_list (self, error);

  g_assert_not_reached ();
}

static gboolean
load_puzzle_set_puzzles (PuzzleSetConfig  *self,
                         GError          **error)
{
  guint puzzle_id = 1;

  while (TRUE)
    {
      ConfigPuzzle config_puzzle = {0, };
      g_autofree gchar *section = NULL;

      section = g_strdup_printf ("Puzzle%u", puzzle_id);
      config_puzzle.puzzle_name =
        g_key_file_get_string (self->key_file, section, "PuzzleName", NULL);
      if (config_puzzle.puzzle_name == NULL)
        break;

      g_array_append_val (self->puzzles, config_puzzle);
      puzzle_id++;
    }
  return TRUE;
}

static gboolean
puzzle_set_config_load (PuzzleSetConfig  *self,
                        GError          **error)
{
  if (! load_puzzle_config (self,error))
    return FALSE;
  else if (! load_puzzle_set (self, error))
    return FALSE;
  else if (! load_puzzle_set_picker (self, error))
    return FALSE;
  else if (! load_puzzle_set_puzzles (self, error))
    return FALSE;

  return TRUE;
}

/* Public functions */

GQuark
config_error_quark (void)
{
  return g_quark_from_static_string ("config-error-quark");
}

PuzzleSetConfig *
puzzle_set_config_new (GResource  *resource,
                       GError    **error)
{
  PuzzleSetConfig *config;

  g_return_val_if_fail (resource != NULL, NULL);

  config = g_object_new (PUZZLE_TYPE_SET_CONFIG,
                         "resource", resource,
                         NULL);
  if (puzzle_set_config_load (config, error))
    return config;

  g_object_unref (config);
  return NULL;
}

GResource *
puzzle_set_config_get_resource (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->resource;
}

GKeyFile *
puzzle_set_config_get_key_file (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->key_file;
}

/* [Puzzle Set] */
const gchar *
puzzle_set_config_get_id (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->id;
}

const gchar *
puzzle_set_config_get_short_name (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->short_name;
}

const gchar *
puzzle_set_config_get_long_name (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->long_name;
}

const gchar *
puzzle_set_config_get_locale (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->locale;
}

const gchar *
puzzle_set_config_get_language (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->language;
}

gboolean
puzzle_set_config_get_disabled (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), TRUE);

  return self->disabled;
}

ConfigSetTags
puzzle_set_config_get_tags (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), 0);

  return self->tags;
}

/* [Picker] */
GType
puzzle_set_config_get_picker_type (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), G_TYPE_NONE);

  return self->picker_gtype;
}

const gchar *
puzzle_set_config_get_header (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->header;
}

const gchar *
puzzle_set_config_get_subheader (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->subheader;
}

const gchar *
puzzle_set_config_get_header_face (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->header_face;
}

const gchar *
puzzle_set_config_get_subheader_face (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->subheader_face;
}


/* [Picker Grid] */
ConfigGridStyle
puzzle_set_config_get_grid_style (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), FALSE);

  return self->grid_style;
}

guint
puzzle_set_config_get_width (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), 0);

  return self->width;
}

guint
puzzle_set_config_get_height (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), 0);

  return self->height;
}

/* [Picker List] */
gboolean
puzzle_set_config_get_use_button (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), FALSE);

  return self->use_button;
}

gboolean
puzzle_set_config_get_show_progress (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), FALSE);

  return self->show_progress;
}

const gchar *
puzzle_set_config_get_url (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->url;
}

/* [Picker Button] */
const gchar *
puzzle_set_config_get_button_label (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->button_label;
}

ConfigButtonType
puzzle_set_config_get_button_type (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), CONFIG_BUTTON_FILE);

  return self->button_type;
}


/* [Downloader] */
ConfigDownloaderType
puzzle_set_config_get_downloader_type (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), CONFIG_DOWNLOADER_AUTO);

  return self->downloader_type;
}

const gchar *puzzle_set_config_get_downloader_command (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->downloader_command;
}

const gchar *puzzle_set_config_get_downloader_primary_text (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->downloader_primary_text;
}

const gchar *puzzle_set_config_get_downloader_secondary_text (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->downloader_secondary_text;
}

const gchar *puzzle_set_config_get_downloader_link_uri (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->downloader_link_uri;
}

const gchar *puzzle_set_config_get_downloader_link_text (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->downloader_link_text;
}

gboolean
puzzle_set_config_get_convert_puz_to_ipuz (PuzzleSetConfig  *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), FALSE);

  return self->downloader_convert_puz_to_ipuz;
}

gboolean
puzzle_set_config_get_requires_network (PuzzleSetConfig  *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), FALSE);

  return self->downloader_requires_network;
}

gint
puzzle_set_config_get_lower_number (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), -1);

  return self->lower_number;
}

gint
puzzle_set_config_get_upper_number (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), -1);

  return self->upper_number;
}

gint
puzzle_set_config_get_default_number (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), -1);

  return self->default_number;
}

GDate *
puzzle_set_config_get_lower_date (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->lower_date;
}

GDate *
puzzle_set_config_get_upper_date (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->upper_date;
}

GDate *
puzzle_set_config_get_default_date (PuzzleSetConfig *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  return self->default_date;
}

/* [Puzzles] */
const gchar *
puzzle_set_config_get_puzzle_name (PuzzleSetConfig *self,
                                   guint            puzzle_id)
{
  ConfigPuzzle config_puzzle;

  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  if (puzzle_id >= self->puzzles->len)
    return NULL;
  config_puzzle = g_array_index (self->puzzles, ConfigPuzzle, puzzle_id);

  return config_puzzle.puzzle_name;
}

static IPuzPuzzle *
resource_load_puzzle (GResource   *resource,
                      const gchar *id,
                      const gchar *file,
                      GError      **error)
{
  IPuzPuzzle *puzzle;
  g_autofree gchar *file_path = g_build_filename (PUZZLE_SET_PREFIX, id, file, NULL);

  puzzle = xwd_load_puzzle_from_resource (resource, file_path, error);

  return puzzle;
}

const gchar *
puzzle_set_config_get_puzzle_thumb (PuzzleSetConfig *self,
                                    guint            puzzle_id)
{
  return NULL;
}


IPuzPuzzle *
puzzle_set_config_load_puzzle (PuzzleSetConfig  *self,
                               guint             puzzle_id,
                               GError          **error)
{
  ConfigPuzzle config_puzzle;
  IPuzPuzzle *puzzle;

  g_return_val_if_fail (PUZZLE_IS_SET_CONFIG (self), NULL);

  if (puzzle_id >= self->puzzles->len)
    return NULL;
  config_puzzle = g_array_index (self->puzzles, ConfigPuzzle, puzzle_id);

  puzzle = resource_load_puzzle (self->resource, self->id,
                                 config_puzzle.puzzle_name, error);

  return puzzle;
}

/* Public functions */

void
puzzle_set_config_print (PuzzleSetConfig *self)
{
  g_return_if_fail (PUZZLE_IS_SET_CONFIG (self));

  g_print ("[Puzzle Set]\n");
  if (self->id) g_print ("id=%s\n", self->id);
  if (self->short_name) g_print ("short_name=%s\n", self->short_name);
  if (self->long_name) g_print ("long_name=%s\n", self->long_name);
  if (self->locale) g_print ("locale=%s\n", self->locale);
  if (self->language) g_print ("language=%s\n", self->language);
  if (self->disabled) g_print ("disabled=%s\n", self->disabled?"true":"false");

  if (self->picker_gtype == PICKER_TYPE_LIST)
    g_print ("\n[Picker List]\n");
  else
    g_print ("\n[Picker Grid]\n");
  if (self->header) g_print ("header=%s\n", self->header);
  if (self->subheader) g_print ("subheader=%s\n", self->subheader);
  if (self->header_face) g_print ("header_face=%s\n", self->header_face);
  if (self->subheader_face) g_print ("subheader_face=%s\n", self->subheader_face);
  if (self->puzzle_image) g_print ("puzzle_image=%s\n", self->puzzle_image);


  if (self->picker_gtype == PICKER_TYPE_GRID)
    {
      switch (self->grid_style)
        {
        case CONFIG_GRID_OPEN:
          g_print ("grid_style=open\n");
          break;
        case CONFIG_GRID_LOCK_AND_IMAGE:
          g_print ("grid_style=lock-and-image\n");
          break;
        case CONFIG_GRID_RIDDLE:
          g_print ("grid_style=riddle\n");
          break;
        default:
          g_assert_not_reached ();
        }
      g_print ("width=%u\n", self->width);
      g_print ("height=%u\n", self->height);
    }
  else /* PICKER_TYPE_LIST */
    {
      if (self->use_button) g_print ("use_button=%s\n", self->use_button?"true":"false");
      if (self->show_progress) g_print ("show_progress=%s\n", self->show_progress?"true":"false");
      if (self->url) g_print ("url=%s\n", self->url);

      if (self->use_button)
        {
          g_print ("\n[Picker Button]\n");
          if (self->button_label) g_print ("button_label=%s\n", self->button_label);
          if (self->button_type == CONFIG_BUTTON_FILE)
            g_print ("button_type=file");
          else
            g_print ("button_type=downloader");
        }

      if (self->use_button && self->button_type == CONFIG_BUTTON_DOWNLOADER)
        {
          g_print ("\n[Downloader]\n");
          g_print ("type=");
          switch (self->downloader_type)
            {
            case CONFIG_DOWNLOADER_AUTO:
              g_print ("auto\n");
              break;
            case CONFIG_DOWNLOADER_DATE:
              g_print ("date\n");
              break;
            case CONFIG_DOWNLOADER_URI:
              g_print ("uri\n");
              break;
            case CONFIG_DOWNLOADER_NUMBER:
              g_print ("number\n");
              break;
            case CONFIG_DOWNLOADER_ENTRY:
              g_print ("entry\n");
              break;
            default:
              g_assert_not_reached ();
            }
          if (self->downloader_command) g_print ("command=%s\n", self->downloader_command);
          if (self->downloader_primary_text) g_print ("primary_text=%s\n", self->downloader_primary_text);
          if (self->downloader_secondary_text) g_print ("secondary_text=%s\n", self->downloader_secondary_text);
          if (self->downloader_link_uri) g_print ("link_uri=%s\n", self->downloader_link_uri);
          if (self->downloader_link_text) g_print ("link_text=%s\n", self->downloader_link_text);
          if (self->downloader_requires_network) g_print ("requires_network=%s\n", self->downloader_requires_network?"true":"false");
          if (self->downloader_convert_puz_to_ipuz) g_print ("convert_puz_to_ipuz=%s\n", self->downloader_convert_puz_to_ipuz?"true":"false");

          if (self->downloader_type == CONFIG_DOWNLOADER_DATE)
            {
#define DATE_STR_LEN 30
              gchar date_str[DATE_STR_LEN];
              if (self->lower_date)
                {
                  g_date_strftime (date_str, DATE_STR_LEN, "%a, %d %b %Y %T %z", self->lower_date);
                  g_print ("lower_date=%s\n", date_str);
                }
              if (self->upper_date)
                {
                  g_date_strftime (date_str, DATE_STR_LEN, "%a, %d %b %Y %T %z", self->upper_date);
                  g_print ("upper_date=%s\n", date_str);
                }
              if (self->default_date)
                {
                  g_date_strftime (date_str, DATE_STR_LEN, "%a, %d %b %Y %T %z", self->default_date);
                  g_print ("default_date=%s\n", date_str);
                }
            }

          if (self->downloader_type == CONFIG_DOWNLOADER_NUMBER)
            {
              if (self->lower_number != -1) g_print ("lower_number=%u\n", self->lower_number);
              if (self->upper_number != -1) g_print ("upper_number=%u\n", self->upper_number);
              if (self->default_number != -1) g_print ("default_number=%u\n", self->default_number);
            }
        }
    }

  for (guint i = 0; i < self->puzzles->len; i++)
    {
      ConfigPuzzle config_puzzle;

      g_print ("\n[Puzzle%u]\n", i + 1);
      config_puzzle = g_array_index (self->puzzles, ConfigPuzzle, i);
      g_print ("puzzle_name=%s\n", config_puzzle.puzzle_name);
    }

  g_print ("\n\n");
}
