/* edit-entry-row.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>

G_BEGIN_DECLS


typedef enum
{
  EDIT_ENTRY_ROW_HTML,
  EDIT_ENTRY_ROW_ENUMERATION,
} EditEntryRowValidationMode;


#define EDIT_TYPE_ENTRY_ROW (edit_entry_row_get_type())
G_DECLARE_FINAL_TYPE (EditEntryRow, edit_entry_row, EDIT, ENTRY_ROW, AdwEntryRow);


void edit_entry_row_set_text            (EditEntryRow               *self,
                                         const gchar                *text);
void edit_entry_row_set_warning         (EditEntryRow               *self,
                                         const gchar                *message);
void edit_entry_row_set_error           (EditEntryRow               *self,
                                         const gchar                *message);
void edit_entry_row_commit              (EditEntryRow               *self);
void edit_entry_row_set_validation_mode (EditEntryRow               *self,
                                         EditEntryRowValidationMode  mode);


G_END_DECLS
