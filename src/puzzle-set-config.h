/* puzzle-set-config.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS


typedef enum
{
  CONFIG_SET_TAGS_MINI = 1 << 0,    /* 7×7 or smaller */
  CONFIG_SET_TAGS_REGULAR = 1 << 1, /* Between 8×8 and 20×20 */
  CONFIG_SET_TAGS_JUMBO = 1 << 2,   /* 21×21 or greater */

  CONFIG_SET_TAGS_ACROSTIC  = 1 << 3,
  CONFIG_SET_TAGS_ARROWWORD = 1 << 4,
  CONFIG_SET_TAGS_BARRED    = 1 << 5,
  CONFIG_SET_TAGS_CROSSWORD = 1 << 6,
  CONFIG_SET_TAGS_CRYPTIC   = 1 << 7,
  CONFIG_SET_TAGS_FILIPPINE = 1 << 8,
} ConfigSetTags;

typedef enum
{
  CONFIG_BUTTON_FILE,
  CONFIG_BUTTON_DOWNLOADER,
} ConfigButtonType;

typedef enum
{
  CONFIG_GRID_OPEN,
  CONFIG_GRID_LOCK_AND_IMAGE,
  CONFIG_GRID_RIDDLE,
} ConfigGridStyle;

typedef enum
{
  CONFIG_DOWNLOADER_AUTO,
  CONFIG_DOWNLOADER_DATE,
  CONFIG_DOWNLOADER_URI,
  CONFIG_DOWNLOADER_NUMBER,
  CONFIG_DOWNLOADER_ENTRY,
} ConfigDownloaderType;

enum
{
  CONFIG_ERROR_INVALID_CONF,
  CONFIG_ERROR_MISSING_ASSET,
};

#define CONFIG_ERROR config_error_quark ()
GQuark config_error_quark (void);


#define PUZZLE_TYPE_SET_CONFIG (puzzle_set_config_get_type())
G_DECLARE_FINAL_TYPE (PuzzleSetConfig, puzzle_set_config, PUZZLE, SET_CONFIG, GObject);


PuzzleSetConfig      *puzzle_set_config_new                           (GResource        *resource,
                                                                       GError          **error);
GResource            *puzzle_set_config_get_resource                  (PuzzleSetConfig  *self);

/* [Puzzle Set] */
const gchar          *puzzle_set_config_get_id                        (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_short_name                (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_long_name                 (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_locale                    (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_language                  (PuzzleSetConfig  *self);
gboolean              puzzle_set_config_get_disabled                  (PuzzleSetConfig  *self);
ConfigSetTags         puzzle_set_config_get_tags                      (PuzzleSetConfig  *self);

/* [Picker] */
GType                 puzzle_set_config_get_picker_type               (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_header                    (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_subheader                 (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_header_face               (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_subheader_face            (PuzzleSetConfig  *self);

/* [Picker Grid] */
ConfigGridStyle       puzzle_set_config_get_grid_style                (PuzzleSetConfig  *self);
guint                 puzzle_set_config_get_width                     (PuzzleSetConfig  *self);
guint                 puzzle_set_config_get_height                    (PuzzleSetConfig  *self);

/* [Picker List] */
gboolean              puzzle_set_config_get_use_button                (PuzzleSetConfig  *self);
gboolean              puzzle_set_config_get_show_progress             (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_url                       (PuzzleSetConfig  *self);

/* [Picker Button] */
const gchar          *puzzle_set_config_get_button_label              (PuzzleSetConfig  *self);
ConfigButtonType      puzzle_set_config_get_button_type               (PuzzleSetConfig  *self);

/* [Downloader] */
ConfigDownloaderType  puzzle_set_config_get_downloader_type           (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_downloader_command        (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_downloader_primary_text   (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_downloader_secondary_text (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_downloader_link_uri       (PuzzleSetConfig  *self);
const gchar          *puzzle_set_config_get_downloader_link_text      (PuzzleSetConfig  *self);
gboolean              puzzle_set_config_get_convert_puz_to_ipuz       (PuzzleSetConfig  *self);
gboolean              puzzle_set_config_get_requires_network          (PuzzleSetConfig  *self);
gint                  puzzle_set_config_get_lower_number              (PuzzleSetConfig  *self);
gint                  puzzle_set_config_get_upper_number              (PuzzleSetConfig  *self);
gint                  puzzle_set_config_get_default_number            (PuzzleSetConfig  *self);
GDate                *puzzle_set_config_get_lower_date                (PuzzleSetConfig  *self);
GDate                *puzzle_set_config_get_upper_date                (PuzzleSetConfig  *self);
GDate                *puzzle_set_config_get_default_date              (PuzzleSetConfig  *self);

/* [Puzzles] */
const gchar          *puzzle_set_config_get_puzzle_name               (PuzzleSetConfig  *self,
                                                                       guint             puzzle_id);
const gchar          *puzzle_set_config_get_puzzle_thumb              (PuzzleSetConfig  *self,
                                                                       guint             puzzle_id);
IPuzPuzzle           *puzzle_set_config_load_puzzle                   (PuzzleSetConfig  *self,
                                                                       guint             puzzle_id,
                                                                       GError          **error);

/* Public functions  */
void                  puzzle_set_config_print                         (PuzzleSetConfig  *self);

G_END_DECLS
