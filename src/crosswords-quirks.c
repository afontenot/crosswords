/* crosswords-quirks.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "contrib/gnome-languages.h"
#include "crosswords-quirks.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"


enum
{
  PROP_0,
  PROP_IJ_DIGRAPH,
  PROP_GUESS_ADVANCE,
  PROP_SYMMETRY,
  PROP_FOCUS_LOCATION,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };


struct _CrosswordsQuirks
{
  GObject parent_object;

  GSettings *settings;
  gboolean ij_digraph;
  QuirksFocusLocation location;
  gboolean rebus;
  IPuzSymmetry symmetry;
};


static void crosswords_quirks_init         (CrosswordsQuirks      *self);
static void crosswords_quirks_class_init   (CrosswordsQuirksClass *klass);
static void crosswords_quirks_set_property (GObject               *object,
                                            guint                  prop_id,
                                            const GValue          *value,
                                            GParamSpec            *pspec);
static void crosswords_quirks_get_property (GObject               *object,
                                            guint                  prop_id,
                                            GValue                *value,
                                            GParamSpec            *pspec);
static void crosswords_quirks_dispose      (GObject               *object);


G_DEFINE_TYPE(CrosswordsQuirks, crosswords_quirks, G_TYPE_OBJECT);


static void
crosswords_quirks_init (CrosswordsQuirks *self)
{
  const gchar *const *languages;

  self->settings = g_settings_new ("org.gnome.Crosswords");

  /* IJ Digraph */
  self->ij_digraph = FALSE;
  languages = g_get_language_names ();
  for (int i = 0; languages[i] != NULL; i++)
    {
      if (g_strcmp0 (languages[i], "nl") == 0)
        self->ij_digraph = TRUE;
    }
  self->symmetry = IPUZ_SYMMETRY_NONE;
}

static void
crosswords_quirks_class_init (CrosswordsQuirksClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = crosswords_quirks_set_property;
  object_class->get_property = crosswords_quirks_get_property;
  object_class->dispose = crosswords_quirks_dispose;

  obj_props[PROP_IJ_DIGRAPH] =
    g_param_spec_boolean ("ij-digraph",
                          "IJ Digraph",
                          "IJ Digraph",
                          FALSE,
                          G_PARAM_READABLE);

  obj_props[PROP_GUESS_ADVANCE] =
    g_param_spec_enum ("guess-advance",
                       "Guess Advance",
                       "Guess Advance",
                       QUIRKS_TYPE_GUESS_ADVANCE,
                       QUIRKS_GUESS_ADVANCE_ADJACENT,
                       G_PARAM_READABLE);

  obj_props[PROP_SYMMETRY] =
    g_param_spec_enum ("symmetry",
                       "Symmetry",
                       "Symmetry",
                       I_TYPE_PUZ_SYMMETRY,
                       IPUZ_SYMMETRY_NONE,
                       G_PARAM_READABLE);

  obj_props[PROP_FOCUS_LOCATION] =
    g_param_spec_enum ("focus-location",
		       "Focus Location",
		       "Focus Location",
		       QUIRKS_TYPE_FOCUS_LOCATION,
		       QUIRKS_FOCUS_LOCATION_NONE,
		       G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
crosswords_quirks_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  CrosswordsQuirks *self = (CrosswordsQuirks *) object;

  switch (prop_id)
    {
    case PROP_FOCUS_LOCATION:
      self->location = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
crosswords_quirks_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  CrosswordsQuirks *self = (CrosswordsQuirks *) object;

  switch (prop_id)
    {
    case PROP_IJ_DIGRAPH:
      g_value_set_boolean (value, self->ij_digraph);
      break;
    case PROP_GUESS_ADVANCE:
      g_value_set_enum (value, crosswords_quirks_get_guess_advance (self));
      break;
    case PROP_SYMMETRY:
      g_value_set_enum (value, crosswords_quirks_get_symmetry (self));
      break;
    case PROP_FOCUS_LOCATION:
      g_value_set_enum (value, crosswords_quirks_get_focus_location (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
crosswords_quirks_dispose (GObject *object)
{
  CrosswordsQuirks *self;

  self = CROSSWORDS_QUIRKS (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (crosswords_quirks_parent_class)->dispose (object);
}


static void
crosswords_quirks_load_from_puzzle (CrosswordsQuirks *self,
                                    IPuzPuzzle       *puzzle)
{
  guint row, col, width, height;
  g_autofree gchar *puzzle_locale = NULL;
  g_autofree gchar *puzzle_language = NULL;

  if (IPUZ_IS_ACROSTIC (puzzle))
    {
      crosswords_quirks_set_focus_location (self, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
      return;
    }

  g_object_get (puzzle,
                "locale", &puzzle_locale,
                NULL);

  if (puzzle_locale)
    {
      gnome_parse_locale (puzzle_locale,
                               &puzzle_language,
                               NULL, NULL, NULL);
      if (g_strcmp0 (puzzle_language, "nl") == 0)
        self->ij_digraph = TRUE;
    }

  width = ipuz_crossword_get_width (IPUZ_CROSSWORD (puzzle));
  height = ipuz_crossword_get_height (IPUZ_CROSSWORD (puzzle));

  for (row = 0; row < height; row++)
    {
      for (col = 0; col < width; col++)
        {
          IPuzCell *cell;
          IPuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);

          if (g_strcmp0 (ipuz_cell_get_solution (cell), "IJ") == 0)
            self->ij_digraph = TRUE;
        }
    }
}

/* Public methods */

CrosswordsQuirks *
crosswords_quirks_new (IPuzPuzzle *puzzle)
{
  CrosswordsQuirks *quirks;
  
  if (puzzle)
    g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), NULL);

  quirks = g_object_new (CROSSWORDS_TYPE_QUIRKS, NULL);
  if (puzzle)
    crosswords_quirks_load_from_puzzle (quirks, puzzle);

  return quirks;
}

gboolean
crosswords_quirks_get_ij_digraph (CrosswordsQuirks *quirks)
{
  if (quirks == NULL)
    return FALSE;

  return quirks->ij_digraph;
}

QuirksGuessAdvance
crosswords_quirks_get_guess_advance (CrosswordsQuirks *quirks)
{
  g_autofree gchar *advance_type = NULL;

  if (quirks == NULL)
    return QUIRKS_GUESS_ADVANCE_ADJACENT;

  advance_type = g_settings_get_string (quirks->settings, "guess-advance-type");

  if (g_strcmp0 (advance_type, "adjacent") == 0)
    return QUIRKS_GUESS_ADVANCE_ADJACENT;
  else if (g_strcmp0 (advance_type, "open") == 0)
    return QUIRKS_GUESS_ADVANCE_OPEN;
  else if (g_strcmp0 (advance_type, "open-in-clue") == 0)
    return QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE;

  g_warning ("Unknown guess-advance-type: %s", advance_type);
  return QUIRKS_GUESS_ADVANCE_ADJACENT;
}

gboolean
crosswords_quirks_get_switch_on_move (CrosswordsQuirks *quirks)
{
  if (quirks == NULL)
    return TRUE;

  return g_settings_get_boolean (quirks->settings, "switch-on-move");
}

IPuzSymmetry
crosswords_quirks_get_symmetry (CrosswordsQuirks *quirks)
{
  if (quirks == NULL)
    return IPUZ_SYMMETRY_NONE;

  return quirks->symmetry;
}

void
crosswords_quirks_set_symmetry (CrosswordsQuirks *quirks,
                                IPuzSymmetry      symmetry)
{
  g_return_if_fail (CROSSWORDS_IS_QUIRKS (quirks));

  if (quirks->symmetry != symmetry)
    {
      quirks->symmetry = symmetry;
      g_object_notify_by_pspec (G_OBJECT (quirks), obj_props [PROP_SYMMETRY]);
    }
}

void
crosswords_quirks_set_focus_location (CrosswordsQuirks   *quirks,
		                      QuirksFocusLocation location)
{
  g_return_if_fail (CROSSWORDS_IS_QUIRKS (quirks));

  if (quirks->location != location)
    {
      quirks->location = location;
      g_object_notify_by_pspec (G_OBJECT (quirks), obj_props [PROP_FOCUS_LOCATION]);
    }
}

QuirksFocusLocation
crosswords_quirks_get_focus_location (CrosswordsQuirks *quirks)
{
  if (quirks == NULL)
    return QUIRKS_FOCUS_LOCATION_NONE;

  return quirks->location;
}
