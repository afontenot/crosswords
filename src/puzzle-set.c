/* puzzle-set.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "play-window.h"
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "picker-grid.h"
#include "picker-list.h"
#include "play-xword.h"
#include "puzzle-downloader.h"
#include "puzzle-set.h"
#include "puzzle-set-config.h"
#include "puzzle-set-model.h"

enum
{
  PUZZLES_START,
  CHANGE_PHASE,
  PUZZLES_DONE,
  REVEAL_CANCELED,
  WON_CHANGED,
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SET_TYPE,
  PROP_SHOWN,
  PROP_N_WON,
  PROP_N_PUZZLES,
  N_PROPS
};

static guint puzzle_set_signals[LAST_SIGNAL] = { 0 };
static GParamSpec *obj_props[N_PROPS] = {NULL, };


typedef struct
{
  PuzzleSetConfig *config;
  PuzzleSetModel *model;
  GtkWidget *xword;
  GtkWidget *picker;
  gchar *selected_puzzle_name;

  PuzzleSetType set_type;
  gboolean shown;

  PuzzlePhase phase;
  guint n_won;
  guint n_puzzles;

  /* Downloader */
  DownloaderState state;
  GCancellable *downloader_cancellable; /* This being non-null is our download_state */
  PuzzleDownloader *downloader;
  gulong network_monitor_signal;
} PuzzleSetPrivate;


static void         puzzle_set_init                 (PuzzleSet       *self);
static void         puzzle_set_class_init           (PuzzleSetClass  *klass);
static void         puzzle_set_set_property         (GObject         *object,
                                                     guint            prop_id,
                                                     const GValue    *value,
                                                     GParamSpec      *pspec);
static void         puzzle_set_get_property         (GObject         *object,
                                                     guint            prop_id,
                                                     GValue          *value,
                                                     GParamSpec      *pspec);
static void         puzzle_set_dispose              (GObject         *object);

static void         puzzle_set_real_puzzles_start   (PuzzleSet       *puzzle_set);
static void         puzzle_set_real_change_phase    (PuzzleSet       *puzzle_set,
                                                     PuzzlePhase      phase);
static void         puzzle_set_real_puzzles_done    (PuzzleSet       *puzzle_set);
static void         puzzle_set_save                 (PuzzleSet       *self);
static void         start_download_cb               (PuzzleSet       *self);
static void         puzzle_set_set_downloader_state (PuzzleSet       *self,
                                                     DownloaderState  state);


G_DEFINE_TYPE_WITH_PRIVATE (PuzzleSet, puzzle_set, G_TYPE_OBJECT);


static
void puzzle_set_init (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));

  priv->phase = PUZZLE_PHASE_MAIN;
  /* Keep in sync with the initial visibility state of picker buttons */
  priv->state = DOWNLOADER_STATE_READY;
  priv->n_won = 0;
  priv->n_puzzles = 0;
}

static void
puzzle_set_class_init (PuzzleSetClass *klass)
{
  GObjectClass *object_class;

  object_class = (GObjectClass *) klass;

  object_class->get_property = puzzle_set_get_property;
  object_class->set_property = puzzle_set_set_property;
  object_class->dispose = puzzle_set_dispose;

  /* Navigation signals */
  klass->puzzles_start = puzzle_set_real_puzzles_start;
  klass->change_phase = puzzle_set_real_change_phase;
  klass->puzzles_done = puzzle_set_real_puzzles_done;

  puzzle_set_signals [PUZZLES_START] =
    g_signal_new ("puzzles-start",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, puzzles_start),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  puzzle_set_signals [CHANGE_PHASE] =
    g_signal_new ("change-phase",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, change_phase),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  PUZZLE_TYPE_PHASE);

  puzzle_set_signals [PUZZLES_DONE] =
    g_signal_new ("puzzles-done",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, puzzles_done),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  puzzle_set_signals [REVEAL_CANCELED] =
    g_signal_new ("reveal-canceled",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, reveal_canceled),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  puzzle_set_signals [WON_CHANGED] =
    g_signal_new ("won-changed",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PuzzleSetClass, reveal_canceled),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_SET_TYPE] =
    g_param_spec_enum ("set-type",
                       "Set Type",
                       "Type of Puzzle Set",
                       PUZZLE_TYPE_SET_TYPE,
                       PUZZLE_SET_TYPE_RESOURCE,
                       G_PARAM_READWRITE);

  obj_props[PROP_SHOWN] =
    g_param_spec_boolean ("shown",
                          "Shown",
                          "User selected Puzzle Set",
                          FALSE,
                          G_PARAM_READWRITE);

  obj_props[PROP_N_WON] =
    g_param_spec_uint ("n-won",
                       "N Won",
                       "Number of Won games",
                       0, G_MAXUINT,
                       0,
                       G_PARAM_READABLE);

  obj_props[PROP_N_PUZZLES] =
    g_param_spec_uint ("n-puzzles",
                       "N Puzzles",
                       "Number of Puzzles",
                       0, G_MAXUINT,
                       0,
                       G_PARAM_READABLE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
puzzle_set_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  switch (prop_id)
    {
    case PROP_SET_TYPE:
      g_value_set_enum (value, priv->set_type);
      break;
    case PROP_SHOWN:
      g_value_set_boolean (value, priv->shown);
      break;
    case PROP_N_WON:
      g_value_set_enum (value, priv->n_won);
      break;
    case PROP_N_PUZZLES:
      g_value_set_boolean (value, priv->n_puzzles);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  switch (prop_id)
    {
    case PROP_SET_TYPE:
      priv->set_type = g_value_get_enum (value);
      break;
    case PROP_SHOWN:
      puzzle_set_set_shown (PUZZLE_SET (object), g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_dispose (GObject *object)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (PUZZLE_SET (object));

  g_clear_signal_handler (&priv->network_monitor_signal,
                          g_network_monitor_get_default ());

  g_clear_object (&priv->config);
  g_clear_object (&priv->model);
  g_clear_object (&priv->picker);
  g_clear_object (&priv->xword);
  g_clear_pointer (&priv->selected_puzzle_name, g_free);

  G_OBJECT_CLASS (puzzle_set_parent_class)->finalize (object);
}

static void
select_puzzle_cb (PuzzleSet   *puzzle_set,
                  const gchar *puzzle_name)

{
  PuzzleSetPrivate *priv;
  guint index;

  if (!puzzle_name)
    return;

  priv = puzzle_set_get_instance_private (puzzle_set);

  if (puzzle_set_model_find_by_puzzle_name (priv->model,
                                            puzzle_name,
                                            &index))
    {
      PuzzleSetModelRow *row;

      row = g_list_model_get_item (G_LIST_MODEL (priv->model), index);
      play_xword_load_puzzle (PLAY_XWORD (priv->xword),
                              puzzle_set_model_row_get_puzzle (row));
      g_clear_pointer (&priv->selected_puzzle_name, g_free);
      priv->selected_puzzle_name = g_strdup (puzzle_name);
      puzzle_set_change_phase (puzzle_set, PUZZLE_PHASE_GAME);
    }
}

/* Called when the puzzle set is selected */
static void
puzzle_set_real_puzzles_start (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (self);

  priv->xword = play_xword_new ();
  g_signal_connect_swapped (priv->xword, "user-idle", G_CALLBACK (puzzle_set_save), self);
  g_signal_connect_swapped (priv->xword, "reveal-canceled",
                            G_CALLBACK (puzzle_set_reveal_canceled),
                            self);

  priv->picker = (GtkWidget *)
    g_object_new (puzzle_set_config_get_picker_type (priv->config),
                  "config", priv->config,
                  "model", priv->model,
                  NULL);
  g_signal_connect_swapped (G_OBJECT (priv->picker),
                            "puzzle-selected",
                            G_CALLBACK (select_puzzle_cb),
                            self);
  g_signal_connect_swapped (G_OBJECT (priv->picker),
                            "start-download",
                            G_CALLBACK (start_download_cb),
                            self);

  g_object_ref_sink (priv->xword);
  g_object_ref_sink (priv->picker);
}

static void
puzzle_set_real_change_phase (PuzzleSet   *self,
                              PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (self);

  if (priv->picker)
    puzzle_set_save (self);

  if (priv->xword)
    {
      play_xword_set_reveal_mode (PLAY_XWORD (priv->xword),
                                  GRID_REVEAL_NONE);
    }

  if (phase != PUZZLE_PHASE_GAME)
    {
      g_clear_pointer (&priv->selected_puzzle_name, g_free);
      play_xword_clear_toasts (PLAY_XWORD (priv->xword));
    }

  priv->phase = phase;
}

static void
puzzle_set_real_puzzles_done (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  priv = puzzle_set_get_instance_private (self);

  puzzle_set_save (self);
  play_xword_clear_toasts (PLAY_XWORD (priv->xword));
  g_clear_object (&priv->picker);
  g_clear_object (&priv->xword);
  g_clear_pointer (&priv->selected_puzzle_name, g_free);

  priv->phase = PUZZLE_PHASE_MAIN;
}

static void
puzzle_set_save (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;
  IPuzGuesses *guesses;
  PuzzleSetModelRow *row = NULL;
  guint index = 0;

  priv = puzzle_set_get_instance_private (self);
  
  if (priv->phase != PUZZLE_PHASE_GAME)
    return;

  guesses = play_xword_get_guesses (PLAY_XWORD (priv->xword));
  if (puzzle_set_model_find_by_puzzle_name (priv->model,
                                            priv->selected_puzzle_name,
                                            &index))
    row = g_list_model_get_item (G_LIST_MODEL (priv->model), index);

  if (row)
    puzzle_set_model_row_set_guesses (row, guesses);
}


static void
import_done_func (PuzzleSetModelRow *row,
                  PuzzleSet         *self)
{
  PuzzleSetPrivate *priv;

  g_assert (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  /* If we have a picker, select the puzzle */
  if (priv->picker)
    puzzle_picker_puzzle_selected (PUZZLE_PICKER (priv->picker),
                                   puzzle_set_model_row_get_puzzle_name (row));
}

static void
puzzle_set_downloader_finished (PuzzleSet   *self,
                                const gchar *uri,
                                gboolean     delete_when_done)
{
  PuzzleSetPrivate *priv;

  g_assert (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (PUZZLE_SET (self));

  /* if URI is NULL, then the operation was canceled */
  if (uri)
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (GFile) file = NULL;
      g_autofree gchar *temp_path = NULL;

      file = g_file_new_for_uri (uri);
      temp_path = g_file_get_path (file);

      puzzle_set_model_import_puzzle_path (priv->model,
                                           temp_path,
                                           delete_when_done,
                                           (ModelImportDoneFunc) import_done_func,
                                           g_object_ref (self),
                                           g_object_unref,
                                           &error);

      if (error)
        {
          GtkWidget *toast_overlay;
          GtkRoot *root;
          g_autofree gchar *msg = NULL;
          msg = g_strdup_printf (_("Problem loading puzzle: %s"),
                                                 error -> message);
          root = gtk_widget_get_root (GTK_WIDGET (priv->picker));
          toast_overlay = play_window_get_overlay (PLAY_WINDOW (root));
          adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (toast_overlay),
                                                              adw_toast_new (msg));
        }
    }

  g_clear_object (&priv->downloader_cancellable);
  puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_READY);
}

static void
start_download_cb (PuzzleSet *self)
{
  GtkWindow *parent_window;
  PuzzleSetPrivate *priv;

  g_autoptr (GError) error = NULL;

  priv = puzzle_set_get_instance_private (self);

  g_assert (priv->downloader != NULL);
  g_assert (priv->downloader_cancellable == NULL);

  parent_window = (GtkWindow *) gtk_widget_get_root (priv->picker);
  priv->downloader_cancellable = g_cancellable_new ();
  puzzle_downloader_run_async (priv->downloader,
                               parent_window,
                               priv->downloader_cancellable,
                               &error);
  if (error)
    {
      /* FIXME(error): put up a dialog */
      g_warning ("Could not run downloader: %s\n",
                 error->message);
      puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_DISABLED);
      return;
    }
  else
    {
      puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_DOWNLOADING);
    }
}

/* Public Functions */

PuzzleSetType
puzzle_set_get_puzzle_type (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), PUZZLE_SET_TYPE_RESOURCE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->set_type;
}

const gchar *
puzzle_set_get_id (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_id (priv->config);
}

const gchar *
puzzle_set_get_short_name (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_short_name (priv->config);
}

const gchar *
puzzle_set_get_long_name (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_long_name (priv->config);
}

const gchar *
puzzle_set_get_locale (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_locale (priv->config);
}

const gchar *
puzzle_set_get_language (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), NULL);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_language (priv->config);
}

/* Note: the difference between disabled and shown is that 'disabled'
 * is set in the puzzleset and can't be overridden or change, and
 * 'shown' is a user preference and can change.
 */
gboolean
puzzle_set_get_disabled (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), TRUE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_disabled (priv->config);
}

gboolean
puzzle_set_get_shown (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), TRUE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->shown;
}

void
puzzle_set_set_shown (PuzzleSet *puzzle_set,
                       gboolean   shown)
{
  PuzzleSetPrivate *priv;

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));
  shown = !!shown;

  priv = puzzle_set_get_instance_private (puzzle_set);

  if (priv->shown == shown)
    return;

  priv->shown = shown;
  g_object_notify_by_pspec (G_OBJECT (puzzle_set), obj_props [PROP_SHOWN]);
}

gboolean
puzzle_set_get_auto_download (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), FALSE);

  priv = puzzle_set_get_instance_private (puzzle_set);

  if (puzzle_set_config_get_use_button (priv->config) &&
      puzzle_set_config_get_button_type (priv->config) == CONFIG_BUTTON_DOWNLOADER &&
      puzzle_set_config_get_downloader_type (priv->config) == CONFIG_DOWNLOADER_AUTO)
    return TRUE;

  return FALSE;
}

ConfigSetTags
puzzle_set_get_tags (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), 0);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return puzzle_set_config_get_tags (priv->config);
}

void
puzzle_set_puzzles_start (PuzzleSet *puzzle_set)
{
  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[PUZZLES_START], 0);
}

void
puzzle_set_change_phase (PuzzleSet   *puzzle_set,
                         PuzzlePhase  phase)
{
  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[CHANGE_PHASE], 0, phase);
}

void
puzzle_set_puzzles_done (PuzzleSet *puzzle_set)
{

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[PUZZLES_DONE], 0);
}

void
puzzle_set_reveal_canceled (PuzzleSet *puzzle_set)
{

  g_return_if_fail (PUZZLE_IS_SET (puzzle_set));

  g_signal_emit (puzzle_set, puzzle_set_signals[REVEAL_CANCELED], 0);
}


GtkWidget *
puzzle_set_get_widget (PuzzleSet   *self,
                       PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (self), NULL);

  priv = puzzle_set_get_instance_private (self);

  if (phase == PUZZLE_PHASE_PICKER)
    return priv->picker;
  else if (phase == PUZZLE_PHASE_GAME)
    return priv->xword;

  return NULL;

}

IPuzPuzzle *
puzzle_set_get_puzzle (PuzzleSet   *self,
                       PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (self), NULL);

  priv = puzzle_set_get_instance_private (self);
  if (priv->xword)
    return play_xword_get_puzzle (PLAY_XWORD (priv->xword));

  return NULL;
}

const gchar *
puzzle_set_get_title (PuzzleSet   *self,
                      PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;
  const char *title = NULL;

  g_return_val_if_fail (PUZZLE_IS_SET (self), NULL);
  g_return_val_if_fail (phase != PUZZLE_PHASE_MAIN, NULL);

  priv = puzzle_set_get_instance_private (self);

  if (phase == PUZZLE_PHASE_PICKER)
    title = puzzle_picker_get_title (PUZZLE_PICKER (priv->picker));
  else if (phase == PUZZLE_PHASE_GAME)
    title = play_xword_get_title (PLAY_XWORD (priv->xword));

  if (title == NULL)
    title = puzzle_set_config_get_short_name (priv->config);

  return title;
}

void
puzzle_set_reset_puzzle (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  g_return_if_fail (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  /* FIXME(game): Make PlayXword an interface so we can have multiple types */
  if (PLAY_IS_XWORD (priv->xword))
    play_xword_clear_puzzle (PLAY_XWORD (priv->xword));
}

void
puzzle_set_reveal_toggled (PuzzleSet *self,
                           gboolean   reveal)
{
  PuzzleSetPrivate *priv;

  g_return_if_fail (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  /* FIXME(game): Make PlayXword an interface so we can have multiple types */
  if (PLAY_IS_XWORD (priv->xword))
    play_xword_set_reveal_mode (PLAY_XWORD (priv->xword),
                                reveal ? GRID_REVEAL_ERRORS_BOARD : GRID_REVEAL_NONE);
}

void
puzzle_set_show_hint (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;

  g_return_if_fail (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  /* FIXME(game): Make PlayXword an interface so we can have multiple types */
  if (PLAY_IS_XWORD (priv->xword))
    play_xword_show_hint (PLAY_XWORD (priv->xword));
}

const gchar *
puzzle_set_get_uri (PuzzleSet   *self,
                    PuzzlePhase  phase)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (self), NULL);

  priv = puzzle_set_get_instance_private (self);

  if (priv->selected_puzzle_name)
    return g_build_filename (g_get_user_data_dir (),
                             "gnome-crosswords",
                             "downloads",
                             puzzle_set_config_get_id (priv->config),
                             priv->selected_puzzle_name,
                             NULL);

  return NULL;
}

/* This is used from command line or dbus calls to load uris. It's
 * different from puzzle_picker->load_uri which is internally driven,
 * and requires a .ipuz file. */
void
puzzle_set_import_uri (PuzzleSet   *self,
                       const gchar *uri)
{
  PuzzleSetPrivate *priv;
  GtkWindow *parent_window;
  g_autoptr (GError) error = NULL;

  g_return_if_fail (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);
  g_return_if_fail (g_strcmp0 ("uri", puzzle_set_get_id (self)) == 0);
  g_return_if_fail (PICKER_IS_LIST (priv->picker));

  if (priv->downloader_cancellable)
    {
      g_warning ("Currently can't import multiple uris simultaneously");
      return;
    }

  parent_window = (GtkWindow *) gtk_widget_get_root (priv->picker);
  puzzle_downloader_add_import_target (priv->downloader, uri);
  puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_DOWNLOADING);
  puzzle_downloader_run_async (priv->downloader,
                               parent_window,
                               priv->downloader_cancellable,
                               &error);
  if (error)
    {
      /* FIXME(error): put up a dialog */
      g_warning ("Could not run downloader: %s",
                 error->message);
      puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_READY);
      return;
    }
}

/*
 * Public functions
 */


static void
network_available_cb (GNetworkMonitor *monitor,
                      gboolean         network_available,
                      PuzzleSet       *puzzle_set)
{
  if (network_available)
    {
    }
  else
    {
    }
}

static void
puzzle_set_set_downloader_state (PuzzleSet       *self,
                                 DownloaderState  state)
{
  PuzzleSetPrivate *priv;

  g_assert (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  if (priv->state == state)
    return;

  /* There's no recovery from a disabled downloader */
  if (priv->state != DOWNLOADER_STATE_DISABLED)
    {
      priv->state = state;
    }

  if (priv->picker)
    puzzle_picker_set_downloader_state (PUZZLE_PICKER (priv->picker),
                                        priv->state);
}

static void
won_changed_cb (PuzzleSet      *self,
                PuzzleSetModel *model)
{
  PuzzleSetPrivate *priv;
  guint n_won;

  priv = puzzle_set_get_instance_private (self);

  n_won = puzzle_set_model_get_n_won (model);

  if (n_won != priv->n_won)
    {
      priv->n_won = n_won;
      g_object_notify_by_pspec (G_OBJECT (self), obj_props [PROP_N_WON]);
    }
}

static void
items_changed_cb (PuzzleSet *self)
{
  PuzzleSetPrivate *priv;
  guint n_puzzles;

  priv = puzzle_set_get_instance_private (self);

  n_puzzles = g_list_model_get_n_items (G_LIST_MODEL (priv->model));

  if (n_puzzles != priv->n_puzzles)
    {
      priv->n_puzzles = n_puzzles;
      g_object_notify_by_pspec (G_OBJECT (self), obj_props [PROP_N_PUZZLES]);
    }
}

static void
puzzle_set_set_config (PuzzleSet       *self,
                       PuzzleSetConfig *config)
{
  PuzzleSetPrivate *priv;
  ConfigButtonType button_type;

  g_assert (PUZZLE_IS_SET (self));

  priv = puzzle_set_get_instance_private (self);

  g_assert (priv->config == NULL);

  priv->config = config;
  priv->model = puzzle_set_model_new (priv->config);
  priv->n_won = puzzle_set_model_get_n_won (priv->model);
  priv->n_puzzles = g_list_model_get_n_items (G_LIST_MODEL (priv->model));
  g_signal_connect_swapped (priv->model, "won-changed", G_CALLBACK (won_changed_cb), self);
  g_signal_connect_swapped (priv->model, "items-changed", G_CALLBACK (items_changed_cb), self);

  button_type = puzzle_set_config_get_button_type (config);

  if (button_type == CONFIG_BUTTON_FILE)
    priv->downloader = puzzle_downloader_new_filechooser ();
  else
    priv->downloader = puzzle_downloader_new_from_config (config);

  /* Everything loaded fine; show the button and listen for changes */
  if (priv->downloader)
    {
      puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_READY);

      /* listen to network changes if we require network */
      if (puzzle_downloader_get_requires_network (priv->downloader))
        {
          GNetworkMonitor *monitor;

          monitor = g_network_monitor_get_default ();
          priv->network_monitor_signal =
            g_signal_connect (monitor,
                              "network-changed",
                              G_CALLBACK (network_available_cb),
                              self);
          network_available_cb (monitor,
                                g_network_monitor_get_network_available (monitor),
                                self);
        }

      g_signal_connect_swapped (priv->downloader, "finished",
                                G_CALLBACK (puzzle_set_downloader_finished),
                                self);
    }
  else
    {
      puzzle_set_set_downloader_state (self, DOWNLOADER_STATE_DISABLED);
    }
}

guint
puzzle_set_get_n_puzzles (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), 0);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->n_puzzles;
}

guint
puzzle_set_get_n_won (PuzzleSet *puzzle_set)
{
  PuzzleSetPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_SET (puzzle_set), 0);

  priv = puzzle_set_get_instance_private (puzzle_set);

  return priv->n_won;
}

/* Can return NULL if resource can't be loaded. */
PuzzleSet *
puzzle_set_new (GResource *resource)
{
  g_autoptr (GError) error = NULL;
  PuzzleSet *puzzle_set;
  PuzzleSetConfig *config;

  puzzle_set = g_object_new (PUZZLE_TYPE_SET,
                             "set-type", PUZZLE_SET_TYPE_RESOURCE,
                             NULL);

  config = puzzle_set_config_new (resource, &error);
  if (error)
    g_warning ("Error loading Puzzle Set\n%s", error->message);

  if (config)
    puzzle_set_set_config (puzzle_set, config);
  else
    g_clear_object (& puzzle_set);

  return puzzle_set;
}
