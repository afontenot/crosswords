/* puzzle-button.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "puzzle-button.h"
#include "puzzle-button-icon.h"

enum
{
  PROP_0,
  PROP_MODE,
  PROP_PUZZLE_ID,
  PROP_PUZZLE,
  PROP_THUMB_PIXBUF,
  PROP_SHOW_PROGRESS,
  PROP_PROGRESS_FRACTION,
  PROP_PROGRESS_SIZE_GROUP,
  N_PROPS
};

enum {
  PUZZLE_CLICKED,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };

struct _PuzzleButton
{
  GtkWidget parent_instance;

  GtkWidget *button;
  PuzzleButtonMode mode;
  guint puzzle_id;
  IPuzPuzzle *puzzle;
  GdkPixbuf *thumb_pixbuf;
  gboolean show_progress;
  gdouble progress_fraction;

  GdkPaintable *solved;
  GtkWidget *image;
  GtkWidget *progress_bar;
  GtkWidget *spacer;
  PuzzleSetModelRow *row;

  gulong row_changed_handler;
  GtkSizeGroup *progress_size_group;
};

static void puzzle_button_init                    (PuzzleButton      *self);
static void puzzle_button_class_init              (PuzzleButtonClass *klass);
static void puzzle_button_set_property            (GObject           *object,
                                                   guint              prop_id,
                                                   const GValue      *value,
                                                   GParamSpec        *pspec);
static void puzzle_button_get_property            (GObject           *object,
                                                   guint              prop_id,
                                                   GValue            *value,
                                                   GParamSpec        *pspec);
static void puzzle_button_dispose                 (GObject           *object);
static void puzzle_button_set_mode                (PuzzleButton      *self,
                                                   PuzzleButtonMode   mode);
static void puzzle_button_set_puzzle              (PuzzleButton      *self,
                                                   IPuzPuzzle        *puzzle);
static void puzzle_button_set_thumb_pixbuf        (PuzzleButton      *self,
                                                   GdkPixbuf         *thumb);
static void puzzle_button_set_progress_size_group (PuzzleButton      *self,
                                                   GtkSizeGroup      *progress_size_group);


G_DEFINE_TYPE (PuzzleButton, puzzle_button, GTK_TYPE_WIDGET);


static void
puzzle_button_init (PuzzleButton *self)
{
  self->puzzle_id = 0;
  self->mode = PUZZLE_BUTTON_MODE_NORMAL;
  self->show_progress = FALSE;
  self->progress_fraction = 0.0;
}

static void
puzzle_button_class_init (PuzzleButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = puzzle_button_set_property;
  object_class->get_property = puzzle_button_get_property;
  object_class->dispose = puzzle_button_dispose;

  obj_signals[PUZZLE_CLICKED] =
    g_signal_new ("puzzle-clicked",
                  PUZZLE_TYPE_BUTTON,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT,
                  PUZZLE_TYPE_BUTTON_MODE);

  obj_props[PROP_MODE] =
    g_param_spec_enum ("mode",
                       "Mode",
                       "How the grid button should display itself",
                       PUZZLE_TYPE_BUTTON_MODE,
                       PUZZLE_BUTTON_MODE_NORMAL,
                       G_PARAM_READWRITE);

  obj_props[PROP_PUZZLE_ID] =
    g_param_spec_uint ("puzzle-id",
                       "Puzzle ID",
                       "An identifier of the buttons puzzle",
                       0, G_MAXUINT16, 0,
                       G_PARAM_READWRITE);

  obj_props[PROP_PUZZLE] =
    g_param_spec_object ("puzzle",
                         "Puzzle",
                         "An IPuzPuzzle to thumbnail",
                         IPUZ_TYPE_PUZZLE,
                         G_PARAM_READWRITE);

  obj_props[PROP_THUMB_PIXBUF] =
    g_param_spec_object ("thumb-pixbuf",
                         "Thumb Pixbuf",
                         "gdk-pixbuf with the thumbnail of the puzzle",
                         GDK_TYPE_PIXBUF,
                         G_PARAM_READWRITE);

  obj_props[PROP_SHOW_PROGRESS] =
    g_param_spec_boolean ("show-progress",
                          "Show Progress",
                          "Whether or not to show a progress bar",
                          TRUE,
                          G_PARAM_READWRITE);

  obj_props[PROP_PROGRESS_FRACTION] =
    g_param_spec_double ("progress-fraction",
                         "Progress Fraction",
                         "Percentage of the puzzle that's been filled by the user",
                         0.0, 1.0,
                         0.0,
                         G_PARAM_READWRITE);

  obj_props[PROP_PROGRESS_SIZE_GROUP] =
    g_param_spec_object ("progress-size-group",
                         "Progress Size Group",
                         "A shared size group to keep the progress bars the same size.",
                         GTK_TYPE_SIZE_GROUP,
                         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (GTK_WIDGET_CLASS (klass), "puzzlebutton");
  gtk_widget_class_set_layout_manager_type (GTK_WIDGET_CLASS (klass),
                                            GTK_TYPE_BIN_LAYOUT);
}

static void
puzzle_button_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  PuzzleButton *self;

  self = (PuzzleButton *) object;

  switch (prop_id)
    {
    case PROP_MODE:
      puzzle_button_set_mode (self, g_value_get_enum (value));
      break;
    case PROP_PUZZLE_ID:
      self->puzzle_id = g_value_get_uint (value);
      break;
    case PROP_PUZZLE:
      puzzle_button_set_puzzle (self, (IPuzPuzzle *) g_value_get_object (value));
      break;
    case PROP_THUMB_PIXBUF:
      puzzle_button_set_thumb_pixbuf (self, (GdkPixbuf *) g_value_get_object (value));
      break;
    case PROP_SHOW_PROGRESS:
      self->show_progress = g_value_get_boolean (value);
      gtk_widget_set_visible (self->progress_bar, self->show_progress);
      gtk_widget_set_visible (self->spacer, !self->show_progress);
      break;
    case PROP_PROGRESS_FRACTION:
      self->progress_fraction = g_value_get_double (value);
      gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (self->progress_bar), self->progress_fraction);
      break;
    case PROP_PROGRESS_SIZE_GROUP:
      puzzle_button_set_progress_size_group (self, (GtkSizeGroup *) g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_button_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  PuzzleButton *self;

  self = (PuzzleButton *) object;

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    case PROP_PUZZLE_ID:
      g_value_set_uint (value, self->puzzle_id);
      break;
    case PROP_PUZZLE:
      g_value_set_object (value, self->puzzle);
      break;
    case PROP_THUMB_PIXBUF:
      g_value_set_object (value, self->thumb_pixbuf);
      break;
    case PROP_SHOW_PROGRESS:
      g_value_set_boolean (value, self->show_progress);
      break;
    case PROP_PROGRESS_FRACTION:
      g_value_set_double (value, self->progress_fraction);
      break;
    case PROP_PROGRESS_SIZE_GROUP:
      g_value_set_object (value, self->progress_size_group);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_button_dispose (GObject *object)
{
  PuzzleButton *self;
  GtkWidget *child;

  self = PUZZLE_BUTTON (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_signal_handler (&self->row_changed_handler,
                          self->row);

  g_clear_object (&self->thumb_pixbuf);
  g_clear_object (&self->puzzle);
  g_clear_object (&self->solved);

  G_OBJECT_CLASS (puzzle_button_parent_class)->dispose (object);
}

static void
puzzle_button_set_mode (PuzzleButton     *self,
                        PuzzleButtonMode  mode)
{
  self->mode = mode;
  if (self->mode == PUZZLE_BUTTON_MODE_LOCKED)
    gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);
  else
    gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);

  if (self->mode == PUZZLE_BUTTON_MODE_LOCKED)
    gtk_image_set_from_icon_name (GTK_IMAGE (self->image),
                                 "crossword-locked-symbolic");
  else if (self->mode == PUZZLE_BUTTON_MODE_NORMAL)
    {
      /* fixme: We should make the thumb a Paintable longer term */
      g_autoptr (GdkTexture) texture = NULL;
      texture = gdk_texture_new_for_pixbuf (self->thumb_pixbuf);
      gtk_image_set_from_paintable (GTK_IMAGE (self->image),
                                    GDK_PAINTABLE (texture));
    }
  else if (self->mode == PUZZLE_BUTTON_MODE_SOLVED)
    gtk_image_set_from_paintable (GTK_IMAGE (self->image),
                                  GDK_PAINTABLE (self->solved));
}

static void
puzzle_button_set_puzzle (PuzzleButton *self,
                          IPuzPuzzle   *puzzle)
{
  GdkPixbuf *pixbuf;

  if (puzzle)
    g_object_ref (puzzle);
  g_clear_object (&self->puzzle);

  self->puzzle = puzzle;

  pixbuf = xwd_thumbnail_puzzle (puzzle);
  puzzle_button_set_thumb_pixbuf (self, pixbuf);
  g_object_unref (pixbuf);
}


static void
puzzle_button_set_thumb_pixbuf (PuzzleButton *self,
                                GdkPixbuf    *thumb_pixbuf)
{
  g_autoptr (GdkTexture) texture = NULL;
  if (thumb_pixbuf)
    {
      g_object_ref (thumb_pixbuf);
      texture = gdk_texture_new_for_pixbuf (thumb_pixbuf);
    }

  g_clear_object (&self->thumb_pixbuf);
  g_clear_object (&self->solved);

  self->thumb_pixbuf = thumb_pixbuf;
  self->solved = puzzle_button_icon_new (thumb_pixbuf);

  if (self->mode == PUZZLE_BUTTON_MODE_NORMAL)
    gtk_image_set_from_paintable (GTK_IMAGE (self->image),
                                  GDK_PAINTABLE (texture));
  else if (self->mode == PUZZLE_BUTTON_MODE_SOLVED)
    gtk_image_set_from_paintable (GTK_IMAGE (self->image),
                                  GDK_PAINTABLE (self->solved));
}

static void
puzzle_button_set_progress_size_group (PuzzleButton *self,
                                       GtkSizeGroup *progress_size_group)
{
  if (self->progress_size_group)
    {
      gtk_size_group_remove_widget (self->progress_size_group,
                                    self->progress_bar);
      gtk_size_group_remove_widget (self->progress_size_group,
                                    self->spacer);
    }
  self->progress_size_group = progress_size_group;
  if (self->progress_size_group)
    {
      gtk_size_group_add_widget (self->progress_size_group,
                                 self->progress_bar);
      gtk_size_group_add_widget (self->progress_size_group,
                                 self->spacer);
    }
}

/* Public methods */

static void
clicked_cb (PuzzleButton *puzzle_button)
{
  g_signal_emit (G_OBJECT (puzzle_button), obj_signals[PUZZLE_CLICKED], 0,
                 puzzle_button->puzzle_id,
                 puzzle_button->mode);
}

static void
add_button_contents (PuzzleButton *self)
{
  GtkWidget *box;

  box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 8);
  gtk_widget_set_margin_top (box, 6);
  gtk_widget_set_margin_bottom (box, 6);
  gtk_widget_set_margin_start (box, 6);
  gtk_widget_set_margin_end (box, 6);

  self->image = gtk_image_new ();
  gtk_image_set_pixel_size (GTK_IMAGE (self->image), 150);
  gtk_widget_set_vexpand (self->image, TRUE);
  gtk_widget_set_margin_top (self->image, 8);
  gtk_box_prepend (GTK_BOX (box), self->image);

  /* This is a little confusing. We pack the progress bar and an empty
   * box into the end of the button vbox. Only one of the two of them
   * is visible at any time. If a size_group is set, then they're both
   * added, and we can reserve space for the spacer when its hidden
   */
  self->progress_bar = gtk_progress_bar_new();
  gtk_widget_set_vexpand (self->progress_bar, FALSE);
  self->spacer = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_widget_set_visible (self->spacer, FALSE);

  gtk_widget_set_visible (self->progress_bar, self->show_progress);
  gtk_widget_set_visible (self->spacer, !self->show_progress);

  gtk_box_append (GTK_BOX (box), self->progress_bar);
  gtk_box_append (GTK_BOX (box), self->spacer);

  gtk_button_set_child (GTK_BUTTON (self->button), box);
}

static void
percent_changed_cb (PuzzleSetModelRow *row,
                    GParamSpec        *pspec,
                    PuzzleButton      *puzzle_button)
{
  g_object_set (G_OBJECT (puzzle_button),
                "progress-fraction", puzzle_set_model_row_get_percent (row),
                NULL);
}

GtkWidget *
puzzle_button_new (PuzzleSetModelRow *row)
{
  PuzzleButton *puzzle_button;

  puzzle_button = g_object_new (PUZZLE_TYPE_BUTTON,
                                NULL);

  /* I don't think we need to ref this. We just keep it around to
   * disconnect the signal in dispose() */
  puzzle_button->row = row;
  puzzle_button->button = gtk_button_new ();
  gtk_widget_add_css_class (puzzle_button->button, "card");

  g_signal_connect_swapped (puzzle_button->button,
                            "clicked", G_CALLBACK (clicked_cb),
                            puzzle_button);
  add_button_contents (puzzle_button);
  puzzle_button_set_puzzle (puzzle_button, puzzle_set_model_row_get_puzzle (row));
  puzzle_button->row_changed_handler =
    g_signal_connect (row, "changed::percent", G_CALLBACK (percent_changed_cb), puzzle_button);
  gtk_widget_set_parent (puzzle_button->button, GTK_WIDGET (puzzle_button));

  return GTK_WIDGET (puzzle_button);
}
