/* edit-histogram.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>


G_BEGIN_DECLS


typedef enum
{
  EDIT_HISTOGRAM_MODE_LETTERS,
  EDIT_HISTOGRAM_MODE_CLUELEN,
} EditHistogramMode;


#define EDIT_TYPE_HISTOGRAM (edit_histogram_get_type())
G_DECLARE_FINAL_TYPE (EditHistogram, edit_histogram, EDIT, HISTOGRAM, GtkWidget);


void edit_histogram_update (EditHistogram  *self,
                            IPuzPuzzleInfo *info);


G_END_DECLS
