/* edit-window-controls.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file is a part of the EditWindow widget and is not a
 * standalone object. Instead, it is meant to encapsulate all code
 * related to PanelWidget flows in one separate file for convenience.
 *
 * BE CAREFUL TOUCING THIS FILE. IT IS A COMPLEXITY SYNC
 */



#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "cell-preview.h"
#include "edit-bars.h"
#include "edit-cell.h"
#include "edit-clue-details.h"
#include "edit-clue-list.h"
#include "edit-clue-info.h"
#include "edit-grid.h"
#include "edit-grid-info.h"
#include "edit-histogram.h"
#include "edit-metadata.h"
#include "edit-shapebg-row.h"
#include "edit-state.h"
#include "edit-symmetry.h"
#include "edit-window.h"
#include "edit-window-private.h"
#include "edit-word-list.h"
#include "word-list.h"

/* update_* functions. These are for propagating the state from a new
   state to all the widgets in the editor. They don't touch the
   current state . */
static void update_all                                (EditWindow        *self);
static void update_grid_cursor_moved                  (EditWindow        *self);
static void update_clues_cursor_moved                 (EditWindow        *self);
static void update_clues_selection_changed            (EditWindow        *self);
static void update_style_cursor_moved                 (EditWindow        *self);

/* State change functions. These are called after a change to the
 * puzzle. They modify the internal state to represent the new
 * state. */
static void pushed_puzzle_new                         (EditWindow        *self);
static void pushed_grid_changed                       (EditWindow        *self);
static void pushed_state_changed                      (EditWindow        *self);
static void pushed_metadata_changed                   (EditWindow        *self);

/* Callbacks from widget. These handle individual change requests from
 * a widget. They are responsible for pushing the change (if
 * necessary, and calling appropriate update functions. */
static void symmetry_widget_symmetry_changed_cb       (EditWindow        *self,
                                                       IPuzSymmetry       symmetry);
static void main_grid_guess_cb                        (EditWindow        *self,
                                                       const gchar       *guess);
static void main_grid_guess_at_cell_cb                (EditWindow        *self,
                                                       const gchar       *guess,
                                                       IPuzCellCoord     *coord);
static void main_grid_do_command_cb                   (EditWindow        *self,
                                                       GridCmdKind        kind);
static void main_grid_copy_cb                         (EditWindow        *self);
static void main_grid_paste_cb                        (EditWindow        *self,
                                                       const gchar       *paste);
static void main_grid_cell_selected_cb                (EditWindow        *self,
                                                       IPuzCellCoord     *coord);
static void word_list_widget_word_activated_cb        (EditWindow        *self,
                                                       IPuzClueDirection  direction,
                                                       const gchar       *word,
                                                       const gchar       *enumeration_src);
static void clue_list_widget_clue_selected_cb         (EditWindow        *self,
                                                       IPuzClueId        *clue_id);
static void clue_details_widget_clue_changed_cb       (EditWindow        *self,
                                                       const gchar       *new_clue,
                                                       IPuzEnumeration   *new_enumeration);
static void clue_details_widget_selection_changed_cb  (EditWindow        *self,
                                                       const gchar       *selection_text);
static void edit_metadata_metadata_changed_cb         (EditWindow        *self,
                                                       gchar             *property,
                                                       gchar             *value);
static void edit_metadata_showenumerations_changed_cb (EditWindow        *self,
                                                       gboolean           showenumerations);

static void
update_color_rows (EditWindow *self)
{
  IPuzCell *cell;
  IPuzStyle *style;
  const gchar *color_str;

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  self->edit_state->style_state->cursor);
  g_assert (cell);

  style = ipuz_cell_get_style (cell);

  if (style)
    color_str = ipuz_style_get_text_color (style);
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_foreground_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    {
      g_object_set (self->style_foreground_color_row,
                    "current-color", NULL,
                    NULL);
    }

  if (style)
    color_str = ipuz_style_get_bg_color (style);
  else
    color_str = NULL;
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_background_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    g_object_set (self->style_background_color_row,
                  "current-color", NULL,
                  NULL);
}

static void
update_all (EditWindow *self)
{
  IPuzPuzzle *puzzle;
  //GTimer *timer;

  //timer = g_timer_new ();
  //g_timer_start (timer);

  VALIDATE_EDIT_STATE (self->edit_state);
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  /* recalculate the info for the puzzle */
  g_clear_object (&self->edit_state->info);
  self->edit_state->info = ipuz_puzzle_get_info (puzzle);

  switch (self->edit_state->stage)
    {
    case EDIT_STAGE_GRID:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->grid_state);
      edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
      cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                       self->edit_state->grid_state,
                                       self->edit_state->sidebar_logo_config);
      edit_cell_update (EDIT_CELL (self->cell_widget), self->edit_state->grid_state, self->edit_state->sidebar_logo_config);
      edit_grid_info_update (EDIT_GRID_INFO (self->edit_grid_widget), self->edit_state->grid_state, self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->letter_histogram_widget), self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->cluelen_histogram_widget), self->edit_state->info);
      break;
    case EDIT_STAGE_CLUES:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->clues_state);
      edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                                self->edit_state->clues_state,
                                self->edit_state->sidebar_logo_config);
      edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), self->edit_state->clues_state);
      break;
    case EDIT_STAGE_STYLE:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->style_state);
      edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), self->edit_state->style_state);
      update_color_rows (self);
      cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                       self->edit_state->style_state,
                                       self->edit_state->sidebar_logo_config);

      break;
    case EDIT_STAGE_METADATA:
      edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
      break;
    default:
      g_assert_not_reached ();
    }

  //g_timer_stop (timer);
  //g_print ("update_all: Total time %f\n", g_timer_elapsed (timer, NULL));
  //g_timer_destroy (timer);
}

static void
update_grid_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_GRID)
    return;

  cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                   self->edit_state->grid_state,
                                   self->edit_state->sidebar_logo_config);
  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->grid_state);
  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
  edit_cell_update (EDIT_CELL (self->cell_widget), self->edit_state->grid_state, self->edit_state->sidebar_logo_config);
}

static void
update_clues_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_CLUES)
    return;

  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->clues_state);
  edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                            self->edit_state->clues_state,
                            self->edit_state->sidebar_logo_config);
  edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), self->edit_state->clues_state);
}

static void
update_style_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_STYLE)
    return;

  cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                   self->edit_state->style_state,
                                   self->edit_state->sidebar_logo_config);
  edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), self->edit_state->style_state);
  update_color_rows (self);
  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->style_state);
}

static void
update_clues_selection_changed (EditWindow *self)
{
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_anagram),
                         self->edit_state->clue_selection_text);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_odd),
                         self->edit_state->clue_selection_text);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_even),
                         self->edit_state->clue_selection_text);
}

/* This is called when we clear the stack and push a new puzzle onto
 * it. */
static void
pushed_puzzle_new (EditWindow *self)
{
  IPuzPuzzle *puzzle;
  IPuzSymmetry symmetry;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  g_clear_pointer (&self->edit_state, edit_state_free);
  self->edit_state = edit_state_new (puzzle);

  /* Update widgets */
  symmetry = crosswords_quirks_get_symmetry (self->edit_state->quirks);
  edit_symmetry_set_symmetry (EDIT_SYMMETRY (self->symmetry_widget), symmetry, self->edit_state->sidebar_logo_config);

  update_all (self);
}

static void
pushed_grid_changed (EditWindow *self)
{
  IPuzPuzzle *puzzle;

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_swap_xword (self->edit_state->grid_state,
                                               IPUZ_CROSSWORD (puzzle)));
  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_swap_xword (self->edit_state->clues_state,
                                               IPUZ_CROSSWORD (puzzle)));
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_swap_xword (self->edit_state->style_state,
                                               IPUZ_CROSSWORD (puzzle)));

  update_all (self);
}

static void
pushed_state_changed (EditWindow *self)
{
  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
}

static void
pushed_metadata_changed (EditWindow *self)
{
  IPuzPuzzle *puzzle;

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
}

/*
 * Callbacks
 */

static void
symmetry_widget_symmetry_changed_cb (EditWindow   *self,
                                     IPuzSymmetry  symmetry)
{
  crosswords_quirks_set_symmetry (self->edit_state->quirks,
                                  symmetry);
}

static void
edit_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess (self->edit_state->grid_state, guess));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess_at_cell (self->edit_state->grid_state, guess, *coord));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_grid_do_command_cb (EditWindow   *self,
                         GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_do_command (self->edit_state->grid_state, kind));

  /* FIXME(undo): this will trigger even if nothing has changed, such
   * as backspace of an empty cell. We should check first*/
  if (CMD_KIND_DESTRUCTIVE (kind))
    puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                              IPUZ_PUZZLE (self->edit_state->grid_state->xword));
  else
    update_grid_cursor_moved (self);
}

static void
edit_grid_cell_selected_cb (EditWindow    *self,
                            IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_cell_selected (self->edit_state->grid_state, *coord));

  update_grid_cursor_moved (self);
}

static void
edit_grid_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->grid_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->grid_state->xword,
                                              self->edit_state->grid_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  IPuzClueDirection direction;

  direction = self->edit_state->grid_state->clue.direction;
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;

  self->edit_state->grid_state = grid_state_replace (self->edit_state->grid_state,
                                                     grid_state_guess_word (self->edit_state->grid_state,
                                                                            self->edit_state->grid_state->cursor,
                                                                            direction, str));
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_clues_cell_selected_cb (EditWindow    *self,
                             IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_cell_selected (self->edit_state->clues_state, *coord));

  update_clues_cursor_moved (self);
}

static void
edit_clues_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->clues_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->clues_state->xword,
                                              self->edit_state->clues_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}


static void
edit_clues_do_command_cb (EditWindow  *self,
                          GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_do_command (self->edit_state->clues_state, kind));

  update_clues_cursor_moved (self);
}

static void
edit_style_cell_selected_cb (EditWindow    *self,
                             IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_cell_selected (self->edit_state->style_state, *coord));

  update_style_cursor_moved (self);
}

static void
edit_style_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->style_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->style_state->xword,
                                              self->edit_state->style_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_style_do_command_cb (EditWindow  *self,
                          GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_do_command (self->edit_state->style_state, kind));

  update_style_cursor_moved (self);
}

static void
main_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_cb (self, guess);
}

static void
main_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_at_cell_cb (self, guess, coord);
}

static void
main_grid_do_command_cb (EditWindow  *self,
                         GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_do_command_cb (self, kind);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_do_command_cb (self, kind);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_do_command_cb (self, kind);
}

static void
main_grid_cell_selected_cb (EditWindow    *self,
                            IPuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_cell_selected_cb (self, coord);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_cell_selected_cb (self, coord);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_cell_selected_cb (self, coord);
}

static void
main_grid_copy_cb (EditWindow *self)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_copy_cb (self);
}

static void
main_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_paste_cb (self, str);
}

static void
word_list_widget_word_activated_cb (EditWindow        *self,
                                    IPuzClueDirection  direction,
                                    const gchar       *word,
                                    const gchar       *enumeration_src)
{
  IPuzClue *clue;
  const GArray *coords;
  IPuzCellCoord coord;
  g_autoptr (IPuzEnumeration) enumeration = NULL;

  clue = ipuz_crossword_find_clue_by_coord (self->edit_state->grid_state->xword,
                                            direction,
                                            self->edit_state->grid_state->cursor);

  /* Some sanity checking. May fail in raw mode in the future */
  g_assert (clue != NULL);

  /* Update the enumeration */
  if (enumeration_src)
    {
      enumeration = ipuz_enumeration_new (enumeration_src, IPUZ_VERBOSITY_STANDARD);
    }
  ipuz_clue_set_enumeration (clue, enumeration);
  ipuz_crossword_fix_enumerations (self->edit_state->grid_state->xword);

  coords = ipuz_clue_get_cells (clue);
  g_assert (coords != NULL && coords->len > 0);

  coord = g_array_index (coords, IPuzCellCoord, 0);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess_word (self->edit_state->grid_state,
                                               coord,
                                               direction, word));
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
cell_widget_side_changed_cb (EditWindow     *self,
                             IPuzStyleSides  side)
{
  IPuzSymmetry symmetry;
  IPuzStyleSides new_sides = 0;

  g_assert (IPUZ_IS_BARRED (self->edit_state->grid_state->xword));

  symmetry = crosswords_quirks_get_symmetry (self->edit_state->grid_state->quirks);

  new_sides = ipuz_barred_calculate_side_toggle (IPUZ_BARRED (self->edit_state->grid_state->xword),
                                                 self->edit_state->grid_state->cursor,
                                                 side,
                                                 symmetry);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_bars (self->edit_state->grid_state,
                                             self->edit_state->grid_state->cursor,
                                             new_sides, TRUE));

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
cell_widget_cell_type_changed_cb (EditWindow       *self,
                                  IPuzCellCellType  cell_type)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_cell_type (self->edit_state->grid_state,
                                                    self->edit_state->grid_state->cursor,
                                                    cell_type));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
cell_widget_initial_val_changed_cb (EditWindow *self,
                                    gboolean    use_initial_val)
{
  IPuzCell *cell;
  const gchar *str;

  VALIDATE_EDIT_STATE (self->edit_state);

  cell = ipuz_crossword_get_cell (self->edit_state->grid_state->xword,
                                  self->edit_state->grid_state->cursor);
  g_assert (cell != NULL);

  if (use_initial_val)
    {
      str = ipuz_cell_get_solution (cell);
      g_return_if_fail (str != NULL);
      ipuz_cell_set_initial_val (cell, str);
      ipuz_cell_set_solution (cell, NULL);
    }
  else
    {
      str = ipuz_cell_get_initial_val (cell);
      g_return_if_fail (str != NULL);
      ipuz_cell_set_solution (cell, str);
      ipuz_cell_set_initial_val (cell, NULL);
    }

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
clue_details_widget_clue_changed_cb (EditWindow      *self,
                                     const gchar     *new_clue_text,
                                     IPuzEnumeration *new_enumeration)
{
  IPuzClue *clue;

  clue = ipuz_crossword_get_clue_by_id (self->edit_state->clues_state->xword,
                                        self->edit_state->clues_state->clue);

  g_return_if_fail (clue != NULL);

  ipuz_clue_set_clue_text (clue, new_clue_text);
  ipuz_clue_set_enumeration (clue, new_enumeration);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->clues_state->xword));
}

static void
clue_details_widget_selection_changed_cb (EditWindow  *self,
                                          const gchar *selection_text)
{
  g_clear_pointer (&self->edit_state->clue_selection_text, g_free);
  self->edit_state->clue_selection_text = g_strdup (selection_text);
  update_clues_selection_changed (self);
}

static void
clue_list_widget_clue_selected_cb (EditWindow *self,
                                   IPuzClueId *clue_id)
{
  IPuzClue *clue;

  VALIDATE_EDIT_STATE (self->edit_state);
  g_assert (clue_id);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  clue = ipuz_crossword_get_clue_by_id (self->edit_state->clues_state->xword, *clue_id);
  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_clue_selected (self->edit_state->clues_state, clue));

  update_clues_cursor_moved (self);
}

static void
style_shapebg_changed_cb (EditWindow     *self,
                          IPuzStyleShape  shapebg)
{
  IPuzCell *cell;
  IPuzStyle *style;

  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_shapebg (style, shapebg);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
style_foreground_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IPuzCell *cell;
  IPuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  color_str = gdk_rgba_to_string (color);
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_text_color (style, color_str);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
style_background_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IPuzCell *cell;
  IPuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  color_str = gdk_rgba_to_string (color);
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_bg_color (style, color_str);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
edit_metadata_metadata_changed_cb (EditWindow *self,
                                   gchar      *property,
                                   gchar      *value)
{
  IPuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_set (puzzle,
                property, value,
                NULL);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_METADATA,
                            puzzle);
}

static void
edit_metadata_showenumerations_changed_cb (EditWindow *self,
                                           gboolean    showenumerations)
{
  IPuzPuzzle *puzzle;
  gboolean old_showenumerations = FALSE;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_get (puzzle,
                "showenumerations", &showenumerations,
                NULL);

  if (old_showenumerations ^ showenumerations)
    {
      g_object_set (puzzle,
                    "showenumerations", showenumerations,
                    NULL);
      ipuz_crossword_fix_enumerations (IPUZ_CROSSWORD (puzzle));
      /* We need force the grid to redraw itself, as enumerations show
       * up visibly there. */
      puzzle_stack_push_change (self->puzzle_stack,
                                STACK_CHANGE_GRID,
                                puzzle);
    }
}

void
_edit_window_puzzle_changed_cb (EditWindow            *self,
                                gboolean               new_frame,
                                PuzzleStackChangeType  change_type,
                                PuzzleStack           *puzzle_stack)
{
  IPuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  if (puzzle == NULL)
    return;


  if (new_frame)
    {
      if (change_type == STACK_CHANGE_PUZZLE)
        pushed_puzzle_new (self);
      else if (change_type == STACK_CHANGE_GRID)
        pushed_grid_changed (self);
      else if (change_type == STACK_CHANGE_STATE)
        pushed_state_changed (self);
      else if (change_type == STACK_CHANGE_METADATA)
        pushed_metadata_changed (self);

      edit_state_save_to_stack (self->edit_state, self->puzzle_stack);
    }
  else
    {
      /* It's not really possible to optimize this. We restore the
       * state to where it was, and reload everything */
      edit_state_restore_from_stack (self->edit_state, self->puzzle_stack);
      update_all (self);
    }
}

void
_edit_window_class_controls_init (GtkWidgetClass *widget_class)
{
  g_assert (EDIT_IS_WINDOW_CLASS (widget_class));

  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_copy_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_paste_cb);
  gtk_widget_class_bind_template_callback (widget_class, word_list_widget_word_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_side_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_cell_type_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_initial_val_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, symmetry_widget_symmetry_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_clue_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_selection_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_list_widget_clue_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_shapebg_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_foreground_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_background_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_metadata_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_showenumerations_changed_cb);
}

void
_edit_window_control_update_stage (EditWindow *self)
{
  g_assert (EDIT_IS_WINDOW (self));

  g_object_set (self->main_grid,
                "stage", self->edit_state->stage,
                NULL);
  update_all (self);
}

