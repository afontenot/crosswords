/* cell-preview.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-layout.h"
#include "grid-state.h"

G_BEGIN_DECLS


#define CELL_TYPE_PREVIEW (cell_preview_get_type())
G_DECLARE_FINAL_TYPE (CellPreview, cell_preview, CELL, PREVIEW, GtkWidget);


void cell_preview_update_from_cursor (CellPreview  *self,
                                      GridState    *state,
                                      LayoutConfig  config);


G_END_DECLS
