/* puzzle-set-model.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include "puzzle-set-config.h"


G_BEGIN_DECLS


typedef enum
{
  PUZZLE_SET_MODEL_FILTER_SOLVED = 1 << 0, /* @ Filter out solved puzzles @ */
} PuzzleSetModelFilterType;


#define PUZZLE_TYPE_SET_MODEL (puzzle_set_model_get_type())
G_DECLARE_FINAL_TYPE (PuzzleSetModel, puzzle_set_model, PUZZLE, SET_MODEL, GObject);

#define PUZZLE_TYPE_SET_MODEL_ROW (puzzle_set_model_row_get_type())
G_DECLARE_FINAL_TYPE (PuzzleSetModelRow, puzzle_set_model_row, PUZZLE, SET_MODEL_ROW, GObject);

#define PUZZLE_TYPE_SET_MODEL_FILTER (puzzle_set_model_filter_get_type())
G_DECLARE_FINAL_TYPE (PuzzleSetModelFilter, puzzle_set_model_filter, PUZZLE, SET_MODEL_FILTER, GtkFilter);

typedef void (*ModelImportDoneFunc) (PuzzleSetModelRow *row,
                                     gpointer           user_data);


/* PuzzleSetModel */
PuzzleSetModel *puzzle_set_model_new                         (PuzzleSetConfig           *config);
gboolean        puzzle_set_model_find                        (PuzzleSetModel            *self,
                                                              PuzzleSetModelRow         *row,
                                                              guint                     *index);
gboolean        puzzle_set_model_find_by_puzzle_name         (PuzzleSetModel            *self,
                                                              const gchar               *puzzle_name,
                                                              guint                     *index);
IPuzPuzzle     *puzzle_set_model_puzzle_by_puzzle_name       (PuzzleSetModel            *self,
                                                              const gchar               *puzzle_name);
void            puzzle_set_model_reload                      (PuzzleSetModel            *self);
void            puzzle_set_model_import_puzzle_path          (PuzzleSetModel            *self,
                                                              const gchar               *puzzle_path,
                                                              gboolean                   delete_when_done,
                                                              ModelImportDoneFunc        import_done_func,
                                                              gpointer                   user_data,
                                                              GDestroyNotify             destroy_func,
                                                              GError                   **error);
void            puzzle_set_model_remove_puzzle               (PuzzleSetModel            *self,
                                                              const gchar               *puzzle_name);
guint           puzzle_set_model_get_n_won                   (PuzzleSetModel            *self);
void            puzzle_set_model_print                       (PuzzleSetModel            *self);


/* PuzzleSetModelRow */
IPuzPuzzle     *puzzle_set_model_row_get_puzzle              (PuzzleSetModelRow         *row);
const gchar    *puzzle_set_model_row_get_puzzle_name         (PuzzleSetModelRow         *row);
const gchar    *puzzle_set_model_row_get_last_saved_checksum (PuzzleSetModelRow         *row);
gboolean        puzzle_set_model_row_get_readonly            (PuzzleSetModelRow         *row);
void            puzzle_set_model_row_set_guesses             (PuzzleSetModelRow         *row,
                                                              IPuzGuesses               *guesses);

/* Two convenience functions */
gfloat          puzzle_set_model_row_get_percent             (PuzzleSetModelRow         *row);
gboolean        puzzle_set_model_row_won                     (PuzzleSetModelRow         *row);

/* PuzzleSetModelFilter */
GtkFilter      *puzzle_set_model_filter_new                  (PuzzleSetModelFilterType   type);


G_END_DECLS
