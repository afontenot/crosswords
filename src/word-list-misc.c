/* word-list-misc.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>
#include "word-list.h"
#include "word-list-misc.h"
#include "word-list-model.h"

/* Word Index */

WordIndex *
word_index_copy (WordIndex *word_index)
{
  WordIndex *dest;

  dest = g_new0 (WordIndex, 1);
  *dest = *word_index;

  return dest;
}

void
word_index_free (WordIndex *word_index)
{
  g_free (word_index);
}

/* Word Array */

static gint
word_array_cmp (gconstpointer a,
                gconstpointer b)
{
  WordIndex *word_a = (WordIndex *) a;
  WordIndex *word_b = (WordIndex *) b;

  if (word_a->length == word_b->length)
    return word_a->index - word_b->index;
  return word_a->length - word_b->length;
}

WordArray *
word_array_new (void)
{
  return (WordArray *) g_array_new (FALSE, FALSE, sizeof (WordIndex));
}

WordArray *
word_array_copy (WordArray *src)
{
  WordArray *dest;

  g_return_val_if_fail (src != NULL, NULL);

  dest = word_array_new ();

  g_array_set_size (dest, word_array_len (src));
  memcpy (((GArray*)dest)->data, ((GArray*)src)->data, word_array_len (src) * sizeof (WordIndex));

  return dest;
}

/* Not a macro so we can pass it into other functions */
void
word_array_unref (WordArray *word_array)
{
  g_array_unref ((GArray *) word_array);
}

gboolean
word_array_add (WordArray *word_array,
                WordIndex  word_index)
{
  guint out;

  g_return_val_if_fail (word_array != NULL, FALSE);

  if (g_array_binary_search ((GArray *) word_array,
                             &word_index,
                             word_array_cmp,
                             &out))
    return FALSE;

  g_array_append_val ((GArray *) word_array, word_index);
  g_array_sort ((GArray*) word_array, word_array_cmp);

  return TRUE;
}

gboolean
word_array_remove (WordArray *word_array,
                   WordIndex  word_index)
{
  guint out;

  g_return_val_if_fail (word_array != NULL, FALSE);

  if (g_array_binary_search ((GArray *) word_array,
                             &word_index,
                             word_array_cmp,
                             &out))
    {
      g_array_remove_index ((GArray *) word_array, out);
      return TRUE;
    }
  return FALSE;
}

gboolean
word_array_find (WordArray *word_array,
                 WordIndex  word_index,
                 guint     *out)
{
  g_return_val_if_fail (word_array != NULL, FALSE);

  return g_array_binary_search ((GArray *) word_array,
                                &word_index,
                                word_array_cmp,
                                out);
}

/* WordArrayModel */

struct _WordArrayModel
{
  GObject parent_object;

  WordList *word_list;
  WordArray *word_array;
  GArray *obj_array;
};


static void     word_array_model_init            (WordArrayModel      *self);
static void     word_array_model_class_init      (WordArrayModelClass *klass);
static void     word_array_model_list_model_init (GListModelInterface *iface);
static void     word_array_model_dispose         (GObject             *object);
static GType    word_array_model_get_item_type   (GListModel          *array);
static guint    word_array_model_get_n_items     (GListModel          *array);
static gpointer word_array_model_get_item        (GListModel          *array,
                                                  guint                position);


G_DEFINE_TYPE_WITH_CODE (WordArrayModel, word_array_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                word_array_model_list_model_init));

static void
obj_array_clear (WordListModelRow **data)
{
  if (*data != NULL)
    {
      g_object_unref (G_OBJECT (*data));
      *data = NULL;
    }
}

static void
word_array_model_init (WordArrayModel *self)
{
  self->word_list = word_list_new ();
  self->word_array = word_array_new ();
  self->obj_array = g_array_new (FALSE, TRUE, sizeof (WordListModelRow *));
  g_array_set_clear_func (self->obj_array, (GDestroyNotify ) obj_array_clear);
}

static void
word_array_model_class_init (WordArrayModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_array_model_dispose;
}

static void
word_array_model_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = word_array_model_get_item_type;
  iface->get_n_items = word_array_model_get_n_items;
  iface->get_item = word_array_model_get_item;
}

static void
word_array_model_dispose (GObject *object)
{
  WordArrayModel *self;

  self = WORD_ARRAY_MODEL (object);

  g_clear_object (&self->word_list);
  g_clear_pointer (&self->word_array, word_array_unref);
  g_clear_pointer (&self->obj_array, g_array_unref);

  G_OBJECT_CLASS (word_array_model_parent_class)->dispose (object);
}


static GType
word_array_model_get_item_type (GListModel *model)
{
  return WORD_TYPE_LIST_MODEL_ROW;
}

static guint
word_array_model_get_n_items (GListModel *model)
{
  WordArrayModel *array_model;

  array_model = WORD_ARRAY_MODEL (model);

  return array_model->obj_array->len;
}

static gpointer
word_array_model_get_item (GListModel *model,
                          guint       position)
{
  WordArrayModel *array_model;
  WordIndex word_index;
  WordListModelRow *row;
  const gchar *word;
  gint priority;

  array_model = WORD_ARRAY_MODEL (model);

  row  = g_array_index (array_model->obj_array, WordListModelRow *, position);
  word_index = word_array_index (array_model->word_array, position);
  
  word = word_list_get_indexed_word (array_model->word_list, word_index);
  priority = word_list_get_indexed_priority (array_model->word_list, word_index);

  if (row == NULL)
    {
      WordListModelRow **row_ptr = &(g_array_index (array_model->obj_array, WordListModelRow *, position));
      row = word_list_model_row_new (word, priority, NULL);
      *row_ptr = row;
    }
  else
    {
      row->word = word;
      row->priority = priority;
    }

  return g_object_ref (row);
}

WordArrayModel *
word_array_model_new (void)
{
  WordArrayModel *array_model;
  
  array_model = (WordArrayModel *) g_object_new (WORD_TYPE_ARRAY_MODEL, NULL);

  return array_model;
}

/**
 * word_array_model_add:
 * @array_model: an `ArrayModel`
 * @word_index: a `WordIndex`
 *
 * Adds @word_index to the @array_model.
 *
 * Returns: %True, if @word_index was added
 **/
gboolean
word_array_model_add (WordArrayModel *array_model,
                      WordIndex       word_index)
{
  guint pos = 0;

  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);

  if (word_array_add (array_model->word_array, word_index))
    {
      /* since we reset the value every time you get an object, we don't
       * actually care what's in the obj_array. It just has to be the
       * same length. Weird, huh... */
      g_array_set_size (array_model->obj_array, array_model->obj_array->len +1);
      word_array_find (array_model->word_array, word_index, &pos);
      g_list_model_items_changed (G_LIST_MODEL (array_model),
                                  pos, 0, 1);
      return TRUE;
    }
  return FALSE;
}

/**
 * word_array_model_remove:
 * @array_model: an `ArrayModel`
 * @word_index: a `WordIndex`
 *
 * Removes @word_index from the @array_model.
 *
 * Returns: %True, if @word_index was removed
 **/
gboolean
word_array_model_remove (WordArrayModel *array_model,
                         WordIndex       word_index)
{
  guint pos = 0;
  
  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);
  g_return_val_if_fail (array_model->obj_array->len != 0, FALSE);

  word_array_find (array_model->word_array, word_index, &pos);

  if (word_array_remove (array_model->word_array, word_index))
    {
      /* since we reset the value every time you get an object, we don't
       * actually care what's in the obj_array. It just has to be the
       * same length. Weird, huh... */
      g_array_set_size (array_model->obj_array, array_model->obj_array->len -1);
      g_list_model_items_changed (G_LIST_MODEL (array_model),
                                  pos, 1, 0);
      return TRUE;
    }

  return FALSE;
}

gboolean
word_array_model_find (WordArrayModel *array_model,
                       WordIndex       word_index,
                       guint          *out)
{
  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), FALSE);

  return word_array_find (array_model->word_array,
                          word_index, out);
}


/**
 * word_array_model_get_array:
 * @array_model: an `ArrayModel`
 *
 * Returns the internal `WordArray` of @array_model. This should not
 * be modified in any way, but is useful to pass into a `WordSolver`.
 *
 * Returns: the internal `WordArray`
 **/
WordArray *
word_array_model_get_array (WordArrayModel *array_model)
{
  g_return_val_if_fail (WORD_IS_ARRAY_MODEL (array_model), NULL);

  return array_model->word_array;
}

/* Returns the nth triangular number, 0 + 1 + 2 + ... + n
 *
 * 0 -> 0
 *
 * 1 -> *   -> 1
 *
 * 2 -> *   -> 3
 *      **
 *
 * 3 -> *   -> 6
 *      **
 *      ***
 */
static gsize
triangular_number (gsize n)
{
  return (n * (n + 1)) / 2;
}

/**
 * word_index_fragment_index:
 * @min_word_length: Minimum word length stored in the word index.
 * @charset_len: Size of the character set in the word index.
 * @fragment: Description of a filter fragment.
 *
 * Computes the index in the list of fragment list for a certain fragment.
 *
 * Returns: the index within a fragment list where @fragment will be.
 */
gsize
word_index_fragment_index (gint            min_word_length,
                           gsize           charset_len,
                           FilterFragment  fragment)
{
  g_assert (min_word_length > 0);
  g_assert (fragment.length > 0);

  /* In the diagram below, the number of O's before the * is the letter_slot_index, and we
   * compute it by taking the (fragment.length - 1)'th triangular number and subtracting
   * the (min_word_length - 1)'th triangular number.  Then we add in the
   * fragment.position.  In the end we multiply by charset_len, and add the
   * fragment.char_offset within that position.
   *
   *    x
   *    xx
   *    xxx
   *    OOOO     min_word_length
   *    OOOOO
   *    OOOOOO   fragment.length - 1
   *    OOOO*OO  fragment.length
   *        ^
   *     fragment.position
   *
   */
  gsize letter_slot_index;

  letter_slot_index = triangular_number (fragment.length - 1) - triangular_number (min_word_length - 1);
  letter_slot_index += fragment.position;

  return letter_slot_index * charset_len + fragment.char_index;
}

guint
word_index_calculate_letters_size (gint min_word_length,
                                   gint max_word_length,
                                   gsize charset_len)
{
  g_assert (min_word_length > 0);
  g_assert (max_word_length >= min_word_length);
  g_assert (charset_len > 0);

  /* We want to find the number of O's here (letter slots), and ignore the x's:
   *
   *     x
   *     xx
   *     xxx
   *     OOOO     min_word_length
   *     OOOOO
   *     OOOOOO   max_word_length
   *
   * So, we compute the max_word_length'th triangular number, and subtract the
   * (min_word_length - 1)'th triangular number.
   *
   * In the end we multiply by the charset_len because each letter slot corresponds
   * to that many characters.
   */

  gsize num_letter_slots = triangular_number (max_word_length) - triangular_number (min_word_length - 1);

  return num_letter_slots * charset_len;
}

/* Comparison function for qsort */
static gint
compare_chars (gconstpointer a,
               gconstpointer b,
               gpointer      user_data)
{
  gchar char_a = *(const gchar*)a;
  gchar char_b = *(const gchar*)b;

  if (char_a < char_b)
    return -1;
  else if (char_a > char_b)
    return 1;
  else
    return 0;
}

guint
word_list_hash_func (const gchar *str)
{
  g_autofree gchar *sorted_str = g_strdup (str);

  /* Sort the characters in the string */
  g_qsort_with_data (sorted_str,
                     strlen (sorted_str),
                     sizeof (gchar),
                     compare_chars,
                     NULL);

  return g_str_hash (sorted_str);
}



#ifdef TESTING
static void
computes_letters_size (void)
{
  /* For these tests, each O is a letter slot. See docs/word-list.md for an equivalent drawing.
   *
   *     O    min_length=1
   *     OO
   *     OOO  max_length=3
   *     ^
   *     we have 1+2+3 = 6 letter slots
   *     multiplied by a charset size of 4, gives 6*4 = 24.
   */
  g_assert_cmpint (word_index_calculate_letters_size (1, 3, 4), ==, 24);

  /*     OOO         min_length=3
   *     OOOO
   *     OOOOO
   *     OOOOOO      max_length=6
   *     ^
   *     we have 3+4+5+6 = 18 letter slots
   *     multiplied by a charset size of 5, gives 18 * 5 = 90
   */
  g_assert_cmpint (word_index_calculate_letters_size (3, 6, 5), ==, 90);
}

static void
computes_fragment_index (void)
{
  /*    O      min_length=1
   *    OO
   *    OOO
   *    OOOO
   *    OOOO*  fragment.length=5
   *        ^  fragment.position=4
   *
   * charset_len=6, fragment.char_index=3
   *
   * 6 * (1+2+3+4+4) + 3 = 87
   */
  FilterFragment frag = {
    .length = 5,
    .position = 4,
    .char_index = 3,
  };
  g_assert_cmpint (word_index_fragment_index (1, 6, frag), ==, 87);

  /*   *
   *   **
   *   ***
   *   OOOO      min_length = 4
   *   OOOOO
   *   OO*OOO    fragment.length=6
   *     ^       fragment.position=2
   *
   * charset_len=10, fragment.char_index=7
   *
   * 10 * (4 + 5 + 2) + 7 = 117
   */

  frag.length = 6;
  frag.position = 2;
  frag.char_index = 7;
  g_assert_cmpint (word_index_fragment_index (4, 10, frag), ==, 117);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/fragments/computes_letters_size", computes_letters_size);
  g_test_add_func ("/fragments/computes_fragment_index", computes_fragment_index);

  return g_test_run ();
}
#endif
