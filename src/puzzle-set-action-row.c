/* puzzle-set-action-row.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>

#include "puzzle-set-action-row.h"

struct _PuzzleSetActionRow
{
  AdwActionRow parent_object;

  PuzzleSet *puzzle_set;
  GtkWidget *unsolved_label;
  gulong notify_handler_id;
};

/* PuzzleSetActionRow */
static void               puzzle_set_action_row_init              (PuzzleSetActionRow         *self);
static void               puzzle_set_action_row_class_init        (PuzzleSetActionRowClass    *klass);
static void               puzzle_set_action_row_dispose           (GObject                   *object);


G_DEFINE_TYPE (PuzzleSetActionRow, puzzle_set_action_row, ADW_TYPE_ACTION_ROW);


static void
puzzle_set_action_row_init (PuzzleSetActionRow *self)
{
  self->unsolved_label = gtk_label_new (NULL);
  gtk_label_set_yalign (GTK_LABEL (self->unsolved_label), 1.0);
  gtk_widget_add_css_class (self->unsolved_label, "subtitle");
  /* Keep in sync with AdwRowItem CSS */
  gtk_widget_set_margin_top (self->unsolved_label, 6);
  gtk_widget_set_margin_bottom (self->unsolved_label, 6);
  adw_action_row_add_suffix (ADW_ACTION_ROW (self), self->unsolved_label);
}

static void
puzzle_set_action_row_class_init (PuzzleSetActionRowClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = puzzle_set_action_row_dispose;
}

static void
puzzle_set_action_row_dispose (GObject *object)
{
  PuzzleSetActionRow *self;

  self = PUZZLE_SET_ACTION_ROW (object);

  g_clear_signal_handler (&self->notify_handler_id, self->puzzle_set);
  g_clear_object (&self->puzzle_set);

  G_OBJECT_CLASS (puzzle_set_action_row_parent_class)->dispose (object);
}

static void
update_won_label (PuzzleSetActionRow *self,
                  GParamSpec         *pspec,
                  PuzzleSet          *puzzle_set)
{
  guint n_puzzles;
  guint n_puzzles_won;

  n_puzzles = puzzle_set_get_n_puzzles (puzzle_set);
  n_puzzles_won = puzzle_set_get_n_won (puzzle_set);

  if (n_puzzles == 0)
    gtk_label_set_text (GTK_LABEL (self->unsolved_label), NULL);
  if (n_puzzles == n_puzzles_won)
    gtk_label_set_text (GTK_LABEL (self->unsolved_label), _("All puzzles solved"));
  else
    {
      const char *unsolved_str;
      g_autofree gchar *unsolved_text = NULL;

      unsolved_str = g_dngettext (NULL,
                                  _("%d puzzle unsolved"),
                                  _("%d puzzles unsolved"),
                                  n_puzzles - n_puzzles_won);
      unsolved_text = g_strdup_printf (unsolved_str, n_puzzles - n_puzzles_won);
      gtk_label_set_text (GTK_LABEL (self->unsolved_label), unsolved_text);
    }
}

GtkWidget *
puzzle_set_action_row_new (PuzzleSet *puzzle_set)
{
  PuzzleSetActionRow *row;

  row = g_object_new (PUZZLE_TYPE_SET_ACTION_ROW, NULL);
  row->puzzle_set = g_object_ref (puzzle_set);
  update_won_label (row, NULL, puzzle_set);
  row->notify_handler_id = g_signal_connect_swapped (puzzle_set, "notify", G_CALLBACK (update_won_label), row);

  return GTK_WIDGET (row);
}

PuzzleSet *
puzzle_set_action_row_get_puzzle_set (PuzzleSetActionRow *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_ACTION_ROW (self), NULL);

  return self->puzzle_set;
}
