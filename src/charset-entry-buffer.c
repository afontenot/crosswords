/* charset-entry-buffer.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "charset-entry-buffer.h"


struct _CharsetEntryBuffer
{
  GtkEntryBuffer parent_instance;

  /* FIXME(charset): Figure out puzzle vs wordlist charsets, and add
   * this filter */
  char *charset;
};

static void  charset_entry_buffer_init        (CharsetEntryBuffer      *self);
static void  charset_entry_buffer_class_init  (CharsetEntryBufferClass *klass);
static guint charset_entry_buffer_insert_text (GtkEntryBuffer          *buffer,
                                               guint                    position,
                                               const gchar             *chars,
                                               guint                    n_chars);


G_DEFINE_TYPE (CharsetEntryBuffer, charset_entry_buffer, GTK_TYPE_ENTRY_BUFFER);



static void
charset_entry_buffer_init (CharsetEntryBuffer *self)
{

}

static void
charset_entry_buffer_class_init (CharsetEntryBufferClass *klass)
{
  GtkEntryBufferClass *entry_buffer_class = GTK_ENTRY_BUFFER_CLASS (klass);

  entry_buffer_class->insert_text = charset_entry_buffer_insert_text;
}

/* We need to go back and validate characters against charsets. On the
 * other hand, we handle ß->SS which is pretty nifty. */
static guint
charset_entry_buffer_insert_text (GtkEntryBuffer *buffer,
                                  guint           position,
                                  const gchar    *chars,
                                  guint           n_chars)
{
  g_autofree char *upper = NULL;
  guint retval;

  upper = g_utf8_strup (chars, -1);
  
  retval = GTK_ENTRY_BUFFER_CLASS (charset_entry_buffer_parent_class)->insert_text (buffer, position, upper,
                                                                                    g_utf8_strlen (upper, -1));

  return retval;
}

