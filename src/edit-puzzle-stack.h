
/* edit-puzzle-stack.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz.h>
#include "grid-state.h"
#include "word-solver.h"


G_BEGIN_DECLS


#define EDIT_TYPE_PUZZLE_STACK (edit_puzzle_stack_get_type())
G_DECLARE_FINAL_TYPE (EditPuzzleStack, edit_puzzle_stack, EDIT, PUZZLE_STACK, GtkWidget);


GtkWidget *  edit_puzzle_stack_new            (void);
void         edit_puzzle_stack_set_puzzle     (EditPuzzleStack *puzzle_stack,
                                               IPuzPuzzle      *puzzle);
void         edit_puzzle_stack_set_solver     (EditPuzzleStack *puzzle_stack,
                                               WordSolver      *solver);
CellArray *  edit_puzzle_stack_get_cell_array (EditPuzzleStack *puzzle_stack);
void         edit_puzzle_stack_update         (EditPuzzleStack *puzzle_stack);
IPuzGuesses *edit_puzzle_stack_get_guess      (EditPuzzleStack *puzzle_stack);


G_END_DECLS
