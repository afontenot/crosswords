/* edit-state.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "edit-state.h"


#define EDIT_STATE_GRID_STATE_HINT  "edit-state-grid-state-hint"
#define EDIT_STATE_CLUES_STATE_HINT "edit-state-clues-state-hint"
#define EDIT_STATE_STYLE_STATE_HINT "edit-state-style-state-hint"
#define DEFAULT_SIDEBAR_BASE_SIZE   20

EditState *
edit_state_new (IPuzPuzzle *puzzle)
{
  IPuzSymmetry symmetry = IPUZ_SYMMETRY_ROTATIONAL_QUARTER;
  EditState *edit_state;

  if (puzzle)
    {
      g_assert (IPUZ_IS_CROSSWORD (puzzle));
      symmetry = ipuz_crossword_get_symmetry (IPUZ_CROSSWORD (puzzle));
    }

  edit_state = g_new0 (EditState, 1);
  edit_state->stage = EDIT_STAGE_GRID;
  edit_state->quirks = crosswords_quirks_new (puzzle);
  crosswords_quirks_set_symmetry (edit_state->quirks, symmetry);
  edit_state->sidebar_logo_config =
    layout_config_at_base_size (DEFAULT_SIDEBAR_BASE_SIZE);

  if (puzzle)
    {
      edit_state->puzzle_kind = ipuz_puzzle_get_puzzle_kind (puzzle);
      edit_state->grid_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT);
      edit_state->clues_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT_BROWSE);
      edit_state->style_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT);
      if (IPUZ_IS_BARRED (puzzle))
        edit_state->sidebar_logo_config.border_size *= 2;
    }

  return edit_state;
}

void
edit_state_validate (const char *curframe,
                     EditState  *edit_state)
{
  g_assert (edit_state != NULL);
  g_assert (edit_state->grid_state != NULL);
  g_assert (edit_state->clues_state != NULL);
  g_assert (edit_state->style_state != NULL);
  g_assert (edit_state->quirks == edit_state->grid_state->quirks);
  g_assert (edit_state->quirks == edit_state->clues_state->quirks);
  g_assert (edit_state->quirks == edit_state->style_state->quirks);
}

void
edit_state_free (EditState *edit_state)
{
  g_clear_object (&edit_state->quirks);
  g_clear_object (&edit_state->info);
  g_clear_pointer (&edit_state->grid_state, grid_state_free);
  g_clear_pointer (&edit_state->clues_state, grid_state_free);
  g_clear_pointer (&edit_state->style_state, grid_state_free);
  g_free (edit_state->clue_selection_text);

  g_free (edit_state);
}

void
edit_state_save_to_stack(EditState   *edit_state,
                         PuzzleStack *puzzle_stack)
{
  GridState *dehydrated_state;

  dehydrated_state = grid_state_dehydrate (edit_state->grid_state);
  puzzle_stack_set_data (puzzle_stack, EDIT_STATE_GRID_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);

  dehydrated_state = grid_state_dehydrate (edit_state->clues_state);
  puzzle_stack_set_data (puzzle_stack, EDIT_STATE_CLUES_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);

  dehydrated_state = grid_state_dehydrate (edit_state->style_state);
  puzzle_stack_set_data (puzzle_stack, EDIT_STATE_STYLE_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);
}

void
edit_state_restore_from_stack (EditState   *edit_state,
                               PuzzleStack *puzzle_stack)
{
  IPuzPuzzle *puzzle;
  GridState *dehydrated_state;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack, EDIT_STATE_GRID_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->grid_state =
    grid_state_replace (edit_state->grid_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack, EDIT_STATE_CLUES_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->clues_state =
    grid_state_replace (edit_state->clues_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack, EDIT_STATE_STYLE_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->style_state =
    grid_state_replace (edit_state->style_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));
}
