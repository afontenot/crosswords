/* edit-state.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include "crosswords-quirks.h"
#include "grid-layout.h"
#include "puzzle-stack.h"
#include "grid-state.h"


G_BEGIN_DECLS


#ifdef DEVELOPMENT_BUILD
#define VALIDATE_EDIT_STATE(edit_state) edit_state_validate(__FUNCTION__,edit_state)
#else
#define VALIDATE_EDIT_STATE(edit_state)
#endif


typedef struct
{
  IPuzPuzzleKind puzzle_kind;
  PuzzleStackStage stage;

  CrosswordsQuirks *quirks;
  GridState *grid_state;
  GridState *clues_state;
  gchar *clue_selection_text;
  GridState *style_state; // Unused for now
  IPuzPuzzleInfo *info;

  LayoutConfig sidebar_logo_config; /* Used to configure the size of the logos in the sidebar */
} EditState;


EditState *edit_state_new                (IPuzPuzzle  *puzzle);
void       edit_state_free               (EditState   *edit_state);
void       edit_state_validate           (const gchar *curframe,
                                          EditState   *edit_state);
void       edit_state_save_to_stack      (EditState   *edit_state,
                                          PuzzleStack *puzzle_stack);
void       edit_state_restore_from_stack (EditState   *edit_state,
                                          PuzzleStack *puzzle_stack);


G_END_DECLS
