/* edit-puzzle-stack.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib/gi18n.h>
#include "edit-puzzle-stack.h"
#include "play-grid.h"

typedef enum {
  AUTOFILL_GRID,
  GRID1,
  GRID2,
} CurrentPage;

enum {
  SELECTION_CHANGED,
  N_SIGNALS
};


static guint obj_signals[N_SIGNALS] = { 0 };

struct _EditPuzzleStack
{
  GtkWidget parent_instance;

  IPuzPuzzle *puzzle;
  WordSolver *solver;
  guint solution;
  CurrentPage page;

  GridState *autofill_state;
  GridState *grid1_state;
  GridState *grid2_state;

  /* Autofill widgets */
  GtkWidget *stack;
  GtkWidget *autofill_grid;
  GtkWidget *grid1;
  GtkWidget *grid2;
  GtkWidget *autofill_back_button;
  GtkWidget *autofill_count_menu_button;
  GtkWidget *autofill_next_button;

  GtkAdjustment *popover_adjustment;
  GtkWidget *popover_spin;
};

static void edit_puzzle_stack_init         (EditPuzzleStack        *self);
static void edit_puzzle_stack_class_init   (EditPuzzleStackClass   *klass);
static void edit_puzzle_stack_dispose      (GObject                *object);
static void edit_puzzle_stack_set_page     (EditPuzzleStack        *self,
                                            CurrentPage             page,
                                            GtkStackTransitionType  transition);
static void edit_puzzle_stack_set_solution (EditPuzzleStack        *self,
                                            guint                   solution,
                                            guint                   n_solutions);


/* callbacks */
static void select_drag_start_cb           (EditPuzzleStack        *self,
                                            IPuzCellCoord          *anchor_coord,
                                            IPuzCellCoord          *new_coord,
                                            GridSelectionMode       mode);
static void select_drag_update_cb          (EditPuzzleStack        *self,
                                            IPuzCellCoord          *anchor_coord,
                                            IPuzCellCoord          *new_coord,
                                            GridSelectionMode       mode);
static void select_drag_end_cb             (EditPuzzleStack        *self);
static void prev_clicked_cb                (EditPuzzleStack        *puzzle_stack);
static void next_clicked_cb                (EditPuzzleStack        *puzzle_stack);
static void popover_spin_changed_cb        (EditPuzzleStack        *puzzle_stack,
                                            GtkSpinButton          *spin);



G_DEFINE_TYPE (EditPuzzleStack, edit_puzzle_stack, GTK_TYPE_WIDGET);


static void
edit_puzzle_stack_init (EditPuzzleStack *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->solution = 0;
  self->page = AUTOFILL_GRID;

  gtk_stack_set_visible_child_name (GTK_STACK (self->stack),
                                    "autofill-grid");
  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
}

static void
edit_puzzle_stack_class_init (EditPuzzleStackClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_puzzle_stack_dispose;

  obj_signals[SELECTION_CHANGED] =
    g_signal_new ("selection-changed",
                  EDIT_TYPE_PUZZLE_STACK,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-puzzle-stack.ui");

  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, stack);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, autofill_grid);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, grid1);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, grid2);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, autofill_back_button);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, autofill_count_menu_button);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, autofill_next_button);

  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, popover_spin);
  gtk_widget_class_bind_template_child (widget_class, EditPuzzleStack, popover_adjustment);

  gtk_widget_class_bind_template_callback (widget_class, select_drag_start_cb);
  gtk_widget_class_bind_template_callback (widget_class, select_drag_update_cb);
  gtk_widget_class_bind_template_callback (widget_class, select_drag_end_cb);
  gtk_widget_class_bind_template_callback (widget_class, prev_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, next_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, popover_spin_changed_cb);

  gtk_widget_class_set_layout_manager_type (GTK_WIDGET_CLASS (klass),
                                            GTK_TYPE_BOX_LAYOUT);

}

static void
edit_puzzle_stack_dispose (GObject *object)
{
  EditPuzzleStack *self;
  GtkWidget *child;

  self = EDIT_PUZZLE_STACK (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->autofill_state, grid_state_free);
  g_clear_pointer (&self->grid1_state, grid_state_free);
  g_clear_pointer (&self->grid2_state, grid_state_free);
  g_clear_object (&self->puzzle);
  g_clear_object (&self->solver);

  G_OBJECT_CLASS (edit_puzzle_stack_parent_class)->dispose (object);
}


static void
edit_puzzle_stack_set_page (EditPuzzleStack        *self,
                            CurrentPage             page,
                            GtkStackTransitionType  transition)
{
  const char *child_name = NULL;

  if (self->page == page)
    return;
  switch (page)
    {
    case AUTOFILL_GRID:
      child_name = "autofill-grid";
      break;
    case GRID1:
      child_name = "grid1";
      break;
    case GRID2:
      child_name = "grid2";
      break;
    }
  gtk_stack_set_transition_type (GTK_STACK (self->stack),
                                 transition);
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack),
                                    child_name);
  self->page = page;

}

/* Note, we pass in n_solutions beceause this could change between
 * when we last called get_n_solutions and when this is called. This
 * will keep the label / next / prev buttons all consistent with the
 * animation etc.
 */

static void
edit_puzzle_stack_update_ui (EditPuzzleStack *self)
{
  g_autofree gchar *label = NULL;
  gboolean back_sensitive = FALSE;
  gboolean count_sensitive = FALSE;
  gboolean next_sensitive = FALSE;
  guint n_solutions;

  if (word_solver_get_state (self->solver) != WORD_SOLVER_READY)
    {
      n_solutions = word_solver_get_n_solutions (self->solver);
      if (n_solutions > 0)
        {
          count_sensitive = TRUE;

          /* our solution is 0-based; our display is 1-based. */
          label = g_strdup_printf (_("Solution %u / %u"), self->solution + 1, n_solutions);
          gtk_adjustment_set_upper (self->popover_adjustment, n_solutions);

          back_sensitive = (self->solution != 0);
          next_sensitive = ((self->solution + 1) != n_solutions);
        }
    }

  gtk_menu_button_set_label (GTK_MENU_BUTTON (self->autofill_count_menu_button), label);
  gtk_widget_set_sensitive (self->autofill_back_button, back_sensitive);
  gtk_widget_set_visible (self->autofill_count_menu_button, count_sensitive);
  gtk_widget_set_sensitive (self->autofill_next_button, next_sensitive);
}


static void
edit_puzzle_stack_set_solution (EditPuzzleStack *self,
                                guint            solution,
                                guint            n_solutions)
{
  GtkStackTransitionType transition = GTK_STACK_TRANSITION_TYPE_CROSSFADE;
  IPuzGuesses *guesses;
  CurrentPage dest;
  PlayGrid *grid;
  GridState *state;

  g_return_if_fail (solution <= n_solutions);

  if (self->page != AUTOFILL_GRID &&
      solution == self->solution)
    goto out;

  if (solution == self->solution + 1)
    transition = GTK_STACK_TRANSITION_TYPE_CROSSFADE;
  if (solution == self->solution - 1)
    transition = GTK_STACK_TRANSITION_TYPE_CROSSFADE;

  self->solution = solution;
  gtk_adjustment_set_value (self->popover_adjustment, solution + 1);

  if (self->page == AUTOFILL_GRID ||
      self->page == GRID2)
    {
      dest = GRID1;
      grid = PLAY_GRID (self->grid1);
      state = self->grid1_state;
    }
  else
    {
      dest = GRID2;
      grid = PLAY_GRID (self->grid2);
      state = self->grid2_state;
    }

  guesses = word_solver_get_solution (self->solver, solution);
  ipuz_crossword_set_guesses (state->xword, guesses);
  play_grid_update_state (PLAY_GRID (grid), state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  edit_puzzle_stack_set_page (self, dest, transition);

 out:
  edit_puzzle_stack_update_ui (self);
}


static void
edit_puzzle_stack_load_states (EditPuzzleStack *self)
{
  play_grid_update_state (PLAY_GRID (self->autofill_grid), self->autofill_state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));

  play_grid_update_state (PLAY_GRID (self->grid1), self->grid1_state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  play_grid_update_state (PLAY_GRID (self->grid2), self->grid2_state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));

  g_signal_emit (self, obj_signals[SELECTION_CHANGED], 0);
}

static void
edit_puzzle_stack_update_states (EditPuzzleStack *self)
{
  play_grid_update_state (PLAY_GRID (self->autofill_grid), self->autofill_state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));

  g_signal_emit (self, obj_signals[SELECTION_CHANGED], 0);
}

static void
select_drag_start_cb(EditPuzzleStack   *self,
                     IPuzCellCoord     *anchor_coord,
                     IPuzCellCoord     *new_coord,
                     GridSelectionMode  mode)
{
  self->autofill_state =
    grid_state_replace (self->autofill_state,
                        grid_state_select_drag_start (self->autofill_state,
                                                      *anchor_coord,
                                                      *new_coord,
                                                      mode));
  edit_puzzle_stack_update_states (self);
}

static void
select_drag_update_cb (EditPuzzleStack   *self,
                       IPuzCellCoord     *anchor_coord,
                       IPuzCellCoord     *new_coord,
                       GridSelectionMode  mode)
{
  self->autofill_state =
    grid_state_replace (self->autofill_state,
                        grid_state_select_drag_update (self->autofill_state,
                                                       *anchor_coord,
                                                       *new_coord,
                                                       mode));
  edit_puzzle_stack_update_states (self);
}

static void
select_drag_end_cb (EditPuzzleStack *self)
{
  self->autofill_state =
    grid_state_replace (self->autofill_state,
                        grid_state_select_drag_end (self->autofill_state));
  edit_puzzle_stack_update_states (self);
}

static
void prev_clicked_cb (EditPuzzleStack *puzzle_stack)
{
  guint n_solutions = word_solver_get_n_solutions (puzzle_stack->solver);

  g_return_if_fail (puzzle_stack->solution > 0);

  edit_puzzle_stack_set_solution (puzzle_stack, puzzle_stack->solution - 1, n_solutions);
}

static void
next_clicked_cb (EditPuzzleStack *puzzle_stack)
{
  guint n_solutions = word_solver_get_n_solutions (puzzle_stack->solver);

  g_return_if_fail ((puzzle_stack->solution + 1) < n_solutions);

  edit_puzzle_stack_set_solution (puzzle_stack, puzzle_stack->solution + 1, n_solutions);
}

static void
popover_spin_changed_cb (EditPuzzleStack *puzzle_stack,
                         GtkSpinButton   *spin)
{
  guint n_solutions = word_solver_get_n_solutions (puzzle_stack->solver);
  gint page;

  page = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (puzzle_stack->popover_spin));

  /* our solution is 0-based; our display is 1-based. */
  edit_puzzle_stack_set_solution (puzzle_stack, page - 1, n_solutions);
}


/* Public Functions */

GtkWidget *
edit_puzzle_stack_new (void)
{
  return (GtkWidget *) g_object_new (EDIT_TYPE_PUZZLE_STACK, NULL);
}

void
edit_puzzle_stack_set_puzzle (EditPuzzleStack *puzzle_stack,
                              IPuzPuzzle      *puzzle)
{
  g_return_if_fail (EDIT_IS_PUZZLE_STACK (puzzle_stack));

  g_object_ref (puzzle);

  g_clear_object (& puzzle_stack->puzzle);
  g_clear_pointer (& puzzle_stack->autofill_state, grid_state_free);

  puzzle_stack->puzzle = puzzle;

  puzzle_stack->autofill_state = grid_state_new (IPUZ_CROSSWORD (puzzle_stack->puzzle), NULL, GRID_STATE_SELECT);

  puzzle_stack->grid1_state = grid_state_new (IPUZ_CROSSWORD (puzzle_stack->puzzle), NULL, GRID_STATE_BROWSE);
  puzzle_stack->grid2_state = grid_state_new (IPUZ_CROSSWORD (puzzle_stack->puzzle), NULL, GRID_STATE_BROWSE);

  edit_puzzle_stack_load_states (puzzle_stack);
}

void
edit_puzzle_stack_set_solver (EditPuzzleStack *puzzle_stack,
                              WordSolver      *solver)
{
  g_return_if_fail (EDIT_IS_PUZZLE_STACK (puzzle_stack));

  g_object_ref (solver);

  g_clear_object (& puzzle_stack->solver);
  puzzle_stack->solver = solver;

}

CellArray *
edit_puzzle_stack_get_cell_array (EditPuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (EDIT_IS_PUZZLE_STACK (puzzle_stack), NULL);

  return puzzle_stack->autofill_state->selected_cells;
}

void
edit_puzzle_stack_update (EditPuzzleStack *puzzle_stack)
{
  if (word_solver_get_state (puzzle_stack->solver) == WORD_SOLVER_READY)
    {
      edit_puzzle_stack_set_page (puzzle_stack, AUTOFILL_GRID, GTK_STACK_TRANSITION_TYPE_CROSSFADE);
    }
  else
    {
      guint n_solutions = word_solver_get_n_solutions (puzzle_stack->solver);

      if (n_solutions > 0)
        {
          /* Go to the first page */
          if (puzzle_stack->page == AUTOFILL_GRID)
            edit_puzzle_stack_set_solution (puzzle_stack, 0, n_solutions);
        }
    }
  edit_puzzle_stack_update_ui (puzzle_stack);
}

IPuzGuesses *
edit_puzzle_stack_get_guess (EditPuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (EDIT_IS_PUZZLE_STACK (puzzle_stack), NULL);

  if (word_solver_get_state (puzzle_stack->solver) == WORD_SOLVER_READY)
    return NULL;

  return word_solver_get_solution (puzzle_stack->solver, puzzle_stack->solution);

}
