/* puzzle-picker.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include "puzzle-downloader.h"
#include "puzzle-set-config.h"
#include "puzzle-set-model.h"

G_BEGIN_DECLS


#define PUZZLE_TYPE_PICKER (puzzle_picker_get_type())
G_DECLARE_DERIVABLE_TYPE (PuzzlePicker, puzzle_picker, PUZZLE, PICKER, GtkWidget);

struct _PuzzlePickerClass
{
  GtkWidgetClass widget_class;

  /* Virtual function */
  void        (*load)                 (PuzzlePicker    *picker);
  const char *(*get_title)            (PuzzlePicker    *picker);
  void        (*set_downloader_state) (PuzzlePicker    *picker,
                                       DownloaderState  state);
};


void             puzzle_picker_load                    (PuzzlePicker     *picker);
const gchar     *puzzle_picker_get_title            (PuzzlePicker    *picker);
void             puzzle_picker_set_downloader_state (PuzzlePicker    *picker,
                                                     DownloaderState  state);


/* Subclass functions */
PuzzleSetConfig *puzzle_picker_get_config           (PuzzlePicker    *picker);
PuzzleSetModel  *puzzle_picker_get_model            (PuzzlePicker    *picker);
void             puzzle_picker_puzzle_selected      (PuzzlePicker    *picker,
                                                     const gchar     *puzzle_name);
void             puzzle_picker_start_download       (PuzzlePicker    *picker);
void             puzzle_picker_clear_puzzle         (PuzzlePicker    *picker,
                                                     const gchar     *puzzle_name,
                                                     const gchar     *primary,
                                                     const gchar     *secondary,
                                                     gboolean         play_when_done,
                                                     gboolean         allow_view);


G_END_DECLS
