/* svg.c - Generate SVG documents from crosswords
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "svg.h"

/* appends src to dest and consumes src */
static void
append (GString *dest, GString *src)
{
  g_string_append_len (dest, src->str, src->len);
  g_string_free (src, TRUE);
}

static GString *
xml_preamble (void)
{
  return g_string_new ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
}

static GString *
svg_toplevel (LayoutGeometry *g)
{
  GString *s = g_string_new ("");
  g_string_append_printf (s,
                          "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"%u\" height=\"%u\" viewBox=\"0 0 %u %u\">\n",
                          g->document_width, g->document_height,
                          g->document_width, g->document_height);
  return s;
}

static GString *
svg_toplevel_end (void)
{
  return g_string_new ("</svg>\n");
}

static GString *
definitions (LayoutGeometry *g)
{
  GString *s = g_string_new ("");

  g_string_append (s, "<defs>\n");

  g_string_append_printf (s,
                          "<rect id=\"intersection\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->border_size, g->border_size);
  g_string_append_printf (s,
                          "<rect id=\"border_horizontal\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->cell_size, g->border_size);
  g_string_append_printf (s,
                          "<rect id=\"border_vertical\" class=\"border\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->border_size, g->cell_size);
  g_string_append_printf (s,
                          "<rect id=\"cell\" class=\"cell\" x=\"0\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g->cell_size, g->cell_size);

  g_string_append (s, "</defs>\n");

  return s;

}

static double
scale_for_arrows (LayoutGeometry *g)
{
  /* This is pretty ad-hoc, but I drew the original arrows based on a border_size of 4.
   * An alternative would be to pick a size based on the base size of cells.
   */
  return g->border_size / 4.0;
}

static GString *
overlay_definitions (LayoutGeometry *g)
{
  GString *s = g_string_new ("");
  /* These are used for g_ascii_dtostr() for doing locale-independent
   * printing of doubles. Otherwise, european locales will print 1,5
   * for coordinates, that librsvg doesn't like */
  gchar buf_a[G_ASCII_DTOSTR_BUF_SIZE];
  gchar buf_b[G_ASCII_DTOSTR_BUF_SIZE];
  gchar buf_c[G_ASCII_DTOSTR_BUF_SIZE];

  double half_border = (double) g->border_size / 2.0;
  double double_border = (double) g->border_size * 2.0;

  g_string_append (s, "<defs>\n");

  /* Each overlay has its anchor point at the LayoutItem's top-left corner.  However, an
   * overlay may overshoot its LayoutItem (e.g. a thick barred border).  The overlay
   * should still be placed as if it were a normal LayoutItem, without adjusting for its
   * specific size.
   */

  g_string_append_printf (s,
                          "<rect id=\"barred_horizontal\" class=\"border\" x=\"0\" y=\"%s\" width=\"%u\" height=\"%s\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g->cell_size,
                          g_ascii_dtostr (buf_b, sizeof (buf_b), double_border));

  g_string_append_printf (s,
                          "<rect id=\"barred_vertical\" class=\"border\" x=\"%s\" y=\"0\" width=\"%s\" height=\"%u\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), double_border),
                          g->cell_size);

  g_string_append_printf (s,
                          "<rect id=\"enumeration_space_horizontal\" class=\"enumeration\" x=\"0\" y=\"%s\" width=\"%u\" height=\"%s\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g->cell_size,
                          g_ascii_dtostr (buf_b, sizeof (buf_b), double_border));

  g_string_append_printf (s,
                          "<rect id=\"enumeration_space_vertical\" class=\"enumeration\" x=\"%s\" y=\"0\" width=\"%s\" height=\"%u\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), double_border),
                          g->cell_size);

  /* This is slightly confusing. enumeration_dash_horizontal is
   * actually a vertical dash, but it goes across the horizontal
   * line */
  g_string_append_printf (s,
                          "<rect id=\"enumeration_dash_horizontal\" class=\"enumeration\" x=\"%s\" y=\"0\" width=\"%u\" height=\"%u\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g->border_size,
                          g->border_size * 5);

  g_string_append_printf (s,
                          "<rect id=\"enumeration_dash_vertical\" class=\"enumeration\" x=\"0\" y=\"%s\" width=\"%u\" height=\"%u\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), -half_border),
                          g->border_size * 5,
                          g->border_size);

  /* same as the dash; this cuts across a horizontal line */
  g_string_append_printf (s,
                          "<text id=\"enumeration_apostrophe_horizontal\" class=\"enumeration\" font-size=\"%spx\" x=\"%s\" y=\"%s\" text-anchor=\"middle\">'</text>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size*2/3.0),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), g->cell_size*.3),
                          g_ascii_dtostr (buf_c, sizeof (buf_c), g->cell_size/2.0 - double_border));
  
  g_string_append_printf (s,
                          "<text id=\"enumeration_apostrophe_vertical\" class=\"enumeration\" font-size=\"%spx\" x=\"%s\" y=\"%s\" text-anchor=\"middle\">'</text>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size*2/3.0),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), half_border),
                          g_ascii_dtostr (buf_c, sizeof (buf_c), g->cell_size*2/3.0));

  /* same as the dash; this cuts across a horizontal line */
  g_string_append_printf (s,
                          "<text id=\"enumeration_period_horizontal\" class=\"enumeration\" font-size=\"%spx\" x=\"%s\" y=\"%s\" text-anchor=\"middle\">.</text>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size*2/3.0),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), g->cell_size/2.0),
                          g_ascii_dtostr (buf_c, sizeof (buf_c), (double) g->border_size));

  g_string_append_printf (s,
                          "<text id=\"enumeration_period_vertical\" class=\"enumeration\" font-size=\"%spx\" x=\"%s\" y=\"%s\" text-anchor=\"middle\">.</text>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size*2/3.0),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), half_border),
                          g_ascii_dtostr (buf_c, sizeof (buf_c), g->cell_size*5/6.0 - half_border));

  /* divided */
  g_string_append_printf (s,
                          "<rect id=\"divided_horiz\" class=\"border\" x=\"0\" y=\"%s\" width=\"%u\" height=\"%s\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size/2.0),
                          g->cell_size,
                          g_ascii_dtostr (buf_b, sizeof (buf_b), half_border));
  g_string_append_printf (s,
                          "<rect id=\"divided_vert\" class=\"border\" x=\"%s\" y=\"0\" width=\"%s\" height=\"%u\"/>\n",
                          g_ascii_dtostr (buf_a, sizeof (buf_a), g->cell_size/2.0),
                          g_ascii_dtostr (buf_b, sizeof (buf_b), half_border),
                          g->cell_size);

  /* The following arrows come from docs/arrows.svg */
  g_ascii_dtostr (buf_a, sizeof (buf_a), scale_for_arrows (g));

  g_string_append_printf (s,
                          "<path id=\"arrow-right\" transform=\"scale(%s)\" d=\"M0,0 L0,2 L5,2 L5,4 L11,0 L5,-4 L5,-2 L0,-2 Z\"/>",
                          buf_a);
  g_string_append_printf (s,
                          "<path id=\"arrow-down\" transform=\"scale(%s)\" d=\"M0,0 L2,0 L2,5 L4,5 L0,11 L-4,5 L-2,5 L-2,0 Z\"/>",
                          buf_a);
  g_string_append_printf (s,
                          "<path id=\"arrow-right-down\" transform=\"scale(%s)\" d=\"M0,0 L0,2 L5,2 L5,6 L3,6 L7,12 L11,6 L9,6 L9,-2 L0,-2 Z\"/>",
                          buf_a);
  g_string_append_printf (s,
                          "<path id=\"arrow-down-right\" transform=\"scale(%s)\" d=\"M0,0 L2,0 L2,5 L6,5 L6,3 L12,7 L6,11 L6,9 L-2,9 L-2,0 Z\"/>",
                          buf_a);
  g_string_append_printf (s,
                          "<path id=\"arrow-up-right\" transform=\"scale(%s)\" d=\"M0,0 L2,0 L2,-5 L6,-5 L6,-3 L12,-7 L6,-11 L6,-9 L-2,-9 L-2,0 Z\"/>",
                          buf_a);
  g_string_append_printf (s,
                          "<path id=\"arrow-left-down\" transform=\"scale(%s)\" d=\"M0,0 L0,2 L-5,2 L-5,6 L-3,6 L-7,12 L-11,6 L-9,6 L-9,-2 L0,-2 Z\"/>",
                          buf_a);

  g_string_append (s, "</defs>\n");

  return s;
}

static GString *
background ()
{
  return g_string_new ("");
  /*  return g_string_new ("<rect x=\"0\" y=\"0\" width=\"100\%\" height=\"100\%\" fill=\"white\"/>"); */
}

static GString *
instance (const char *id, guint xpos, guint ypos, guint width, guint height)
{
  GString *s = g_string_new ("");

  g_string_append_printf (s,
                          "<use href=\"#%s\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\"/>\n",
                          id,
                          xpos, ypos,
                          width, height);

  return s;

}

/**
 * positioned_instance:
 * @id: XML id of the element to instantiate.
 * @extra_classes: (nullable) string to put inside a `class=""` attribute, or NULL to not include it.
 * @xpos: X coordinate for the `<use x="">` attribute.
 * @xpos: Y coordinate for the `<use y="">` attribute.
 *
 * Adds a `<use>` element to create an instance of the specified @id, at the specified position.
 *
 * Extra CSS classes can be put in @extra_classes, or pass NULL for no extra classes.
 */
static GString *
positioned_instance (const char *id, const char *extra_classes, double xpos, double ypos)
{
  GString *s = g_string_new ("");
  gchar buf_a[G_ASCII_DTOSTR_BUF_SIZE];
  gchar buf_b[G_ASCII_DTOSTR_BUF_SIZE];

  if (extra_classes)
    {
      g_string_append_printf (s,
                              "<use href=\"#%s\" class=\"%s\" x=\"%s\" y=\"%s\"/>\n",
                              id,
                              extra_classes,
                              g_ascii_dtostr (buf_a, sizeof (buf_a), xpos),
                              g_ascii_dtostr (buf_b, sizeof (buf_b), ypos));
    }
  else
    {
      g_string_append_printf (s,
                              "<use href=\"#%s\" x=\"%s\" y=\"%s\"/>\n",
                              id,
                              g_ascii_dtostr (buf_a, sizeof (buf_a), xpos),
                              g_ascii_dtostr (buf_b, sizeof (buf_b), ypos));
    }

  return s;

}

static GString *
cell (LayoutCell *item, guint xpos, guint ypos, guint width, guint height)
{
  if (item->bg_color.alpha == 0.0 || item->cell_type == IPUZ_CELL_NULL)
    {
      return g_string_new ("");
    }
  else
    {
      GString *s = g_string_new ("");
      guint r = (guint) (item->bg_color.red * 255.0 + 0.5);
      guint g = (guint) (item->bg_color.green * 255.0 + 0.5);
      guint b = (guint) (item->bg_color.blue * 255.0 + 0.5);
      guint a = (guint) (item->bg_color.alpha * 255.0 + 0.5);
      g_string_append_printf (s,
                              "<use href=\"#cell\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\" fill=\"#%02x%02x%02x%02x\"/>\n",
                              xpos, ypos,
                              width, height,
                              r, g, b, a);
      return s;
    }
}

static GString *
objects (GridLayout *layout)
{
  LayoutGeometry *g = &layout->geometry;
  GString *s = g_string_new ("");

  guint grid_row, grid_column;

  guint ypos = 0;

  for (grid_row = 0; grid_row < layout->grid_rows; grid_row++)
    {
      guint xpos = 0;

      for (grid_column = 0; grid_column < layout->grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION: {
            LayoutIntersection i = grid_layout_get_intersection (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("intersection", xpos, ypos, g->border_size, g->border_size));
              }
            xpos += g->border_size;
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL: {
            LayoutBorderHorizontal i = grid_layout_get_border_horizontal (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("border_horizontal", xpos, ypos, g->cell_size, g->border_size));
              }
            xpos += g->cell_size;
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            LayoutBorderVertical i = grid_layout_get_border_vertical (layout, grid_coord);
            if (i.filled)
              {
                append (s, instance ("border_vertical", xpos, ypos, g->border_size, g->cell_size));
              }
            xpos += g->border_size;
            break;
          }

          case LAYOUT_ITEM_KIND_CELL: {
            LayoutCell i = grid_layout_get_cell (layout, grid_coord);
            append (s, cell (&i, xpos, ypos, g->cell_size, g->cell_size));
            xpos += g->cell_size;
            break;
          }

          default:
            g_assert_not_reached ();
          }
        }

      if (grid_row % 2 == 0)
        {
          ypos += g->border_size;
        }
      else
        {
          ypos += g->cell_size;
        }
    }

  return s;
}

static GString *
overlays (GridLayout *layout)
{
  LayoutGeometry *g = &layout->geometry;
  GString *s = g_string_new ("");
  guint i;

  for (i = 0; i < layout->overlays->len; i++)
    {
      LayoutOverlay overlay = g_array_index (layout->overlays, LayoutOverlay, i);
      LayoutItemKind item_kind;
      const char *id;
      GridCoord coord;
      double xpos = 0.0;
      double ypos = 0.0;

      switch (overlay.kind)
        {
        case LAYOUT_OVERLAY_KIND_BARRED:
          coord = overlay.u.barred.coord;
          break;

        case LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE:
        case LAYOUT_OVERLAY_KIND_ENUMERATION_DASH:
        case LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE:
        case LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD:
          coord = overlay.u.enumeration.coord;
          break;

        default:
          g_assert_not_reached ();
          return NULL;
        }

      item_kind = grid_layout_get_kind (layout, coord);

      switch (overlay.kind)
        {
        case LAYOUT_OVERLAY_KIND_BARRED:

          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "barred_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "barred_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
          /* FIXME(enumeration): Use a space separator for apostrophes
           * for now */
        case LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_apostrophe_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_apostrophe_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_space_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_space_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_period_vertical";
              ypos = g->border_size;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_period_horizontal";
              xpos = g->border_size;
            }
          else
            continue;
          break;
        case LAYOUT_OVERLAY_KIND_ENUMERATION_DASH:
          if (item_kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
            {
              id = "enumeration_dash_vertical";
              xpos = g->border_size * -2.0;
              ypos = g->border_size + g->cell_size / 2;
            }
          else if (item_kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
            {
              id = "enumeration_dash_horizontal";
              xpos = g->border_size + g->cell_size / 2;
              ypos = g->border_size * -2.0;
            }
          else
            continue;
          break;
        default:
          g_assert_not_reached ();
          return NULL;
        }

      xpos += coord.column / 2 * (g->border_size + g->cell_size);
      ypos += coord.row / 2 * (g->border_size + g->cell_size);

      append (s, positioned_instance (id, NULL, xpos, ypos));
    }

  return s;
}

static GString *
divided (GridLayout *layout)
{
  LayoutGeometry *g = &layout->geometry;
  GString *s = g_string_new ("");
  guint row, column;
  double xpos = 0.0;
  double ypos = 0.0;

  for (row = 0; row < layout->grid_rows; row++)
    {
      for (column = 0; column < layout->grid_columns; column++)
        {
          IPuzStyleDivided divided = IPUZ_STYLE_DIVIDED_NONE;
          LayoutItemKind kind;
          GridCoord coord = {
            .row = row,
            .column = column,
          };
          gchar *id = NULL;

          kind = grid_layout_get_kind (layout, coord);
          if (kind == LAYOUT_ITEM_KIND_CELL)
            {
              LayoutCell cell = grid_layout_get_cell (layout, coord);
              divided = cell.divided;
            }
          else if (kind == LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL)
            {
              LayoutClueBlockCell cell = grid_layout_get_clue_block_cell (layout, coord);
              divided = cell.divided;
            }
          else
            continue;

          xpos = coord.column / 2 * (g->border_size + g->cell_size) + g->border_size;
          ypos = coord.row / 2 * (g->border_size + g->cell_size) + g->border_size;

          if (divided == IPUZ_STYLE_DIVIDED_HORIZ)
            id = "divided_horiz";
          else if (divided == IPUZ_STYLE_DIVIDED_VERT)
            id = "divided_vert";
          /* FIXME: handle the rest */

          if (id)
            append (s, positioned_instance (id, NULL, xpos, ypos));
        }
    }

  return s;
}


static void
arrow_direction (IPuzArrowwordArrow  arrow,
                 gint               *x,
                 gint               *y)
{
  *x = 0;
  *y = 0;
  switch (arrow)
    {
    case IPUZ_ARROWWORD_ARROW_RIGHT:
    case IPUZ_ARROWWORD_ARROW_RIGHT_DOWN:
      *x = 1;
      break;
    case IPUZ_ARROWWORD_ARROW_DOWN:
    case IPUZ_ARROWWORD_ARROW_DOWN_RIGHT:
      *y = 1;
      break;
    case IPUZ_ARROWWORD_ARROW_LEFT_DOWN:
      *x = -1;
      break;
    case IPUZ_ARROWWORD_ARROW_UP_RIGHT:
      *y = -1;
      break;
    case IPUZ_ARROWWORD_ARROW_NONE:
      g_assert_not_reached ();
    }
}

static void
draw_arrow (GString               *s,
            LayoutGeometry        *g,
            IPuzArrowwordArrow     arrow,
            LayoutItemBorderStyle  border_style,
            gdouble                cell_center_x,
            gdouble                cell_center_y,
            gdouble                cell_height)
{
  const gchar *direction_id;
  const gchar *classes;
  gint x, y;

  arrow_direction (arrow, &x, &y);

  switch (arrow)
    {
    case IPUZ_ARROWWORD_ARROW_RIGHT:
      direction_id = "arrow-right";
      break;
    case IPUZ_ARROWWORD_ARROW_RIGHT_DOWN:
      direction_id = "arrow-right-down";
      break;
    case IPUZ_ARROWWORD_ARROW_DOWN:
      direction_id = "arrow-down";
      break;
    case IPUZ_ARROWWORD_ARROW_DOWN_RIGHT:
      direction_id = "arrow-down-right";
      break;
    case IPUZ_ARROWWORD_ARROW_LEFT_DOWN:
      direction_id = "arrow-left-down";
      break;
    case IPUZ_ARROWWORD_ARROW_UP_RIGHT:
      direction_id = "arrow-up-right";
      break;
    case IPUZ_ARROWWORD_ARROW_NONE:
      return;
    default:
      g_assert_not_reached ();
    }

  switch (border_style)
    {
    case LAYOUT_BORDER_STYLE_UNSET:
    case LAYOUT_BORDER_STYLE_DARK:
    case LAYOUT_BORDER_STYLE_NORMAL:
    case LAYOUT_BORDER_STYLE_SELECTED:
    case LAYOUT_BORDER_STYLE_INITIAL_VAL:
    case LAYOUT_BORDER_STYLE_HIGHLIGHTED:
      classes = "arrow";
      break;
    case LAYOUT_BORDER_STYLE_FOCUSED:
      classes = "arrow-focused";
      break;
    default:
      g_assert_not_reached ();
    }
  cell_center_x += x * (g->cell_size/2.0 + g->border_size);
  cell_center_y += y * (cell_height/2.0 + g->border_size);

  append (s, positioned_instance (direction_id, classes, cell_center_x, cell_center_y));
}

static GString *
arrows (GridLayout *layout)
{
  LayoutGeometry *g = &layout->geometry;
  GString *s = g_string_new ("");
  guint row, column;

  for (row = 0; row < layout->grid_rows; row++)
    {
      for (column = 0; column < layout->grid_columns; column++)
        {
          LayoutItemKind kind;
          LayoutClueBlockCell cell;
          double cell_center_x, cell_center_y;
          gdouble cell_height;
          gint x, y;
          LayoutItemBorderStyle border_style;

          GridCoord coord = {
            .row = row,
            .column = column,
          };
          GridCoord border_coord = {
            .row = row,
            .column = column,
          };

          /* Only draw arrows if it's a BLOCK_CELL */
          kind = grid_layout_get_kind (layout, coord);
          if (kind != LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL)
            continue;

          cell = grid_layout_get_clue_block_cell (layout, coord);


          cell_center_x = coord.column / 2 * (g->border_size + g->cell_size) + g->border_size
            + g->cell_size/2.0;

          if (cell.bottom_arrow == IPUZ_ARROWWORD_ARROW_NONE)
            cell_height = g->cell_size;
          else
            cell_height = g->cell_size/2.0;

          cell_center_y = coord.row / 2 * (g->border_size + g->cell_size) + g->border_size
            + cell_height / 2.0;

          arrow_direction (cell.top_arrow, &x, &y);
          border_coord.row += y;
          border_coord.column += x;
          border_style = grid_layout_get_border_style (layout, border_coord);
          draw_arrow (s, g, cell.top_arrow, border_style, cell_center_x, cell_center_y, cell_height);

          if (cell.bottom_arrow != IPUZ_ARROWWORD_ARROW_NONE)
            {
              arrow_direction (cell.bottom_arrow, &x, &y);
              border_coord = coord;
              border_coord.row += y;
              border_coord.column += x;
              border_style = grid_layout_get_border_style (layout, border_coord);
              draw_arrow (s, g, cell.bottom_arrow, border_style, cell_center_x, cell_center_y + cell_height, cell_height);
            }
        }
    }

  return s;
}

/* Used for thumbnails */
GString *
svg_from_layout (GridLayout *layout)
{
  GString *doc = g_string_new ("");

  append (doc, xml_preamble ());
  append (doc, svg_toplevel (&layout->geometry));
  append (doc, definitions (&layout->geometry));
  append (doc, background ());
  append (doc, objects (layout));
  append (doc, svg_toplevel_end ());

  return doc;
}

GString *
svg_overlays_from_layout (GridLayout *layout)
{
  GString *doc = g_string_new ("");

  append (doc, xml_preamble ());
  append (doc, svg_toplevel (&layout->geometry));
  append (doc, overlay_definitions (&layout->geometry));
  append (doc, overlays (layout));
  append (doc, divided (layout));
  append (doc, arrows (layout));
  append (doc, svg_toplevel_end ());

  return doc;
}

static const char dark_stylesheet[] =
  ".border { fill: black; }\n"
  ".enumeration { fill: #888888; }\n"
  ".arrow { fill: black; }\n"
  ".arrow-focused { fill: #1a5fb4; }\n";

static const char light_stylesheet[] =
  ".border { fill: black; }\n"
  ".enumeration { fill: #888888; }\n"
  ".arrow { fill: black; }\n"
  ".arrow-focused { fill: #1c71d8; }\n";

static void
set_stylesheet (RsvgHandle *handle, Coloring coloring)
{
  const char *stylesheet = NULL;

  switch (coloring)
    {
    case COLORING_LIGHT:
      stylesheet = light_stylesheet;
      break;

    case COLORING_DARK:
      stylesheet = dark_stylesheet;
      break;

    default:
      g_assert_not_reached ();
      return;
    }

  GError *error = NULL;
  if (!rsvg_handle_set_stylesheet (handle, (const guint8 *) stylesheet, strlen (stylesheet), &error))
    {
      /* This will crash, which is what we want.  We should always be passing valid stylesheets. */
      g_assert_no_error (error);
    }
}

RsvgHandle *
svg_handle_from_string (GString  *string,
                        Coloring  coloring)
{
  GError *error = NULL;
  RsvgHandle *handle = rsvg_handle_new_from_data ((guint8 *) string->str, string->len, &error);
  set_stylesheet (handle, coloring);

  g_assert (handle != NULL);
  g_assert_no_error (error);
  return handle;
}

RsvgHandle *
svg_handle_new_from_layout (GridLayout *layout,
                            Coloring    coloring)
{
  GString *svg = svg_from_layout (layout);
  RsvgHandle *handle = svg_handle_from_string (svg, coloring);
  g_string_free (svg, TRUE);

  return handle;
}

RsvgHandle *
svg_handle_new_overlays_from_layout (GridLayout *layout,
                                     Coloring    coloring)
{
  GString *svg = svg_overlays_from_layout (layout);
  RsvgHandle *handle = svg_handle_from_string (svg, coloring);
  g_string_free (svg, TRUE);

  return handle;
}
