/* edit-greeter.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "crosswords-misc.h"
#include "edit-greeter.h"
#include "edit-greeter-details.h"
#include "edit-window.h"
#include "play-grid.h"
#include "puzzle-downloader.h"


#define TOGGLE_DATA "toggle-data"


static void edit_greeter_init           (EditGreeter      *self);
static void edit_greeter_class_init     (EditGreeterClass *klass);
static void edit_greeter_dispose        (GObject          *object);
static void edit_greeter_activated      (EditGreeter      *self);
static void edit_greeter_picker_toggled (EditGreeter      *self,
                                         GtkToggleButton  *toggle);
static void edit_greeter_actions_create (EditGreeter      *self,
                                         const gchar      *action_name,
                                         GVariant         *param);
static void edit_greeter_actions_import (EditGreeter      *self,
                                         const gchar      *action_name,
                                         GVariant         *param);


struct _EditGreeter
{
  AdwApplicationWindow parent_instance;

  GtkWidget *grid;
  GtkWidget *stack;
  GtkWidget *create_button;

  IPuzPuzzleKind kind;
};


G_DEFINE_TYPE (EditGreeter, edit_greeter, ADW_TYPE_APPLICATION_WINDOW);


static GtkWidget *
greeter_picker_new (EditGreeter    *self,
                    IPuzPuzzleKind  kind,
                    const gchar    *puzzle_path,
                    guint           base_size,
                    guint           border_size,
                    GtkWidget      *group)
{
  /* FIXME(svg): It would be good to replace this with images. The
   * grids are pretty heavyweight for what we get */
  GtkWidget *button;
  GtkWidget *grid;
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  g_autofree GridState *state = NULL;
  LayoutConfig config = {
    .base_size = base_size,
    .border_size = border_size,
  };

  button = g_object_new (GTK_TYPE_TOGGLE_BUTTON,
                         "hexpand", TRUE,
                         "vexpand", TRUE,
                         NULL);
  grid = g_object_new (PLAY_TYPE_GRID,
                       "can_focus", FALSE,
                       "halign", GTK_ALIGN_CENTER,
                       "valign", GTK_ALIGN_CENTER,
                       NULL);
  puzzle = xwd_load_puzzle_from_resource (NULL, puzzle_path, NULL);
  gtk_button_set_child (GTK_BUTTON (button), grid);
  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_VIEW);
  gtk_toggle_button_set_group (GTK_TOGGLE_BUTTON (button), GTK_TOGGLE_BUTTON (group));
  g_object_set_data (G_OBJECT (button), TOGGLE_DATA, GINT_TO_POINTER (kind));
  g_signal_connect_swapped (button, "toggled", G_CALLBACK (edit_greeter_picker_toggled), self);

  play_grid_update_state (PLAY_GRID (grid), state, config);

  return button;
}

static void
edit_greeter_init (EditGreeter *self)
{
  GtkWidget *group;
  GtkWidget *button;

  self->kind = IPUZ_PUZZLE_CROSSWORD;

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "edit-greeter");

  button = greeter_picker_new (self, IPUZ_PUZZLE_CROSSWORD, "/org/gnome/Crosswords/crosswords/thumb-crossword.ipuz", 8, 1, NULL);
  group = button;
  gtk_grid_attach (GTK_GRID (self->grid), button, 0, 0, 1, 1);

  button = greeter_picker_new (self, IPUZ_PUZZLE_CRYPTIC, "/org/gnome/Crosswords/crosswords/thumb-cryptic.ipuz", 8, 1, group);
  gtk_grid_attach (GTK_GRID (self->grid), button, 1, 0, 1, 1);

  button = greeter_picker_new (self, IPUZ_PUZZLE_BARRED, "/org/gnome/Crosswords/crosswords/thumb-barred.ipuz", 8, 1, group);
  gtk_grid_attach (GTK_GRID (self->grid), button, 2, 0, 1, 1);

  button = greeter_picker_new (self, IPUZ_PUZZLE_ARROWWORD, "/org/gnome/Crosswords/crosswords/thumb-arrowword.ipuz", 20, 2, group);
  gtk_grid_attach (GTK_GRID (self->grid), button, 0, 1, 1, 1);

  button = greeter_picker_new (self, IPUZ_PUZZLE_FILIPPINE, "/org/gnome/Crosswords/crosswords/thumb-filippine.ipuz", 8, 1, group);
  gtk_grid_attach (GTK_GRID (self->grid), button, 1, 1, 1, 1);

  button = greeter_picker_new (self, IPUZ_PUZZLE_ACROSTIC, "/org/gnome/Crosswords/crosswords/thumb-acrostic.ipuz", 4, 1, group);
  gtk_grid_attach (GTK_GRID (self->grid), button, 2, 1, 1, 1);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (group), TRUE);
}

static void
edit_greeter_class_init (EditGreeterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_greeter_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-greeter.ui");

  gtk_widget_class_bind_template_child (widget_class, EditGreeter, grid);
  gtk_widget_class_bind_template_child (widget_class, EditGreeter, stack);
  gtk_widget_class_bind_template_child (widget_class, EditGreeter, create_button);
  gtk_widget_class_bind_template_callback (widget_class, edit_greeter_activated);

  gtk_widget_class_install_action (widget_class, "win.create", NULL,
                                   (GtkWidgetActionActivateFunc) edit_greeter_actions_create);
  gtk_widget_class_install_action (widget_class, "win.import", NULL,
                                   (GtkWidgetActionActivateFunc) edit_greeter_actions_import);

}

static void
edit_greeter_dispose (GObject *object)
{
  G_OBJECT_CLASS (edit_greeter_parent_class)->dispose (object);
}

static void
edit_greeter_activated (EditGreeter *self)
{
  GtkWidget *window;
  GtkWidget *details;
  GtkApplication *application = NULL;
  g_autoptr (IPuzPuzzle) puzzle = NULL;

  g_object_get (G_OBJECT (self),
                "application", &application,
                NULL);

  details = gtk_stack_get_visible_child (GTK_STACK (self->stack));
  puzzle = edit_greeter_details_get_puzzle (EDIT_GREETER_DETAILS (details));

  window = g_object_new (EDIT_TYPE_WINDOW,
                         "application", application,
                         "puzzle", puzzle,
                         NULL);
  gtk_window_present (GTK_WINDOW (window));
  gtk_window_destroy (GTK_WINDOW (self));
}

static void
edit_greeter_picker_toggled (EditGreeter     *self,
                             GtkToggleButton *toggle)
{
  if (gtk_toggle_button_get_active (toggle))
    {
      IPuzPuzzleKind kind;
      const gchar *name;

      kind = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (toggle), TOGGLE_DATA));
      switch (kind)
        {
        case IPUZ_PUZZLE_CROSSWORD:
          name = "crossword";
          break;
        case IPUZ_PUZZLE_CRYPTIC:
          name = "cryptic";
          break;
        case IPUZ_PUZZLE_BARRED:
          name = "barred";
          break;
        case IPUZ_PUZZLE_ARROWWORD:
          name = "arrowword";
          break;
        case IPUZ_PUZZLE_FILIPPINE:
          name = "filippine";
          break;
        case IPUZ_PUZZLE_ACROSTIC:
          name = "acrostic";
          break;
        default:
          g_assert_not_reached ();
        }

      /* Quick hack until we have more types working */
      if (kind == IPUZ_PUZZLE_CROSSWORD || kind == IPUZ_PUZZLE_CRYPTIC || kind == IPUZ_PUZZLE_BARRED)
        gtk_widget_set_sensitive (self->create_button, TRUE);
      else
        gtk_widget_set_sensitive (self->create_button, FALSE);
      gtk_stack_set_visible_child_name (GTK_STACK (self->stack), name);
      self->kind = kind;
    }
}

static void
edit_greeter_actions_create (EditGreeter *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  edit_greeter_activated (self);
}

static void
edit_greeter_actions_import (EditGreeter *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  PuzzleDownloader *downloader;

  /* FIXME: Make this work */
  downloader = puzzle_downloader_new_filechooser ();

  puzzle_downloader_run_async (downloader, GTK_WINDOW (self), NULL, NULL);
}
