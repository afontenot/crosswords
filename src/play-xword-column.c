/* play-xword-column.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "play-xword-column.h"
#include <adwaita.h>


enum
{
  PROP_0,
  PROP_SPACING,
  N_PROPS
};


enum
{
  VISIBILITY_CHANGED,
  LAST_SIGNAL
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint play_xword_column_signals[LAST_SIGNAL] = { 0 };
static GtkBuildableIface *parent_buildable_iface;


typedef enum
{
  CLUE_VISIBLE = 1 << 0,
  PRIMARY_VISIBLE = 1 << 1,
  SECONDARY_VISIBLE = 1 << 2,
} Visibility;

#define ANIMATION_DURATION 400


#define CHECK_CLUE_VISIBLE(v)                   (((v)&CLUE_VISIBLE)==CLUE_VISIBLE)
#define CHECK_PRIMARY_VISIBLE(v)                (((v)&PRIMARY_VISIBLE)==PRIMARY_VISIBLE)
#define CHECK_SECONDARY_VISIBLE(v)              (((v)&SECONDARY_VISIBLE)==SECONDARY_VISIBLE)
#define START_PRIMARY_ANIMATION(v_orig,v_new)   (v_orig && CHECK_PRIMARY_VISIBLE(v_orig^v_new))
#define START_SECONDARY_ANIMATION(v_orig,v_new) (v_orig && CHECK_SECONDARY_VISIBLE(v_orig^v_new))


struct _PlayXwordColumn
{
  GtkWidget parent_instance;

  gint spacing;
  GtkWidget *intro;
  GtkWidget *grid;
  GtkWidget *grid_swindow;
  GtkWidget *notes;
  GtkWidget *clue;
  GtkWidget *primary;
  GtkWidget *secondary;
  GtkWidget *action;

  Visibility visibility;

  /* Animation */
  gboolean skip_next_anim;
  AdwAnimation *primary_animation;
  AdwAnimation *secondary_animation;
  gfloat primary_value;
  gfloat secondary_value;
  GtkAllocation grid_start_alloc;
  GtkAllocation grid_end_alloc;
  GtkAllocation notes_start_alloc;
  GtkAllocation notes_end_alloc;
};


static void               play_xword_column_init                (PlayXwordColumn      *self);
static void               play_xword_column_class_init          (PlayXwordColumnClass *klass);
static void               play_xword_column_set_property        (GObject              *object,
                                                                 guint                 prop_id,
                                                                 const GValue         *value,
                                                                 GParamSpec           *pspec);
static void               play_xword_column_get_property        (GObject              *object,
                                                                 guint                 prop_id,
                                                                 GValue               *value,
                                                                 GParamSpec           *pspec);
static void               play_xword_column_dispose             (GObject              *object);
static void               play_xword_column_size_allocate       (GtkWidget            *widget,
                                                                 int                   width,
                                                                 int                   height,
                                                                 int                   baseline);
static void               play_xword_column_measure             (GtkWidget            *widget,
                                                                 GtkOrientation        orientation,
                                                                 int                   for_size,
                                                                 int                  *minimum,
                                                                 int                  *natural,
                                                                 int                  *minimum_baseline,
                                                                 int                  *natural_baseline);
static GtkSizeRequestMode play_xword_column_get_request_mode    (GtkWidget            *widget);
static void               play_xword_column_buildable_init      (GtkBuildableIface    *iface);
static void               play_xword_column_buildable_add_child (GtkBuildable         *buildable,
                                                                 GtkBuilder           *builder,
                                                                 GObject              *child,
                                                                 const char           *type);


G_DEFINE_TYPE_WITH_CODE (PlayXwordColumn, play_xword_column, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE,
                                                play_xword_column_buildable_init));


static void
play_xword_column_init (PlayXwordColumn *self)
{
  self->spacing = 16;
  self->visibility = 0;
  self->skip_next_anim = TRUE;
}

static void
play_xword_column_class_init (PlayXwordColumnClass *klass)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = G_OBJECT_CLASS (klass);
  widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = play_xword_column_set_property;
  object_class->get_property = play_xword_column_get_property;
  object_class->dispose = play_xword_column_dispose;
  widget_class->size_allocate = play_xword_column_size_allocate;
  widget_class->measure = play_xword_column_measure;
  widget_class->get_request_mode = play_xword_column_get_request_mode;

  play_xword_column_signals [VISIBILITY_CHANGED] =
    g_signal_new ("visibility-changed",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_SPACING] = g_param_spec_uint ("spacing",
                                               "Spacing",
                                               "The space to put between the crossword elements",
                                               0, G_MAXUINT, 0,
                                               G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
play_xword_column_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  PlayXwordColumn *self = PLAY_XWORD_COLUMN (object);

  switch (prop_id)
    {
    case PROP_SPACING:
      self->spacing = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_xword_column_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  PlayXwordColumn *self = PLAY_XWORD_COLUMN (object);

  switch (prop_id)
    {
    case PROP_SPACING:
      g_value_set_uint (value, self->spacing);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_xword_column_dispose (GObject *object)
{
  PlayXwordColumn *self = PLAY_XWORD_COLUMN (object);

  g_clear_object (&self->primary_animation);
  g_clear_object (&self->secondary_animation);

  g_clear_pointer (&self->intro, gtk_widget_unparent);
  g_clear_pointer (&self->grid_swindow, gtk_widget_unparent);
  g_clear_pointer (&self->notes, gtk_widget_unparent);
  g_clear_pointer (&self->clue, gtk_widget_unparent);
  g_clear_pointer (&self->primary, gtk_widget_unparent);
  g_clear_pointer (&self->secondary, gtk_widget_unparent);
  g_clear_pointer (&self->action, gtk_widget_unparent);

  G_OBJECT_CLASS (play_xword_column_parent_class)->dispose (object);
}

static void
child_relative_allocation (GtkWidget *child,
                           gint       target_width,
                           gint      *height,
                           gint      *visible_children)
{
  if (gtk_widget_should_layout (child))
    {
      gint child_nat_min, child_min_baseline, child_nat_baseline;
      gtk_widget_measure (child, GTK_ORIENTATION_VERTICAL, target_width,
                          height, &child_nat_min,
                          &child_min_baseline, &child_nat_baseline);
      *visible_children += 1;
    }
}


static void
primary_animation_done_cb (AdwAnimation    *animation,
                           PlayXwordColumn *self)
{
  gtk_widget_set_child_visible (self->clue, (self->primary_value < 1.0));
  gtk_widget_set_child_visible (self->primary, (self->primary_value > 0.0));
}

static void
secondary_animation_done_cb (AdwAnimation    *animation,
                             PlayXwordColumn *self)
{
  gtk_widget_set_child_visible (self->secondary, (self->secondary_value > 0.0));
}

static void
primary_animation_cb (double   value,
                      gpointer user_data)
{
  PlayXwordColumn *self = user_data;
  g_assert ((value <= 1.0) && (value >= 0.0));

  if (self->primary_value != value)
    {
      self->primary_value = value;
      gtk_widget_queue_allocate (GTK_WIDGET (self));
    }
  g_clear_object (&self->primary_animation);
}

static void
secondary_animation_cb (double   value,
                        gpointer user_data)
{
  PlayXwordColumn *self = user_data;
  g_assert ((value <= 1.0) && (value >= 0.0));

  if (self->secondary_value != value)
    {
      self->secondary_value = value;
      gtk_widget_queue_allocate (GTK_WIDGET (self));
    }
  g_clear_object (&self->secondary_animation);
}

static void
run_primary_animation (PlayXwordColumn *self,
                       gboolean         target_visibility)
{
  AdwAnimationTarget *target;

  g_clear_pointer (&self->primary_animation, g_object_unref);

  target = adw_callback_animation_target_new (primary_animation_cb, self, NULL);
  self->primary_animation =
    adw_timed_animation_new (GTK_WIDGET (self), 1.0, 0.0, ANIMATION_DURATION, target);
  adw_timed_animation_set_easing (ADW_TIMED_ANIMATION (self->primary_animation),
                                  ADW_EASE_IN_OUT_QUART);
  g_signal_connect (self->primary_animation, "done",
                    G_CALLBACK (primary_animation_done_cb),
                    self);

  adw_timed_animation_set_reverse (ADW_TIMED_ANIMATION (self->primary_animation), target_visibility);
  adw_animation_play (self->primary_animation);
}

static void
run_secondary_animation (PlayXwordColumn *self,
                         gboolean         target_visibility)
{
  AdwAnimationTarget *target;

  g_clear_pointer (&self->secondary_animation, g_object_unref);

  target = adw_callback_animation_target_new (secondary_animation_cb, self, NULL);
  self->secondary_animation =
    adw_timed_animation_new (GTK_WIDGET (self), 1.0, 0.0, ANIMATION_DURATION, target);
  adw_timed_animation_set_easing (ADW_TIMED_ANIMATION (self->secondary_animation),
                                  ADW_EASE_IN_OUT_QUART);
  g_signal_connect (self->secondary_animation, "done",
                    G_CALLBACK (secondary_animation_done_cb),
                    self);

  adw_timed_animation_set_reverse (ADW_TIMED_ANIMATION (self->secondary_animation), target_visibility);
  adw_animation_play (self->secondary_animation);
}

static gboolean
allocate_widget (GtkWidget *widget,
                 gint       x,
                 gint      *y,
                 gint       width,
                 gint       height,
                 gint       spacing)
{
  GtkAllocation allocation = {
    .x = x,
    .y = *y,
    .width = width,
    .height = height,
  };

  if (gtk_widget_should_layout (widget))
    {
      gtk_widget_size_allocate (widget, &allocation, -1);
      *y += height + spacing;
      return TRUE;
    }
  return FALSE;
}

Visibility
calculate_new_visible (PlayXwordColumn *self,
                       gint             width,
                       gint             grid_width,
                       gint             primary_width,
                       gint             secondary_width)
{
  if (width >= (grid_width + primary_width + secondary_width + self->spacing * 4))
    return (PRIMARY_VISIBLE | SECONDARY_VISIBLE);
  else if (width >= (grid_width + primary_width + self->spacing * 3))
    return PRIMARY_VISIBLE;
  return CLUE_VISIBLE;
}

static void
play_xword_column_size_allocate (GtkWidget *widget,
                                 int        width,
                                 int        height,
                                 int        baseline)
{
  PlayXwordColumn *self;
  gint grid_width;
  gint grid_height;
  gint grid_nat_width;
  gint grid_nat_height;
  gint child_nat = 0;
  gint child_min_baseline = -1;
  gint child_nat_baseline = -1;
  gint visible_children = 0;
  gint swindow_min = 0;
  gint intro_height = 0;
  gint notes_height = 0;
  gint swindow_height = 0;
  gint action_height = 0;
  gint primary_width = 0;
  gint secondary_width = 0;
  gint clue_height = 0;
  gint x, y;
  Visibility new_visibility;
  gboolean initial_allocation = FALSE;
  gboolean intro_is_empty = TRUE;
  gboolean start_primary_animation, start_secondary_animation;
  gint column_width;

  self = PLAY_XWORD_COLUMN (widget);

  /* This is a little bit of a bodge. In compact mode, we only want to
   * show the intro label if there's actually text. In non-compact
   * mode, we show it regardless of the text situation. */
  if (GTK_IS_LABEL (self->intro))
    {
      const gchar *intro_text;
      intro_text = gtk_label_get_text (GTK_LABEL (self->intro));
      if (intro_text && *intro_text)
        intro_is_empty = FALSE;
    }
  else
    g_assert_not_reached ();

#if 0
  g_print ("allocate: width %d   height %d\n", width, height);
#endif

  g_assert (self->intro != NULL && self->grid != NULL &&
            self->notes != NULL && self->clue != NULL &&
            self->primary != NULL && self->secondary != NULL);

  /* Get grid_width. we calculate our overage based on that. */
  gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, -1,
                      &grid_width, &grid_nat_width,
                      &child_min_baseline, &child_nat_baseline);
  gtk_widget_measure (self->grid, GTK_ORIENTATION_VERTICAL, -1,
                      &grid_height, &grid_nat_height,
                      &child_min_baseline, &child_nat_baseline);
  gtk_widget_measure (self->grid_swindow, GTK_ORIENTATION_VERTICAL, -1,
                      &swindow_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);
  visible_children ++; /* For the grid */

  /* Measure the width of the PlayClueLists */
  if (gtk_widget_should_layout (self->primary))
    gtk_widget_measure  (self->primary, GTK_ORIENTATION_HORIZONTAL, -1,
                         &primary_width, &child_nat,
                         &child_min_baseline, &child_nat_baseline);
  if (gtk_widget_should_layout (self->secondary))
    gtk_widget_measure  (self->secondary, GTK_ORIENTATION_HORIZONTAL, -1,
                         &secondary_width, &child_nat,
                         &child_min_baseline, &child_nat_baseline);

  /* We use the widths to figure out what widgets are visible */
  new_visibility = calculate_new_visible (self, width,
                                          grid_nat_width,
                                          primary_width,
                                          secondary_width);

  /* if primary clues aren't visible, then we only display the
   * grid. In this instance, we see if we have room for the expanded
   * grid, or need to shrink*/
  if (!CHECK_PRIMARY_VISIBLE (new_visibility) &&
      (width - self->spacing *2 < grid_nat_width))
    {
      gint unused;

      gtk_widget_measure (self->grid, GTK_ORIENTATION_VERTICAL, width - self->spacing *2,
                          &unused, &grid_height,
                          &child_min_baseline, &child_nat_baseline);
      gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, grid_height,
                          &unused, &grid_width,
                          &child_min_baseline, &child_nat_baseline);
    }
  else
    {
      grid_width = grid_nat_width;
      grid_height = grid_nat_height;
    }


  /* We should always have something visible. So if self->visibility
   * is 0, that means that we are in the initial allocation. This
   * means we ignore the current _values, and initialize them. Also,
   * we should ignore animations.
   */
  if (self->visibility == 0)
    initial_allocation = TRUE;
  start_primary_animation = START_PRIMARY_ANIMATION (self->visibility, new_visibility);
  start_secondary_animation = START_SECONDARY_ANIMATION (self->visibility, new_visibility);

  /* Warning: This is potentially loopy behavior. The
   * visibilty_changed signal changes the polarity of the across/down
   * clues. We are counting on the fact that:
   * 1) PlayClueLists are all about the same width, and fixed in size.
   * 2) Nothing else about the UI is changing in that signal handler
   */
  if (new_visibility != self->visibility)
    {
      self->visibility = new_visibility;
      g_signal_emit (self, play_xword_column_signals[VISIBILITY_CHANGED], 0);

      /* These may have changed */
      if (gtk_widget_should_layout (self->primary))
        gtk_widget_measure (self->primary, GTK_ORIENTATION_HORIZONTAL, -1,
                            &primary_width, &child_nat,
                            &child_min_baseline, &child_nat_baseline);
      if (gtk_widget_should_layout (self->secondary))
        gtk_widget_measure (self->secondary, GTK_ORIENTATION_HORIZONTAL, -1,
                            &secondary_width, &child_nat,
                            &child_min_baseline, &child_nat_baseline);
    }
  else
    self->visibility = new_visibility;

  /* initialize the primary/secondary_value to match the width. We
   * aren't mid-animation by definition
   */
  if (initial_allocation || self->skip_next_anim)
    {
      self->primary_value = CHECK_PRIMARY_VISIBLE (self->visibility) ? 1.0 : 0.0;
      self->secondary_value = CHECK_SECONDARY_VISIBLE (self->visibility) ? 1.0 : 0.0;
    }
  /* Sanity check our values */
  self->primary_value = CLAMP (self->primary_value, 0.0, 1.0);
  self->secondary_value = CLAMP (self->secondary_value, 0.0, 1.0);

  /* MEASUREMENT */
  /* Measure the labels, so we will have measured all the widgets */
  child_relative_allocation (self->intro, grid_width, &intro_height, &visible_children);
  child_relative_allocation (self->notes, grid_width, &notes_height, &visible_children);
  child_relative_allocation (self->clue, grid_width, &clue_height, &visible_children);
  child_relative_allocation (self->action, grid_width, &action_height, &visible_children);

  /* measure the swindow. This is a really complex height
   * calculation. Touch it with care.
   * This is where the magic happens.
   */
  swindow_height = height - ((intro_is_empty?((intro_height + self->spacing) * (self->primary_value)) : (intro_height + self->spacing)) +
                             notes_height +
                             (self->spacing + clue_height + action_height) * (1.0-self->primary_value ) +
                             (visible_children - 3) * self->spacing);
  swindow_height = MIN (swindow_height, grid_height);
  swindow_height = MAX (swindow_height, swindow_min);

  /* ALLOCATION */
  /* Now we have enough to allocate everything. */
  column_width = grid_width +
    self->primary_value * primary_width +
    self->secondary_value * secondary_width +
    self->spacing * 2;
  x = MAX (self->spacing, self->spacing + (width - column_width)/2);
  y = 0;

  /* always allocate the intro regardless of what it contains */
  allocate_widget (self->intro, x, &y, grid_width, intro_height, self->spacing);
  /* shift the grid up if we're in compact mode and the intro is
   * empty */
  if (intro_is_empty)
    y -= (intro_height + self->spacing) * (1.0 - self->primary_value);

  allocate_widget (self->grid_swindow, x, &y, grid_width, swindow_height, self->spacing);
  allocate_widget (self->notes, x, &y, grid_width, notes_height, self->spacing);
  allocate_widget (self->clue, x, &y, grid_width, clue_height, self->spacing);
  y = height - (action_height * (1.0 - self->primary_value));
  allocate_widget (self->action, 0, &y, width, action_height, self->spacing);
  y = 0;
  allocate_widget (self->primary, x + grid_width + self->spacing, &y, primary_width, height, 0);
  y = 0;
  allocate_widget (self->secondary, x + grid_width + primary_width + 2*self->spacing, &y, secondary_width, height, 0);

  /* Set the opacity and visibility based on the current *_value */
  gtk_widget_set_opacity (self->clue, 1.0 - self->primary_value);
  gtk_widget_set_opacity (self->action, 1.0 - self->primary_value);
  gtk_widget_set_opacity (self->primary, self->primary_value);
  gtk_widget_set_opacity (self->secondary, self->secondary_value);

  if (intro_is_empty)
    gtk_widget_set_child_visible (self->intro, (self->primary_value > 0.0));
  else
    gtk_widget_set_child_visible (self->intro, TRUE);
  gtk_widget_set_child_visible (self->clue, (self->primary_value < 1.0));
  gtk_widget_set_child_visible (self->action, (self->primary_value < 1.0));
  gtk_widget_set_child_visible (self->primary, (self->primary_value > 0.0));
  gtk_widget_set_child_visible (self->secondary, (self->secondary_value > 0.0));

  /* Start animations */
  if (! self->skip_next_anim)
    {
      if (start_primary_animation)
        run_primary_animation (self, CHECK_PRIMARY_VISIBLE (new_visibility));
      if (start_secondary_animation)
        run_secondary_animation (self, CHECK_SECONDARY_VISIBLE (new_visibility));
    }
  self->skip_next_anim = FALSE;
}

static void
play_xword_column_measure (GtkWidget      *widget,
                           GtkOrientation  orientation,
                           int             for_size,
                           int            *minimum,
                           int            *natural,
                           int            *minimum_baseline,
                           int            *natural_baseline)
{
  PlayXwordColumn *self;
  gint child_min = 0;
  gint child_nat = 0;
  gint child_min_baseline = -1;
  gint child_nat_baseline = -1;
  gint target_width;
  gint visible_children = 0;

  self = PLAY_XWORD_COLUMN (widget);

  g_assert (self->intro != NULL && self->grid != NULL &&
            self->notes != NULL && self->clue != NULL);

  *minimum = 0;
  *natural = 0;

  /* get the width of the grid. we calculate our height for wrapped
   * text based on that, instead of the height we're given. */
  gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, -1,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);
  target_width = child_min;

  /* If we're measuring the horizontal, the grid size is all we
   * need */
  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
      *minimum = target_width + self->spacing * 2;
      *natural = target_width + self->spacing * 2;
      return;
    }

  /* We want the vertical size. We need to go through the widgets on
   * the left and add them their heights. The primary and secondary
   * clues don't matter */
  for (GtkWidget *child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      if (gtk_widget_should_layout (child) && gtk_widget_get_child_visible (child))
        {
          gtk_widget_measure (child, orientation, target_width,
                              &child_min, &child_nat,
                              &child_min_baseline, &child_nat_baseline);

          /* Skip these widgets */
          if (widget == self->primary ||
              widget == self->secondary)
            continue;

          *minimum += child_min;
          *natural += child_nat;
          visible_children ++;

          if (child_min_baseline > -1)
            *minimum_baseline = MAX (*minimum_baseline, child_min_baseline);
          if (child_nat_baseline > -1)
            *natural_baseline = MAX (*natural_baseline, child_nat_baseline);
        }
    }

  *minimum += (visible_children - 1)*self->spacing;
  *natural += (visible_children - 1)*self->spacing;
  //g_print ("measure (%s): minimum: %d natural: %d\n", orientation==GTK_ORIENTATION_HORIZONTAL?"horizontal":"vertical", *minimum, *natural);
}

static GtkSizeRequestMode
play_xword_column_get_request_mode (GtkWidget *widget)
{
  return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
play_xword_column_buildable_init (GtkBuildableIface *iface)
{
  parent_buildable_iface = g_type_interface_peek_parent (iface);

  iface->add_child = play_xword_column_buildable_add_child;
}

static void
set_grid_widget (PlayXwordColumn *self,
                 GtkWidget       *grid)
{
  if (self->grid == grid)
    return;

  if (self->grid_swindow)
    g_clear_pointer (&self->grid_swindow, gtk_widget_unparent);
  self->grid = grid;

  if (grid)
    {
      GtkWidget *viewport;
      /* Put this in a scrolled window */
      self->grid_swindow =
        (GtkWidget *) g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                                    "valign", GTK_ALIGN_FILL,
                                    "vexpand", TRUE,
                                    "vscrollbar-policy", GTK_POLICY_AUTOMATIC,
                                    "hscrollbar-policy", GTK_POLICY_NEVER,
                                    NULL);
      viewport = g_object_new (GTK_TYPE_VIEWPORT,
                               "vscroll-policy", GTK_SCROLL_NATURAL,
                               NULL);
      gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW (self->grid_swindow), viewport);
      gtk_viewport_set_child (GTK_VIEWPORT (viewport), self->grid);
      gtk_widget_set_parent (self->grid_swindow, GTK_WIDGET (self));
    }
}

static void
set_buildable_child (PlayXwordColumn  *self,
                     GtkWidget        *child,
                     GtkWidget       **dest)
{
  if (*dest == child)
    return;

  if (*dest)
    gtk_widget_unparent (*dest);
  *dest = child;

  if (child)
    gtk_widget_set_parent (*dest, GTK_WIDGET (self));
}

static void
play_xword_column_buildable_add_child (GtkBuildable *buildable,
                                       GtkBuilder   *builder,
                                       GObject      *object,
                                       const char   *type)
{
  PlayXwordColumn *self;
  GtkWidget *child;

  self = PLAY_XWORD_COLUMN (buildable);
  child = GTK_WIDGET (object);

  if (type && strcmp (type, "intro") == 0)
    set_buildable_child (self, child, &self->intro);
  else if (type && strcmp (type, "grid") == 0)
    set_grid_widget (self, child);
  else if (type && strcmp (type, "notes") == 0)
    set_buildable_child (self, child, &self->notes);
  else if (type && strcmp (type, "clue") == 0)
    set_buildable_child (self, child, &self->clue);
  else if (type && strcmp (type, "primary") == 0)
    set_buildable_child (self, child, &self->primary);
  else if (type && strcmp (type, "secondary") == 0)
    set_buildable_child (self, child, &self->secondary);
  else if (type && strcmp (type, "action") == 0)
    set_buildable_child (self, child, &self->action);
  else
    parent_buildable_iface->add_child (buildable, builder, object, type);
}

/* Public Methods */

void
play_xword_column_set_skip_next_anim (PlayXwordColumn *xword_column,
                                      gboolean         skip_next_anim)
{
  g_return_if_fail (PLAY_IS_XWORD_COLUMN (xword_column));

  xword_column->skip_next_anim = !!skip_next_anim;
}

gboolean
play_xword_column_get_clue_visible (PlayXwordColumn *xword_column)
{
  g_return_val_if_fail (PLAY_IS_XWORD_COLUMN (xword_column), 0);

  return CHECK_CLUE_VISIBLE (xword_column->visibility);
}

gboolean
play_xword_column_get_primary_visible (PlayXwordColumn *xword_column)
{
  g_return_val_if_fail (PLAY_IS_XWORD_COLUMN (xword_column), 0);

  return CHECK_PRIMARY_VISIBLE (xword_column->visibility);
}

gboolean
play_xword_column_get_secondary_visible (PlayXwordColumn *xword_column)
{
  g_return_val_if_fail (PLAY_IS_XWORD_COLUMN (xword_column), 0);

  return CHECK_SECONDARY_VISIBLE (xword_column->visibility);
}
