/* edit-autofill-dialog.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <glib/gi18n.h>
#include "edit-autofill-dialog.h"
#include "edit-puzzle-stack.h"
#include "play-grid.h"
#include "word-list.h"
#include "word-list-model.h"
#include "word-solver.h"

struct _EditAutofillDialog
{
  GtkDialog dialog;
  IPuzPuzzle *puzzle;

  WordSolver *solver;
  guint timeout;
  guint finished_handler;


  /* Needed for the word skip list */
  WordList *word_list;
  WordArrayModel *skip_model;

  /* Template widgets */
  GtkWidget *puzzle_stack;

  GtkWidget *solver_spinner;
  GtkWidget *solver_label;
  GtkWidget *refresh_button;
  GtkWidget *solver_button;

  GtkWidget *skip_word_group;
  GtkWidget *skip_word_list_box;
  GtkWidget *skip_word_entry;
  GObject *skip_word_entry_buffer;
  GtkWidget *skip_word_add_button;

  GtkWidget *constraints_group;
  GtkWidget *solutions_switch;
  GtkWidget *solutions_spin;
  GtkWidget *priorities_switch;
  GtkWidget *priorities_scale;
  GObject *priorities_adjustment;

  GtkWidget *button_use;
};


static void       edit_autofill_dialog_init       (EditAutofillDialog      *self);
static void       edit_autofill_dialog_class_init (EditAutofillDialogClass *klass);
static void       edit_autofill_dialog_dispose    (GObject                 *object);

/* callbacks */
static GtkWidget *skip_list_create_widget_func    (WordListModelRow        *item,
                                                   EditAutofillDialog      *autofill_dialog);
static void       stack_selection_changed_cb      (EditAutofillDialog      *autofill_dialog);
static void       refresh_button_clicked_cb       (EditAutofillDialog      *autofill_dialog);
static void       solver_button_clicked_cb        (EditAutofillDialog      *autofill_dialog);
static void       solver_finished_cb              (EditAutofillDialog      *autofill_dialog);
static void       skip_entry_add_word_cb          (EditAutofillDialog      *autofill_dialog);
static void       skip_entry_buffer_changed_cb    (EditAutofillDialog      *autofill_dialog,
                                                   GParamSpec              *pspec,
                                                   GtkEntryBuffer          *buffer);
static void       solutions_switch_toggled_cb     (EditAutofillDialog      *autofill_dialog,
                                                   GParamSpec              *pspec,
                                                   GtkSwitch               *soln_switch);
static void       priorities_switch_toggled_cb    (EditAutofillDialog      *autofill_dialog,
                                                   GParamSpec              *pspec,
                                                   GtkSwitch               *prio_switch);


G_DEFINE_TYPE (EditAutofillDialog, edit_autofill_dialog, GTK_TYPE_DIALOG);


static void
edit_autofill_dialog_init (EditAutofillDialog *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));


  self->solver = word_solver_new ();
  edit_puzzle_stack_set_solver (EDIT_PUZZLE_STACK (self->puzzle_stack), self->solver);
  self->finished_handler = g_signal_connect_swapped (self->solver, "finished", G_CALLBACK (solver_finished_cb), self);

  self->word_list = word_list_new ();
  self->skip_model = word_array_model_new ();
  gtk_list_box_bind_model (GTK_LIST_BOX (self->skip_word_list_box),
                           G_LIST_MODEL (self->skip_model),
                           (GtkListBoxCreateWidgetFunc) skip_list_create_widget_func,
                           self, NULL);
  gtk_widget_add_css_class (GTK_WIDGET (self), "autofill");
}

static void
edit_autofill_dialog_class_init (EditAutofillDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_autofill_dialog_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-autofill-dialog.ui");

  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, puzzle_stack);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, solver_spinner);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, solver_label);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, refresh_button);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, solver_button);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, skip_word_group);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, skip_word_list_box);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, skip_word_entry);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, skip_word_entry_buffer);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, skip_word_add_button);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, constraints_group);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, solutions_switch);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, solutions_spin);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, priorities_switch);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, priorities_scale);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, priorities_adjustment);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDialog, button_use);

  gtk_widget_class_bind_template_callback (widget_class, stack_selection_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, refresh_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, solver_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, skip_entry_add_word_cb);
  gtk_widget_class_bind_template_callback (widget_class, skip_entry_buffer_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, solutions_switch_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, priorities_switch_toggled_cb);
}

static void
edit_autofill_dialog_dispose (GObject *object)
{
  EditAutofillDialog *self;

  self = EDIT_AUTOFILL_DIALOG (object);

  /*Clear out the solver. Since it will fire asynchronously once the
   * task finishes, we need to manually clear this finished handler */
  if (self->finished_handler && self->solver)
    {
      g_signal_handler_disconnect (self->solver, self->finished_handler);
      self->finished_handler = 0;
    }

  if (self->timeout)
    {
      g_source_remove (self->timeout);
      self->timeout = 0;
    }

  if (self->solver &&
      word_solver_get_state (self->solver) == WORD_SOLVER_RUNNING)
    word_solver_cancel (self->solver);

  g_clear_object (&self->puzzle);
  g_clear_object (&self->solver);
  g_clear_object (&self->word_list);
  g_clear_object (&self->skip_model);

  G_OBJECT_CLASS (edit_autofill_dialog_parent_class)->dispose (object);
}

static gboolean
update_running (EditAutofillDialog *autofill_dialog)
{
  g_autofree gchar *text = NULL;
  gboolean skip_word_group_sensitive = TRUE;
  gboolean constraints_group_sensitive = TRUE;
  gboolean retval = FALSE;
  WordSolverState state;

  state = word_solver_get_state (autofill_dialog->solver);
  switch (state)
    {
    case WORD_SOLVER_COMPLETE:
    case WORD_SOLVER_READY:
      if (state == WORD_SOLVER_COMPLETE)
        text = g_strdup_printf ("%d boards tried", word_solver_get_count (autofill_dialog->solver));

      gtk_widget_remove_css_class (autofill_dialog->solver_button, "destructive-action");
      gtk_widget_add_css_class (autofill_dialog->solver_button, "suggested-action");
      gtk_button_set_label (GTK_BUTTON (autofill_dialog->solver_button),
                            _("Start Autofill"));
      gtk_spinner_set_spinning (GTK_SPINNER (autofill_dialog->solver_spinner), FALSE);
      autofill_dialog->timeout = 0;
      break;
    case WORD_SOLVER_RUNNING:
      gtk_widget_remove_css_class (autofill_dialog->solver_button, "suggested-action");
      gtk_widget_add_css_class (autofill_dialog->solver_button, "destructive-action");
      gtk_button_set_label (GTK_BUTTON (autofill_dialog->solver_button),
                            _("Stop Autofill"));
      gtk_spinner_set_spinning (GTK_SPINNER (autofill_dialog->solver_spinner), TRUE);
      text = g_strdup_printf ("%d boards tried", word_solver_get_count (autofill_dialog->solver));

      skip_word_group_sensitive = FALSE;
      constraints_group_sensitive = FALSE;
      /* Keep the timeout going */
      retval = TRUE;
      break;
    }

  gtk_label_set_text (GTK_LABEL (autofill_dialog->solver_label), text);
  gtk_widget_set_sensitive (autofill_dialog->refresh_button,
                            (state == WORD_SOLVER_COMPLETE));
  gtk_widget_set_sensitive (autofill_dialog->skip_word_group,
                            skip_word_group_sensitive);
  gtk_widget_set_sensitive (autofill_dialog->constraints_group,
                            constraints_group_sensitive);

  /* Tick tock */
  edit_puzzle_stack_update (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack));
  if (edit_puzzle_stack_get_guess (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack)) == NULL)
    gtk_widget_set_sensitive (autofill_dialog->button_use, FALSE);
  else
    gtk_widget_set_sensitive (autofill_dialog->button_use, TRUE);


  return retval;
}

static gint
get_solutions (EditAutofillDialog *autofill_dialog)
{
  if (gtk_switch_get_active (GTK_SWITCH (autofill_dialog->solutions_switch)))
    return (gint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (autofill_dialog->solutions_spin));
  return -1;
}

static gint
get_priority (EditAutofillDialog *autofill_dialog)
{
  if (gtk_switch_get_active (GTK_SWITCH (autofill_dialog->priorities_switch)))
    return (gint) gtk_adjustment_get_value (GTK_ADJUSTMENT (autofill_dialog->priorities_adjustment));
  return 50;
}

static void
autofill_dialog_start (EditAutofillDialog *autofill_dialog)
{
  gint solutions;
  gint priority;

  if (autofill_dialog->timeout != 0)
    {
      /* We didn't stop the timer... ./~ */
      g_warning ("We didn't clear out the old timeout\n");
      g_source_remove (autofill_dialog->timeout);
      autofill_dialog->timeout = 0;
    }

  solutions = get_solutions (autofill_dialog);
  priority = get_priority (autofill_dialog);

  word_solver_run (autofill_dialog->solver,
                   IPUZ_CROSSWORD (autofill_dialog->puzzle),
                   edit_puzzle_stack_get_cell_array (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack)),
                   word_array_model_get_array (autofill_dialog->skip_model),
                   solutions, priority);
  autofill_dialog->timeout = g_timeout_add_seconds (1, G_SOURCE_FUNC (update_running), autofill_dialog);
}


static void
remove_item (GtkWidget   *button,
             const gchar *text)
{
  WordIndex word_index;
  EditAutofillDialog *autofill_dialog;

  autofill_dialog = (EditAutofillDialog *) gtk_widget_get_root (button);
  g_assert (EDIT_IS_AUTOFILL_DIALOG (autofill_dialog));

  if (word_list_lookup_index (autofill_dialog->word_list, text, &word_index))
    word_array_model_remove (autofill_dialog->skip_model, word_index);


}

static GtkWidget *
skip_list_create_widget_func (WordListModelRow   *item,
                              EditAutofillDialog *autofill_dialog)
{
  GtkWidget *retval;
  GtkWidget *label;
  GtkWidget *button;
  g_autofree gchar *text = NULL;

  text = g_strdup_printf ("%s — (%d)",
                          word_list_model_row_get_word (item),
                          word_list_model_row_get_priority (item));
  retval = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);
  label = gtk_label_new (text);
  gtk_widget_set_hexpand (label, TRUE);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_box_prepend (GTK_BOX (retval), label);

  button = gtk_button_new ();
  gtk_button_set_icon_name (GTK_BUTTON (button), "list-remove-symbolic");
  gtk_box_append (GTK_BOX (retval), button);
  g_signal_connect (button, "clicked", G_CALLBACK (remove_item),
                    (gpointer) word_list_model_row_get_word (item));
  return retval;
}

static void
stack_selection_changed_cb (EditAutofillDialog *autofill_dialog)
{
  CellArray *cell_array;

  cell_array = edit_puzzle_stack_get_cell_array (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack));
  gtk_widget_set_sensitive (autofill_dialog->solver_button,
                            cell_array_len (cell_array) > 0);
}


static void
refresh_button_clicked_cb (EditAutofillDialog *autofill_dialog)
{
  if (word_solver_get_state (autofill_dialog->solver) == WORD_SOLVER_COMPLETE)
    word_solver_reset (autofill_dialog->solver);

  update_running (autofill_dialog);
}

static void
solver_button_clicked_cb (EditAutofillDialog *autofill_dialog)
{
  if (word_solver_get_state (autofill_dialog->solver) == WORD_SOLVER_RUNNING)
    {
      g_source_remove (autofill_dialog->timeout);
      autofill_dialog->timeout = 0;
      word_solver_cancel (autofill_dialog->solver);
    }
  else
    {
      word_solver_reset (autofill_dialog->solver);
      autofill_dialog_start (autofill_dialog);
    }

  update_running (autofill_dialog);
}

static void
solver_finished_cb (EditAutofillDialog      *autofill_dialog)
{
  update_running (autofill_dialog);
}

static void
skip_entry_add_word_cb (EditAutofillDialog *autofill_dialog)
{
  const gchar *text;
  WordIndex word_index;

  text = gtk_entry_buffer_get_text (GTK_ENTRY_BUFFER (autofill_dialog->skip_word_entry_buffer));

  if (word_list_lookup_index (autofill_dialog->word_list, text, &word_index))
    {
      word_array_model_add (autofill_dialog->skip_model, word_index);
      gtk_entry_buffer_set_text (GTK_ENTRY_BUFFER (autofill_dialog->skip_word_entry_buffer), "", 0);
    }
}

static void
skip_entry_buffer_changed_cb (EditAutofillDialog *autofill_dialog,
                              GParamSpec         *pspec,
                              GtkEntryBuffer     *buffer)
{
  const gchar *text;
  WordIndex word_index;
  gboolean sensitive = FALSE;

  text = gtk_entry_buffer_get_text (buffer);

  if (word_list_lookup_index (autofill_dialog->word_list, text, &word_index))
    {
      if (! word_array_model_find (autofill_dialog->skip_model,
                                   word_index, NULL))
        sensitive = TRUE;
    }

  gtk_widget_set_sensitive (autofill_dialog->skip_word_add_button, sensitive);
}

static void
solutions_switch_toggled_cb (EditAutofillDialog *autofill_dialog,
                             GParamSpec         *pspec,
                             GtkSwitch          *soln_switch)
{
  gtk_widget_set_sensitive (autofill_dialog->solutions_spin,
                            gtk_switch_get_active (GTK_SWITCH (autofill_dialog->solutions_switch)));
}

static void
priorities_switch_toggled_cb (EditAutofillDialog *autofill_dialog,
                              GParamSpec         *pspec,
                              GtkSwitch          *prio_switch)
{
  gtk_widget_set_sensitive (autofill_dialog->priorities_scale,
                            gtk_switch_get_active (GTK_SWITCH (autofill_dialog->priorities_switch)));
}

/* Public methods */

static void
edit_autofill_dialog_set_puzzle (EditAutofillDialog *autofill_dialog,
                                 IPuzPuzzle         *puzzle)
{
  g_clear_object (&autofill_dialog->puzzle);
  autofill_dialog->puzzle = ipuz_puzzle_deep_copy (puzzle);
  /* Clear the styles */

  edit_puzzle_stack_set_puzzle (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack),
                                autofill_dialog->puzzle);
}

GtkWidget *
edit_autofill_dialog_new (IPuzPuzzle *puzzle)
{
  EditAutofillDialog *autofill_dialog;

  autofill_dialog = g_object_new (EDIT_TYPE_AUTOFILL_DIALOG, NULL);
  edit_autofill_dialog_set_puzzle (autofill_dialog, puzzle);

  return (GtkWidget *) autofill_dialog;
}

IPuzGuesses *
edit_autofill_dialog_get_guess (EditAutofillDialog *autofill_dialog)
{
  g_return_val_if_fail (EDIT_IS_AUTOFILL_DIALOG (autofill_dialog), NULL);

  return edit_puzzle_stack_get_guess (EDIT_PUZZLE_STACK (autofill_dialog->puzzle_stack));
}
