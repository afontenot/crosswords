/* word-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <libipuz/libipuz.h>
#include <json-glib/json-glib.h>

#include "word-list.h"
#include "word-list-index.h"
#include "word-list-misc.h"

#define WORD_LIST_PRIORITY(word) ((gint) (word-WORD_OFFSET)[0])

struct _WordList
{
  GObject parent_object;
  WordListIndex *index;

  /* The mmapped data we get */
  const guchar *data; /* Owned by bytes and mmapped. Do not touch */
  gsize data_size;
  GBytes *bytes;

  /* Set externally */
  gchar *filter;
  gint threshold;
  WordListMode mode;

  /* Internally used to calculate the current filter */
  gboolean word_list_only;
  gint filter_len;
  GArray *list;
};

/* A struct containing the data for a filter fragment. data is
 * mmapped, and shouldn't be freed.
 */
typedef struct
{
  gushort len;  /* Length of the word */
  gushort *data; /* location of the charset from letter_list_offset*/
} FilterFragmentList;

/* A struct for the list of anagram hashes. Note, the pragma pack(1)
 * is because we are packed in repeating 9-byte blocks in the on-disk
 * data, and we don't want the compiler to pad this struct
 */
#pragma pack(push, 1)
typedef struct
{
  guint offset;
  guint hash;
  guchar len;
} AnagramHashIndex;
#pragma pack(pop)


static void         word_list_init               (WordList           *self);
static void         word_list_class_init         (WordListClass      *klass);
static void         word_list_dispose            (GObject            *object);
static void         word_list_load_index         (WordList           *self);
static const gchar *word_list_lookup_word        (WordList           *word_list,
                                                  WordListSection    *section,
                                                  WordIndex           word);
static gboolean     word_list_lookup_fragment    (WordList           *word_list,
                                                  FilterFragment      fragment,
                                                  FilterFragmentList *list);
static void         copy_union_list_to_word_list (WordList           *word_list,
                                                  GArray             *union_list);


G_DEFINE_TYPE (WordList, word_list, G_TYPE_OBJECT);


static void
word_list_init (WordList *self)
{
  self->mode = WORD_LIST_NONE;
}

static void
word_list_class_init (WordListClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_list_dispose;
}

static void
word_list_dispose (GObject *object)
{
  WordList *self;

  self = WORD_LIST (object);

  g_clear_pointer (&self->index, word_list_index_free);
  g_clear_pointer (&self->bytes, g_bytes_unref);
  g_clear_pointer (&self->filter, g_free);
  g_clear_pointer (&self->list, g_array_unref);

  G_OBJECT_CLASS (word_list_parent_class)->dispose (object);
}

/* find the index at the back of mmapped data. */
static void
word_list_load_index (WordList *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;
  gint i;

  for (i = self->data_size - 1; i > 0; i--)
    {
      if (self->data[i] == '\0')
        break;
      g_assert (i > 0);
    }
  /* We went back a step too far */
  i++;

  parser = json_parser_new_immutable ();
  json_parser_load_from_data (parser, (const gchar *) self->data + i,
                              (gint) self->data_size - i,
                              &error);
  if (error)
    {
      g_warning ("Failed to parse index from word list: %s\n", error->message);
      g_warning ("Index:\n%s\n", (const gchar *) self->data + i);
      return;
    }
  self->index = word_list_index_new_from_json (json_parser_get_root (parser));
}

static const gchar *
word_list_lookup_word (WordList        *word_list,
                       WordListSection *section,
                       WordIndex        word)
{
  WordListSection real_section;

  if (section == NULL)
    {
      real_section = word_list_index_get_section (word_list->index, word.length);
      section = &real_section;
    }

  return (const gchar *) word_list->data +
    (section->stride * word.index) +
    section->offset + WORD_OFFSET;
}

static gboolean
word_list_lookup_fragment (WordList           *word_list,
                           FilterFragment      fragment,
                           FilterFragmentList *list)
{
  gint offset;
  guint frag_offset;

  offset = word_index_fragment_index (word_list->index->min_length,
                                      ipuz_charset_get_n_chars (word_list->index->charset),
                                      fragment);
  offset *= (sizeof (guint) + sizeof (gushort));
  offset += word_list->index->letter_index_offset;

  /* FIXME(serialize) */
  frag_offset = *(guint *) (word_list->data + offset);

  list->data = (gushort *)(word_list->data + frag_offset);
  list->len = *(gushort *)(word_list->data + offset + sizeof (guint));

  return TRUE;
}


/* Public methods */

WordList *
word_list_new (void)
{
  GError *error = NULL;
  GBytes *bytes = g_resources_lookup_data ("/org/gnome/Crosswords/data/wordlist.dict", 0,
                                           &error);
  if (bytes == NULL)
    {
      g_error ("Incorrect build; the wordlist.dict resource should exist: %s",
               error->message);
      return NULL; /* unreachable */
    }

  WordList *word_list = word_list_new_from_bytes (bytes);
  g_bytes_unref (bytes);

  return word_list;
}

/**
 * @bytes: (transfer none) Binary data with the computed word list.
 *
 * Creates a word list database by parsing a pre-generated buffer.
 */
WordList *
word_list_new_from_bytes (GBytes *bytes)
{
  WordList *self;

  self = g_object_new (WORD_TYPE_LIST, NULL);

  self->bytes = g_bytes_ref (bytes);
  self->data = g_bytes_get_data (self->bytes, &self->data_size);
  g_assert (self->data != NULL);

  self->list = g_array_new (FALSE, TRUE, sizeof (gchar *));
  word_list_load_index (self);

  return self;
}


static void
word_list_set_list_words (WordList *word_list)
{
  word_list->word_list_only = TRUE;
}

static void
fragment_list_union (GArray             *union_list,
                     FilterFragmentList  fragment_list)
{
  /* We will never have a result longer than the shortest list */
  gushort *target = g_new0 (gushort, MIN (union_list->len, fragment_list.len));
  guint u = 0, f = 0, len = 0;

  do
    {
      gushort u_val, f_val;

      u_val = g_array_index (union_list, gushort, u);
      f_val = fragment_list.data[f];

      if (u_val == f_val)
        {
          target[len++] = u_val;
          u++; f++;
        }
      else if (u_val < f_val)
        u++;
      else
        f++;
    }
  while ((u < union_list->len) && (f < fragment_list.len));

  g_array_set_size (union_list, len);
  memcpy (union_list->data, target, len*sizeof(gushort));

  g_free (target);
}

static void
copy_union_list_to_word_list (WordList *word_list,
                              GArray   *union_list)
{
  g_array_set_size (word_list->list, union_list->len);

  if (union_list->len > 0)
    {
      for (guint i = 0; i < union_list->len; i++)
        {
          WordIndex word = {
            .length = word_list->filter_len,
          };
          const gchar **word_ptr;

          word.index = (int) g_array_index (union_list, gushort, i);

          word_ptr = &(g_array_index (word_list->list, const gchar *, i));
          *word_ptr = word_list_lookup_word (word_list, NULL, word);
        }
    }
}

static void
word_list_set_list_filters (WordList *word_list)
{
  const gchar *ptr;
  gint pos = 0;
  g_autoptr (GArray) union_list = NULL;

  word_list->word_list_only = FALSE;

  for (ptr = word_list->filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      gunichar c;

      /* FIXME (magichars): We should both catch this at the input
       * time and centralize this */
      if (ptr[0] == '?' || ptr[0] == ' ')
        {
          pos++;
          continue;
        }

      c = g_utf8_get_char (ptr);
      /* This should be fine. We call this after sanitizing the
       * filter. */
      fragment.length = word_list->filter_len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, c);

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len == 0)
        {
          /* There are no fragments matching this description */
          g_array_set_size (word_list->list, 0);
          return;
        }

      if (union_list == NULL)
        {
          union_list = g_array_new (FALSE, FALSE, sizeof (gushort));
          g_array_set_size (union_list, fragment_list.len);
          memcpy (union_list->data, fragment_list.data, fragment_list.len * sizeof (gushort));
        }
      else
        {
          fragment_list_union (union_list, fragment_list);
          if (union_list->len == 0)
            {
              g_array_set_size (word_list->list, 0);
              return;
            }
        }
      pos++;
    }

  /* The loop above skips over '?', but we can assert the following because the caller of
   * this function, word_list_set_filter(), already ensured that there is at least one
   * non-'?' character in the query string.
   */
  g_assert (union_list != NULL);

  copy_union_list_to_word_list (word_list, union_list);
}

static AnagramHashIndex *
find_hash_index (WordList *self,
                 guint     hash)
{
  AnagramHashIndex *hash_data;
  guint start, end, middle;

  start = 0;
  end = self->index->anagram_hash_index_length;
  hash_data = (AnagramHashIndex *) (self->data + self->index->anagram_hash_index_offset);

  while (start <= end)
    {
      middle = start + (end - start)/2;

      if (hash_data [middle].hash == hash)
        return &hash_data [middle];
      if (hash_data [middle].hash < hash)
        start = middle + 1;
      else
        end = middle - 1;
    }

  return NULL;
}

static void
word_list_set_anagrams (WordList *self)
{
  AnagramHashIndex *hash_index;
  guint hash;

  hash = word_list_hash_func (self->filter);
  hash_index = find_hash_index (self, hash);

  if (hash_index)
    {
      g_autoptr (GArray) union_list = NULL;

      union_list = g_array_new (FALSE, FALSE, sizeof (gushort));
      g_array_set_size (union_list, (guint) hash_index->len);
      memcpy (union_list->data, self->data + hash_index->offset,
              (size_t) (((guint)hash_index->len) * sizeof (gushort)));
      copy_union_list_to_word_list (self, union_list);
    }
  else /* No hash hit */
    {
      g_array_set_size (self->list, 0);
    }

}

void
word_list_set_filter (WordList     *self,
                      const char   *filter,
                      WordListMode  mode)
{
  const gchar *ptr;
  gint filter_count = 0;
  // GTimer *timer;

  g_return_if_fail (WORD_IS_LIST (self));

  // timer = g_timer_new ();
  // g_timer_start (timer);

  /* short-circuit setting the same filter / mode */
  if (mode == self->mode &&
      ! g_strcmp0 (self->filter, filter))
    return;

  g_clear_pointer (&self->filter, g_free);
  self->filter_len = 0;

  if (filter == NULL)
    {
      g_array_set_size (self->list, 0);
      goto out;
    }

  self->filter = g_strdup (filter);
  self->filter_len = g_utf8_strlen (filter, -1);
  self->mode = mode;

  if (self->filter_len < self->index->min_length ||
      self->filter_len > self->index->max_length)
    {
      g_array_set_size (self->list, 0);
      goto out;
    }

  /* We double check the filter before using it. First, we walk
   * through it making sure that it contains characters we contain. If it
   * has invalid characters, we can't find any results at all.
   *
   * We also keep track of how many characters we explicitly give. For
   * MATCH, if we only have 0 or 1 characters, we can short-circuit
   * some work. (eg. "???" or "C??"). Two or more requires calculating
   * the union of the fragments.
   */
  for (ptr = self->filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      gunichar c;
      gint index;

      /* FIXME (magichars): We should both catch this at the input
       * time and centralize this */
      if (self->mode == WORD_LIST_MATCH &&
          (ptr[0] == '?' || ptr[0] == ' '))
        continue;

      c = g_utf8_get_char (ptr);
      index = ipuz_charset_get_char_index (self->index->charset, c);
      if (index == -1)
        {
          /* we found a character we don't have in our set of
           * words. We can shortcircuit any additional work and return
           * the empty list */
          g_array_set_size (self->list, 0);
          goto out;
        }

      filter_count ++;
    }

  if (self->mode == WORD_LIST_MATCH)
    {
      /* It's just the word list of len. */
      if (filter_count == 0)
        word_list_set_list_words (self);
      else
        word_list_set_list_filters (self);
    }
  else if (self->mode == WORD_LIST_ANAGRAM)
    {
      word_list_set_anagrams (self);
    }

 out:

  // g_timer_stop (timer);
  //  word_list_dump (word_list);
  // g_timer_destroy (timer);
  ; /* the out: label requires a statement in order to be valid C */

}

const gchar *
word_list_get_filter (WordList *word_list)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);

  return word_list->filter;
}

guint
word_list_get_n_items (WordList *word_list)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  if (word_list->word_list_only)
    {
      WordListSection section;

      section = word_list_index_get_section (word_list->index, word_list->filter_len);

      if (section.word_len == -1)
        /* We don't have any words of filter_len length */
        return 0;

      return section.count;
    }
  return word_list->list->len;
}

const gchar *
word_list_get_word (WordList *word_list,
                    guint     position)
{
  const gchar *word;
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  if (word_list->word_list_only)
    {
      WordListSection section;
      WordIndex word_index = {
        .length = word_list->filter_len,
      };

      word_index.index = position;


      section = word_list_index_get_section (word_list->index, word_list->filter_len);

      if (section.word_len == -1)
        /* We don't have any words of filter_len length */
        return NULL;

      word = word_list_lookup_word (word_list, &section, word_index);
      return word;
    }

  word = g_array_index (word_list->list, gchar *, position);

  return word;
}

gboolean
word_list_get_word_index (WordList    *word_list,
                          guint        position,
                          WordIndex   *word_index)
{
  WordListSection section;
  WordIndex new_word_index;
  const guchar *word;

  g_return_val_if_fail (WORD_IS_LIST (word_list), FALSE);

  new_word_index.length = word_list->filter_len;
  section = word_list_index_get_section (word_list->index, word_list->filter_len);

  if (section.word_len == -1)
    /* We don't have any words of filter_len length */
    return FALSE;
  if (word_list->word_list_only)
    {
      /* The index is just the position we were passed in! */
      new_word_index.index = position;
      *word_index = new_word_index;
      return TRUE;
    }

  /* we need to calculate the index from the offset of word. */
  word = g_array_index (word_list->list, guchar *, position);
  new_word_index.index = (((word - word_list->data)) - (section.offset + 1)) / section.stride;

  *word_index = new_word_index;
  return TRUE;

}

gint
word_list_get_priority (WordList *word_list,
                        guint     position)
{
  const gchar *word;
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  word = word_list_get_word (word_list, position);

  return WORD_LIST_PRIORITY (word);
}

const gchar *
word_list_get_enumeration_src  (WordList *word_list,
                                guint     position)
{
  const gchar *word;
  gushort enumeration_offset;

  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  word = word_list_get_word (word_list, position);
  enumeration_offset = ((gushort *) (word - ENUMERATION_SIZE))[0];

  if (enumeration_offset == 0)
    return NULL;

  return (gchar *) (word_list->data + enumeration_offset +
                    word_list->index->enumerations_offset);
}

/**
 * word_list_lookup_index:
 * @word_list: A @WordList
 * @word: The word to lookup
 * @word_index: Location to fill with a new word_index
 *
 * Looks up the location in the master index of @word. Unlike
 * word_list_get_word(), this function ignores the currently set
 * filter and determines if @word is a possible word.
 *
 * Returns:
 **/
gboolean
word_list_lookup_index (WordList    *word_list,
                        const gchar *word,
                        WordIndex   *word_index)
{
  const gchar *ptr;
  gint pos = 0;
  gint len;
  g_autoptr (GArray) union_list = NULL;

  g_return_val_if_fail (WORD_IS_LIST (word_list), FALSE);
  g_return_val_if_fail (word != NULL, FALSE);
  g_return_val_if_fail (word_index != NULL, FALSE);

  if (word == NULL)
    return FALSE;

  len = g_utf8_strlen (word, -1);
  if (len <= word_list->index->min_length ||
      len >= word_list->index->max_length)
    return FALSE;

  for (ptr = word; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      gunichar c;

      /* FIXME (magichars): We should both catch this at the input
       * time and centralize this */
      c = g_utf8_get_char (ptr);
      fragment.length = len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, c);
      if (fragment.char_index == -1)
        /* This word contains a character we don't contain */
        return FALSE;

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len == 0)
        return FALSE;

      if (union_list == NULL)
        {
          union_list = g_array_new (FALSE, FALSE, sizeof (gushort));
          g_array_set_size (union_list, fragment_list.len);
          memcpy (union_list->data, fragment_list.data, fragment_list.len * sizeof (gushort));
        }
      else
        {
          fragment_list_union (union_list, fragment_list);
          if (union_list->len == 0)
            return FALSE;
        }
      pos++;
    }

  if (union_list == NULL)
    return FALSE;

  g_return_val_if_fail (union_list->len == 1, FALSE);
  word_index->length = len;
  word_index->index = (gint) g_array_index (union_list, gushort, 0);

  return TRUE;
}

/**
 * word_list_get_indexed_word:
 * @word_list: A @WordList
 * @word_index: Index location of a word to lookup.
 *
 * Looks up the word at @word_index. This ignores the filter and
 * returns the enry from the global Index table.
 *
 * Returns: the word at @word_index
 **/
const gchar *
word_list_get_indexed_word (WordList  *word_list,
                            WordIndex  word_index)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);

  return word_list_lookup_word (word_list, NULL, word_index);
}

/**
 * word_list_get_indexed_priority:
 * @word_list: A @WordList
 * @word_index: Index location of a priority to lookup.
 *
 * Looks up the word at @word_index. This ignores the filter and
 * returns the entry from the global Index table.
 *
 * Returns: the priority at @word_index
 **/
gint
word_list_get_indexed_priority (WordList  *word_list,
                                WordIndex  word_index)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  return WORD_LIST_PRIORITY (word_list_get_indexed_word (word_list, word_index));
}

/**
 * word_list_dump:
 * @word_list: A @WordList
 *
 * Dumps the current filtered set of words in @word_list to stdout.
 **/
void
word_list_dump (WordList *word_list)
{
  guint i;

  g_return_if_fail (WORD_IS_LIST (word_list));

  if (word_list->list->len == 0)
    g_print ("<no words>\n");

  for (i = 0; i < word_list->list->len; i++)
    {
      g_print ("%s\n", g_array_index (word_list->list, char *, i));
    }
}
