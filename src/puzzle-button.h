/* puzzle-button.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>

#include "puzzle-set-model.h"

G_BEGIN_DECLS

typedef enum
{
  PUZZLE_BUTTON_MODE_NORMAL, /* @ Displays the button with a puzzle thumbnail @ */
  PUZZLE_BUTTON_MODE_LOCKED, /* @ Displays a locked icon @ */
  PUZZLE_BUTTON_MODE_SOLVED, /* @ Displays the button with a solved stamp on the thumbnail @ */
} PuzzleButtonMode;


#define PUZZLE_TYPE_BUTTON (puzzle_button_get_type())

G_DECLARE_FINAL_TYPE (PuzzleButton, puzzle_button, PUZZLE, BUTTON, GtkWidget);

GtkWidget *puzzle_button_new (PuzzleSetModelRow *row);


G_END_DECLS
