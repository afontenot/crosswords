/* word-list.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include "word-list-misc.h"

G_BEGIN_DECLS


typedef enum
{
  WORD_LIST_NONE,
  WORD_LIST_MATCH,
  WORD_LIST_ANAGRAM,
} WordListMode;
  
#define WORD_TYPE_LIST (word_list_get_type())
G_DECLARE_FINAL_TYPE (WordList, word_list, WORD, LIST, GObject);


WordList    *word_list_new                  (void);
WordList    *word_list_new_from_bytes       (GBytes       *bytes);
void         word_list_set_filter           (WordList     *word_list,
                                             const gchar  *filter,
                                             WordListMode  mode);
const gchar *word_list_get_filter           (WordList     *word_list);
guint        word_list_get_n_items          (WordList     *word_list);
const gchar *word_list_get_word             (WordList     *word_list,
                                             guint         position);
gboolean     word_list_get_word_index       (WordList     *word_list,
                                             guint         position,
                                             WordIndex    *word_index);
gint         word_list_get_priority         (WordList     *word_list,
                                             guint         position);
const gchar *word_list_get_enumeration_src  (WordList     *word_list,
                                             guint         position);



/* Indexed words. */
gboolean     word_list_lookup_index         (WordList     *word_list,
                                             const gchar  *word,
                                             WordIndex    *word_index);
const gchar *word_list_get_indexed_word     (WordList     *word_list,
                                             WordIndex     word_index);
gint         word_list_get_indexed_priority (WordList     *word_list,
                                             WordIndex     word_index);
void         word_list_dump                 (WordList     *word_list);


G_END_DECLS
