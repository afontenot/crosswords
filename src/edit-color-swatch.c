/* edit-color-swatch.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-color-swatch.h"


enum
{
  PROP_0,
  PROP_COLOR,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = {0, };


struct _EditColorSwatch
{
  GtkWidget parent_instance;
  GdkRGBA color;
};


G_DEFINE_FINAL_TYPE (EditColorSwatch, edit_color_swatch, GTK_TYPE_WIDGET);


static void edit_color_swatch_init         (EditColorSwatch      *self);
static void edit_color_swatch_class_init   (EditColorSwatchClass *klass);
static void edit_color_swatch_set_property (GObject              *object,
                                            guint                 prop_id,
                                            const GValue         *value,
                                            GParamSpec           *psec);
static void edit_color_swatch_get_property (GObject              *object,
                                            guint                 prop_id,
                                            GValue               *value,
                                            GParamSpec           *psec);
static void edit_color_swatch_snapshot     (GtkWidget            *widget,
                                            GtkSnapshot          *snapshot);


static void
edit_color_swatch_init (EditColorSwatch *self)
{
}

static void
edit_color_swatch_class_init (EditColorSwatchClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_color_swatch_set_property;
  object_class->get_property = edit_color_swatch_get_property;
  widget_class->snapshot = edit_color_swatch_snapshot;

  obj_props [PROP_COLOR] = g_param_spec_boxed ("color",
                                               NULL, NULL,
                                               GDK_TYPE_RGBA,
                                               G_PARAM_READWRITE);
  
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
edit_color_swatch_set_property (GObject              *object,
                                guint                 prop_id,
                                const GValue         *value,
                                GParamSpec           *psec)
{
  EditColorSwatch *self;
  
  g_return_if_fail (EDIT_IS_COLOR_SWATCH (object));

  self = EDIT_COLOR_SWATCH (object);

  switch (prop_id)
    {
      case PROP_COLOR:
        self->color = *(GdkRGBA *)g_value_get_boxed (value);
        gtk_widget_queue_draw (GTK_WIDGET (self));
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
edit_color_swatch_get_property (GObject              *object,
                                guint                 prop_id,
                                GValue               *value,
                                GParamSpec           *psec)
{
  EditColorSwatch *self;
  
  g_return_if_fail (EDIT_IS_COLOR_SWATCH (object));

  self = EDIT_COLOR_SWATCH (object);

  switch (prop_id)
    {
      case PROP_COLOR:
        g_value_set_boxed (value, &self->color);
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
edit_color_swatch_snapshot (GtkWidget   *widget,
                            GtkSnapshot *snapshot)
{
  EditColorSwatch *self;
  gfloat width, height;

  self = EDIT_COLOR_SWATCH (widget);

  width = gtk_widget_get_width (widget);
  height = gtk_widget_get_height (widget);

  gtk_snapshot_append_color (snapshot, &self->color,
                             &GRAPHENE_RECT_INIT (0, 0, width, height));
}
