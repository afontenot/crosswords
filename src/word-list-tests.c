#include <glib.h>
#include <locale.h>
#include "gen-word-list.h"
#include "word-list.h"

static void
dump_bytes (GBytes *bytes)
{
  GError *error = NULL;
  GSubprocess *sub = g_subprocess_new ((G_SUBPROCESS_FLAGS_STDIN_PIPE
                                        | G_SUBPROCESS_FLAGS_STDOUT_PIPE
                                        | G_SUBPROCESS_FLAGS_STDERR_MERGE),
                                       &error,
                                       "od",
                                       /* hex offsets */
                                       "-A",
                                       "x",
                                       /* hex bytes, 1 byte per integer, add printout */
                                       "-t",
                                       "x1z",
                                       /* no duplicate suppression */
                                       "-v",
                                       NULL);
  GBytes *stdout_buf = NULL;
  gpointer stdout_data;
  gsize stdout_size;

  if (!sub)
    {
      g_error ("Could not spawn od(1): %s", error->message);
      return; /* unreachable */
    }

  if (!g_subprocess_communicate (sub,
                                 bytes, /* stdin */
                                 NULL, /* cancellable */
                                 &stdout_buf,
                                 NULL, /* stderr_buf */
                                 &error))
    {
      g_error ("Error while running od(1): %s", error->message);
      return; /* unreachable */
    }

  stdout_data = g_bytes_unref_to_data (stdout_buf, &stdout_size);

  fwrite (stdout_data, 1, stdout_size, stdout);

  g_free (stdout_data);
  g_object_unref (sub);
}

static void
loads_default (void)
{
  WordList *wl = word_list_new ();
  word_list_set_filter (wl, "A??", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (wl), ==, 165);
  g_object_unref (wl);
}

typedef struct {
  const char *word;
  glong priority;
} WordToAdd;

static WordList *
create_test_word_list (const WordToAdd *words,
                       gsize            num_words,
                       gint             min_length,
                       gint             max_length,
                       gint             threshold)
{
  GenWordList *gen = gen_word_list_new (min_length, max_length, 0, BRODA_SCORED);

  for (gsize i = 0; i < num_words; i++)
    {
      gen_word_list_add_test_word (gen, words[i].word, words[i].priority);
    }

  gen_word_list_sort (gen);
  gen_word_list_build_charset (gen);
  gen_word_list_anagram_table (gen);
  gen_word_list_calculate_offsets (gen);
  create_anagram_fragments (gen);

  GOutputStream *stream = g_memory_output_stream_new_resizable ();
  gen_word_list_write_to_stream (gen, stream);
  g_output_stream_close (stream, NULL, NULL);
  gen_word_list_free (gen);

  GBytes *bytes = g_memory_output_stream_steal_as_bytes (G_MEMORY_OUTPUT_STREAM (stream));
  g_object_unref (stream);

  dump_bytes (bytes);

  WordList *word_list = word_list_new_from_bytes (bytes);
  g_bytes_unref (bytes);

  return word_list;
}

static void
ascii_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "GHIJK", .priority = 3 },
    { .word = "DEF", .priority = 1 },
    { .word = "ABC", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ABC");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "DEF");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 2);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex abc;
  WordIndex def;
  g_assert_true (word_list_get_word_index (word_list, 0, &abc));
  g_assert_true (word_list_get_word_index (word_list, 1, &def));

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 0);

  word_list_set_filter (word_list, "?????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "GHIJK");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 3);
  WordIndex ghijk;
  g_assert_true (word_list_get_word_index (word_list, 0, &ghijk));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, abc), ==, "ABC");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, def), ==, "DEF");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ghijk), ==, "GHIJK");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, abc), ==, 2);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, def), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ghijk), ==, 3);

  g_object_unref (word_list);
}

static void
utf8_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "AÑO", .priority = 1 },
    { .word = "ÉSE", .priority = 1 },

    { .word = "HOLA", .priority = 2 },
    { .word = "PÂTÉ", .priority = 2 },
    { .word = "ZULU", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "ÉSE");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 1);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex ano;
  WordIndex ese;
  g_assert_true (word_list_get_word_index (word_list, 0, &ano));
  g_assert_true (word_list_get_word_index (word_list, 1, &ese));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, ano), ==, "AÑO");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ese), ==, "ÉSE");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, ano), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ese), ==, 1);

  word_list_set_filter (word_list, "A?O", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");

  word_list_set_filter (word_list, "??E", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ÉSE");

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 3);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "HOLA");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "PÂTÉ");
  g_assert_cmpstr (word_list_get_word (word_list, 2), ==, "ZULU");

  g_object_unref (word_list);
}

static void
anagram_test (void)
{
  WordList *wl = word_list_new ();

  word_list_set_filter (wl, "CAT", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 4);

  g_assert (!g_strcmp0 (word_list_get_word (wl, 0), "ACT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 1), "CAT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 2), "CTA"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 3), "TAC"));

  word_list_set_filter (wl, "NOTANANAGRAM", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 0);

  g_object_unref (wl);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/word_list/loads_default", loads_default);

  g_test_add_func ("/word_list/ascii_roundtrip", ascii_roundtrip);
  g_test_add_func ("/word_list/utf8_roundtrip", utf8_roundtrip);

  g_test_add_func ("/word_list/set_anagram", anagram_test);

  return g_test_run ();
}
