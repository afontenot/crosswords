common_sources = [
  'cell-array.c',
  'clue-grid.c',
  'crosswords-app.c',
  'crosswords-credits.c',
  'crosswords-init.c',
  'crosswords-misc.c',
  'crosswords-quirks.c',
  'contrib/gnome-languages.c',
  'grid-layout.c',
  'grid-state.c',
  'play-border.c',
  'play-cell.c',
  'play-clue-list.c',
  'play-clue-row.c',
  'play-grid.c',
  'play-preferences-dialog.c',
  'play-style.c',
  'play-window.c',
  'play-xword.c',
  'play-xword-column.c',
  'picker-grid.c',
  'picker-list.c',
  'picker-list-row.c',
  'puzzle-button.c',
  'puzzle-button-icon.c',
  'puzzle-downloader.c',
  'puzzle-downloader-dialog.c',
  'puzzle-picker.c',
  'puzzle-set.c',
  'puzzle-set-action-row.c',
  'puzzle-set-config.c',
  'puzzle-set-list.c',
  'puzzle-set-model.c',
  'puzzle-stack.c',
  'svg.c',
]

common_headers = [
  'cell-array.h',
  'clue-grid.h',
  'crosswords-app.h',
  'crosswords-credits.h',
  'crosswords-init.h',
  'crosswords-misc.h',
  'crosswords-quirks.h',
  'contrib/gnome-languages.h',
  'grid-layout.h',
  'grid-state.h',
  'play-border.h',
  'play-cell.h',
  'play-clue-list.h',
  'play-clue-row.h',
  'play-grid.h',
  'play-preferences-dialog.h',
  'play-style.h',
  'play-window.h',
  'play-xword.h',
  'play-xword-column.h',
  'picker-grid.h',
  'picker-list.h',
  'picker-list-row.h',
  'puzzle-button.h',
  'puzzle-button-icon.h',
  'puzzle-downloader.h',
  'puzzle-downloader-dialog.h',
  'puzzle-picker.h',
  'puzzle-set.h',
  'puzzle-set-action-row.h',
  'puzzle-set-config.h',
  'puzzle-set-list.h',
  'puzzle-set-model.h',
  'puzzle-stack.h',
  'svg.h',
]

edit_sources = [
  'acrostic-generator.c',
  'basic-templates.c',
  'cell-preview.c',
  'charset-entry-buffer.c',
  'edit-app.c',
  'edit-autofill-dialog.c',
  'edit-bars.c',
  'edit-cell.c',
  'edit-clue-details.c',
  'edit-clue-list.c',
  'edit-clue-info.c',
  'edit-color-row.c',
  'edit-color-swatch.c',
  'edit-entry-row.c',
  'edit-greeter.c',
  'edit-greeter-crossword.c',
  'edit-greeter-details.c',
  'edit-grid.c',
  'edit-grid-info.c',
  'edit-histogram.c',
  'edit-metadata.c',
  'edit-paned.c',
  'edit-puzzle-stack.c',
  'edit-save-changes-dialog.c',
  'edit-shapebg-row.c',
  'edit-state.c',
  'edit-symmetry.c',
  'edit-window.c',
  'edit-window-actions.c',
  'edit-window-controls.c',
  'edit-word-list.c',
]

edit_headers = [
  'acrostic-generator.h',
  'basic-templates.h',
  'cell-preview.h',
  'charset-entry-buffer.h',
  'edit-app.h',
  'edit-autofill-dialog.h',
  'edit-bars.h',
  'edit-cell.h',
  'edit-clue-details.h',
  'edit-clue-list.h',
  'edit-clue-info.h',
  'edit-color-row.h',
  'edit-color-swatch.h',
  'edit-entry-row.h',
  'edit-greeter.h',
  'edit-greeter-details.h',
  'edit-greeter-crossword.h',
  'edit-grid.h',
  'edit-grid-info.h',
  'edit-histogram.h',
  'edit-metadata.h',
  'edit-paned.h',
  'edit-puzzle-stack.h',
  'edit-save-changes-dialog.h',
  'edit-shapebg-row.h',
  'edit-state.h',
  'edit-symmetry.h',
  'edit-window.h',
  'edit-window-actions.h',
  'edit-window-controls.h',
  'edit-window-private.h',
  'edit-word-list.h',
  'word-list.h',
  'word-list-misc.h',
  'word-list-model.h',
  'word-list-index.h',
  'word-solver.h',
]

common_includes = include_directories(
  '.',
  '..',
  '../src/contrib',
)

word_list_sources = files(
  'gen-word-list.c',
  'gen-word-list-importer.c',
  'cell-array.c',
  'word-list.c',
  'word-list-misc.c',
  'word-list-model.c',
  'word-list-index.c',
  'word-solver.c',
)

libword_list = static_library(
  'word_list',
  sources: word_list_sources,
  install: false,
  dependencies: [
    gio_dep,
    libipuz_dep,
    json_glib_dep,
  ]
)

libword_list_dep = declare_dependency(
  link_with: libword_list,
  include_directories: word_list_inc,
  dependencies: libipuz_dep
)

gen_word_list_resource = executable(
  'gen-word-list-resource',
  sources: [
    'gen-word-list-resource.c',
  ],
  dependencies: [
    gio_dep,
    libword_list_dep,
    json_glib_dep,
  ],
  install: false,
)

word_list_generated = custom_target(
  'wordlist.dict',
  output: 'wordlist.dict',
  input: 'data/peter-broda-wordlist__full-text__scored.txt',
  #input: 'data/wordlist-20210729.txt',
  command: [ gen_word_list_resource, '-i', 'BRODA-FULL', '-o', '@OUTPUT@', '@INPUT@']
)

crosswords_deps = [
  dependency('gio-2.0', version: '>= 2.50'),
  dependency('gtk4', version: '>= 4.2'),
  json_glib_dep,
  gtk4_dep,
  libipuz_dep,
  libadwaita_dep,
  librsvg_dep,
  libword_list_dep,
]


crosswords_resource_file_config = configuration_data()
crosswords_resource_file_config.set('builddir', meson.current_build_dir())

if get_option('development')
crosswords_resource_file_config.set('hero', 'hero-devel.ipuz')
crosswords_resource_file_config.set('cssprefix', '/org/gnome/Crosswords/Devel')
crosswords_resource_file_config.set('editcssprefix', '/org/gnome/Crosswords/Editor/Devel')
crosswords_resource_file_config.set('metainfofile', 'org.gnome.Crosswords.Devel.metainfo.xml')
crosswords_resource_file_config.set('editmetainfofile', 'org.gnome.Crosswords.Editor.Devel.metainfo.xml')
else
crosswords_resource_file_config.set('hero', 'hero.ipuz')
crosswords_resource_file_config.set('cssprefix', '/org/gnome/Crosswords')
crosswords_resource_file_config.set('editcssprefix', '/org/gnome/Crosswords/Editor')
crosswords_resource_file_config.set('metainfofile', 'org.gnome.Crosswords.metainfo.xml')
crosswords_resource_file_config.set('editmetainfofile', 'org.gnome.Crosswords.Editor.metainfo.xml')
endif
crosswords_resource_file = configure_file(
          input: 'crosswords.gresource.xml.in',
         output: 'crosswords.gresource.xml',
  configuration: crosswords_resource_file_config,
)

common_sources += gnome.compile_resources('crosswords-resources',
                                          crosswords_resource_file,
                                          c_name: 'crosswords')

wordlist_config = configuration_data()
wordlist_config.set('builddir', meson.current_build_dir())

wordlist_gresource_xml = configure_file(
  input: 'wordlist.gresource.xml.in',
  output: 'wordlist.gresource.xml',
  configuration: wordlist_config
)

word_list_resources = gnome.compile_resources(
  'wordlist-resources',
  wordlist_gresource_xml,
  c_name: 'wordlist',
  dependencies: word_list_generated
)

common_sources += word_list_resources

marshallers = gnome.genmarshal('play-marshaller',
  sources: 'marshallers.list',
  install_header : false)

types_headers = [
  'crosswords-quirks.h',
  'edit-color-row.h',
  'edit-entry-row.h',
  'edit-clue-info.h',
  'edit-histogram.h',
  'grid-state.h',
  'play-border.h',
  'play-clue-list.h',
  'play-clue-row.h',
  'play-grid.h',
  'play-xword.h',
  'puzzle-button.h',
  'puzzle-set.h',
  'puzzle-set-model.h',
  'puzzle-stack.h',
]

enum_sources = gnome.mkenums_simple(
  'crosswords-enums',
  sources: types_headers
)

common_sources += marshallers
common_sources += enum_sources


crosswords_sources = common_sources + ['main.c']

crossword_edit_sources = common_sources + edit_sources + ['edit-main.c' ]


executable('crosswords', crosswords_sources,
  dependencies: crosswords_deps,
  install: true,
)


executable('crossword-editor', crossword_edit_sources,
  dependencies: crosswords_deps,
  install: true,
)

tests_main_sources = [
  'layout-tests.c',
  'tests-main.c',
  'test-utils.c',
] + common_sources

tests_main = executable('tests_main', tests_main_sources,
  dependencies: crosswords_deps,
  install: false,
)

test(
  'tests_main',
  tests_main,
  args: [ '--tap', '-k' ],
  env: [
    'G_TEST_SRCDIR=@0@'.format(meson.current_source_dir()),
    'G_TEST_BUILDDIR=@0@'.format(meson.current_build_dir()),
  ],
)

word_list_tests = executable(
  'word-list-tests',
  [ 'word-list-tests.c' ] + word_list_resources,
  dependencies: crosswords_deps,
  install: false
)

test(
  'word-list-tests',
  word_list_tests,
  args: [ '--tap', '-k' ],
  env: [
    'G_TEST_SRCDIR=@0@'.format(meson.current_source_dir()),
    'G_TEST_BUILDDIR=@0@'.format(meson.current_build_dir()),
  ],
)

acrostic_generator_tests = executable(
  'acrostic-generator-tests',
  [ 'acrostic-generator-tests.c' ] + edit_sources + common_sources + word_list_resources,
  dependencies: crosswords_deps,
  install: false
)

test(
  'acrostic-generator-tests',
  acrostic_generator_tests,
  args: [ '--tap', '-k' ],
  env: [
    'G_TEST_SRCDIR=@0@'.format(meson.current_source_dir()),
    'G_TEST_BUILDDIR=@0@'.format(meson.current_build_dir()),
  ],
)


# basenames of files that have standalone unit tests
unit_tests = [
  'gen-word-list',
  'gen-word-list-importer',
  'word-list-misc',
]

foreach t: unit_tests
  exe = executable(
    t,
    '@0@.c'.format(t),
    c_args: '-DTESTING',
    dependencies: [libword_list_dep, json_glib_dep, gio_dep],
    install: false,
  )

  test(
    t,
    exe,
    args: [ '--tap', '-k' ],
    env: [
      'G_TEST_SRCDIR=@0@'.format(meson.current_source_dir()),
      'G_TEST_BUILDDIR=@0@'.format(meson.current_build_dir()),
    ],
  )
endforeach

bench_main_sources = [
  'bench-main.c',
] + common_sources

bench_main = executable('bench_main', bench_main_sources,
  dependencies: crosswords_deps,
  install: false,
)

benchmark(
  'bench_main',
  bench_main,
  env: [
    'G_TEST_SRCDIR=@0@'.format(meson.current_source_dir()),
    'G_TEST_BUILDDIR=@0@'.format(meson.current_build_dir()),
  ],
)
