/* gen-word-list.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib.h>
#include <libipuz/libipuz.h>

G_BEGIN_DECLS


typedef enum {
  PARSE_LINE_STATUS_OK,
  PARSE_LINE_STATUS_BAD_SYNTAX,
  PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE,
  PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE,
} ParseLineStatus;

typedef struct
{
  char *word;
  gboolean free_word;

  char *enumeration_src;

  /* Number of code points in the word (3 for FOO and FÖÖ). */
  gint word_len;

  /* Number of bytes in the word, without the nul terminator (3 for FOO, 5 for FÖÖ). */
  gssize byte_len;

  /* Priority of the word; comes from the corpus. */
  guchar priority;
} ValidatedWord;


ParseLineStatus validate_word           (const gchar   *word,
                                         gboolean       free_word,
                                         gssize         byte_len,
                                         gint           word_len,
                                         glong          priority,
                                         gint           min_length,
                                         gint           max_length,
                                         guchar         threshold,
                                         ValidatedWord *out_validated);
void            validated_word_free     (ValidatedWord  v);

ParseLineStatus parse_line_broda_full   (ValidatedWord *out_validated,
                                         IPuzCharset   *alphabet,
                                         const gchar   *line,
                                         gint           min_length,
                                         gint           max_length,
                                         guchar         threshold);
ParseLineStatus parse_line_broda_scored (ValidatedWord *out_validated,
                                         const gchar   *line,
                                         gint           min_length,
                                         gint           max_length,
                                         guchar         threshold);


G_END_DECLS
